/**
 * Vulkan 1.0
 *
 * Generated from 1.0
 */

import * as GObject from "gobject2";

export class Bool32 {
    static $gtype: GObject.GType<Bool32>;

    constructor(copy: Bool32);
}

export class DeviceAddress {
    static $gtype: GObject.GType<DeviceAddress>;

    constructor(copy: DeviceAddress);
}

export class DeviceSize {
    static $gtype: GObject.GType<DeviceSize>;

    constructor(copy: DeviceSize);
}

export class Flags {
    static $gtype: GObject.GType<Flags>;

    constructor(copy: Flags);
}

export class SampleMask {
    static $gtype: GObject.GType<SampleMask>;

    constructor(copy: SampleMask);
}

export class Buffer {
    static $gtype: GObject.GType<Buffer>;

    constructor(copy: Buffer);
}

export class Image {
    static $gtype: GObject.GType<Image>;

    constructor(copy: Image);
}

export class Instance {
    static $gtype: GObject.GType<Instance>;

    constructor(copy: Instance);
}

export class PhysicalDevice {
    static $gtype: GObject.GType<PhysicalDevice>;

    constructor(copy: PhysicalDevice);
}

export class Device {
    static $gtype: GObject.GType<Device>;

    constructor(copy: Device);
}

export class Queue {
    static $gtype: GObject.GType<Queue>;

    constructor(copy: Queue);
}

export class Semaphore {
    static $gtype: GObject.GType<Semaphore>;

    constructor(copy: Semaphore);
}

export class CommandBuffer {
    static $gtype: GObject.GType<CommandBuffer>;

    constructor(copy: CommandBuffer);
}

export class Fence {
    static $gtype: GObject.GType<Fence>;

    constructor(copy: Fence);
}

export class DeviceMemory {
    static $gtype: GObject.GType<DeviceMemory>;

    constructor(copy: DeviceMemory);
}

export class Event {
    static $gtype: GObject.GType<Event>;

    constructor(copy: Event);
}

export class QueryPool {
    static $gtype: GObject.GType<QueryPool>;

    constructor(copy: QueryPool);
}

export class BufferView {
    static $gtype: GObject.GType<BufferView>;

    constructor(copy: BufferView);
}

export class ImageView {
    static $gtype: GObject.GType<ImageView>;

    constructor(copy: ImageView);
}

export class ShaderModule {
    static $gtype: GObject.GType<ShaderModule>;

    constructor(copy: ShaderModule);
}

export class PipelineCache {
    static $gtype: GObject.GType<PipelineCache>;

    constructor(copy: PipelineCache);
}

export class PipelineLayout {
    static $gtype: GObject.GType<PipelineLayout>;

    constructor(copy: PipelineLayout);
}

export class Pipeline {
    static $gtype: GObject.GType<Pipeline>;

    constructor(copy: Pipeline);
}

export class RenderPass {
    static $gtype: GObject.GType<RenderPass>;

    constructor(copy: RenderPass);
}

export class DescriptorSetLayout {
    static $gtype: GObject.GType<DescriptorSetLayout>;

    constructor(copy: DescriptorSetLayout);
}

export class Sampler {
    static $gtype: GObject.GType<Sampler>;

    constructor(copy: Sampler);
}

export class DescriptorSet {
    static $gtype: GObject.GType<DescriptorSet>;

    constructor(copy: DescriptorSet);
}

export class DescriptorPool {
    static $gtype: GObject.GType<DescriptorPool>;

    constructor(copy: DescriptorPool);
}

export class Framebuffer {
    static $gtype: GObject.GType<Framebuffer>;

    constructor(copy: Framebuffer);
}

export class CommandPool {
    static $gtype: GObject.GType<CommandPool>;

    constructor(copy: CommandPool);
}

export class Result {
    static $gtype: GObject.GType<Result>;

    constructor(copy: Result);
}

export class StructureType {
    static $gtype: GObject.GType<StructureType>;

    constructor(copy: StructureType);
}

export class PipelineCacheHeaderVersion {
    static $gtype: GObject.GType<PipelineCacheHeaderVersion>;

    constructor(copy: PipelineCacheHeaderVersion);
}

export class ImageLayout {
    static $gtype: GObject.GType<ImageLayout>;

    constructor(copy: ImageLayout);
}

export class ObjectType {
    static $gtype: GObject.GType<ObjectType>;

    constructor(copy: ObjectType);
}

export class VendorId {
    static $gtype: GObject.GType<VendorId>;

    constructor(copy: VendorId);
}

export class SystemAllocationScope {
    static $gtype: GObject.GType<SystemAllocationScope>;

    constructor(copy: SystemAllocationScope);
}

export class InternalAllocationType {
    static $gtype: GObject.GType<InternalAllocationType>;

    constructor(copy: InternalAllocationType);
}

export class Format {
    static $gtype: GObject.GType<Format>;

    constructor(copy: Format);
}

export class ImageTiling {
    static $gtype: GObject.GType<ImageTiling>;

    constructor(copy: ImageTiling);
}

export class ImageType {
    static $gtype: GObject.GType<ImageType>;

    constructor(copy: ImageType);
}

export class PhysicalDeviceType {
    static $gtype: GObject.GType<PhysicalDeviceType>;

    constructor(copy: PhysicalDeviceType);
}

export class QueryType {
    static $gtype: GObject.GType<QueryType>;

    constructor(copy: QueryType);
}

export class SharingMode {
    static $gtype: GObject.GType<SharingMode>;

    constructor(copy: SharingMode);
}

export class ComponentSwizzle {
    static $gtype: GObject.GType<ComponentSwizzle>;

    constructor(copy: ComponentSwizzle);
}

export class ImageViewType {
    static $gtype: GObject.GType<ImageViewType>;

    constructor(copy: ImageViewType);
}

export class BlendFactor {
    static $gtype: GObject.GType<BlendFactor>;

    constructor(copy: BlendFactor);
}

export class BlendOp {
    static $gtype: GObject.GType<BlendOp>;

    constructor(copy: BlendOp);
}

export class CompareOp {
    static $gtype: GObject.GType<CompareOp>;

    constructor(copy: CompareOp);
}

export class DynamicState {
    static $gtype: GObject.GType<DynamicState>;

    constructor(copy: DynamicState);
}

export class FrontFace {
    static $gtype: GObject.GType<FrontFace>;

    constructor(copy: FrontFace);
}

export class VertexInputRate {
    static $gtype: GObject.GType<VertexInputRate>;

    constructor(copy: VertexInputRate);
}

export class PrimitiveTopology {
    static $gtype: GObject.GType<PrimitiveTopology>;

    constructor(copy: PrimitiveTopology);
}

export class PolygonMode {
    static $gtype: GObject.GType<PolygonMode>;

    constructor(copy: PolygonMode);
}

export class StencilOp {
    static $gtype: GObject.GType<StencilOp>;

    constructor(copy: StencilOp);
}

export class LogicOp {
    static $gtype: GObject.GType<LogicOp>;

    constructor(copy: LogicOp);
}

export class BorderColor {
    static $gtype: GObject.GType<BorderColor>;

    constructor(copy: BorderColor);
}

export class Filter {
    static $gtype: GObject.GType<Filter>;

    constructor(copy: Filter);
}

export class SamplerAddressMode {
    static $gtype: GObject.GType<SamplerAddressMode>;

    constructor(copy: SamplerAddressMode);
}

export class SamplerMipmapMode {
    static $gtype: GObject.GType<SamplerMipmapMode>;

    constructor(copy: SamplerMipmapMode);
}

export class DescriptorType {
    static $gtype: GObject.GType<DescriptorType>;

    constructor(copy: DescriptorType);
}

export class AttachmentLoadOp {
    static $gtype: GObject.GType<AttachmentLoadOp>;

    constructor(copy: AttachmentLoadOp);
}

export class AttachmentStoreOp {
    static $gtype: GObject.GType<AttachmentStoreOp>;

    constructor(copy: AttachmentStoreOp);
}

export class PipelineBindPoint {
    static $gtype: GObject.GType<PipelineBindPoint>;

    constructor(copy: PipelineBindPoint);
}

export class CommandBufferLevel {
    static $gtype: GObject.GType<CommandBufferLevel>;

    constructor(copy: CommandBufferLevel);
}

export class IndexType {
    static $gtype: GObject.GType<IndexType>;

    constructor(copy: IndexType);
}

export class SubpassContents {
    static $gtype: GObject.GType<SubpassContents>;

    constructor(copy: SubpassContents);
}

export class AccessFlagBits {
    static $gtype: GObject.GType<AccessFlagBits>;

    constructor(copy: AccessFlagBits);
}

export class AccessFlags {
    static $gtype: GObject.GType<AccessFlags>;

    constructor(copy: AccessFlags);
}

export class ImageAspectFlagBits {
    static $gtype: GObject.GType<ImageAspectFlagBits>;

    constructor(copy: ImageAspectFlagBits);
}

export class ImageAspectFlags {
    static $gtype: GObject.GType<ImageAspectFlags>;

    constructor(copy: ImageAspectFlags);
}

export class FormatFeatureFlagBits {
    static $gtype: GObject.GType<FormatFeatureFlagBits>;

    constructor(copy: FormatFeatureFlagBits);
}

export class FormatFeatureFlags {
    static $gtype: GObject.GType<FormatFeatureFlags>;

    constructor(copy: FormatFeatureFlags);
}

export class ImageCreateFlagBits {
    static $gtype: GObject.GType<ImageCreateFlagBits>;

    constructor(copy: ImageCreateFlagBits);
}

export class ImageCreateFlags {
    static $gtype: GObject.GType<ImageCreateFlags>;

    constructor(copy: ImageCreateFlags);
}

export class SampleCountFlagBits {
    static $gtype: GObject.GType<SampleCountFlagBits>;

    constructor(copy: SampleCountFlagBits);
}

export class SampleCountFlags {
    static $gtype: GObject.GType<SampleCountFlags>;

    constructor(copy: SampleCountFlags);
}

export class ImageUsageFlagBits {
    static $gtype: GObject.GType<ImageUsageFlagBits>;

    constructor(copy: ImageUsageFlagBits);
}

export class ImageUsageFlags {
    static $gtype: GObject.GType<ImageUsageFlags>;

    constructor(copy: ImageUsageFlags);
}

export class InstanceCreateFlagBits {
    static $gtype: GObject.GType<InstanceCreateFlagBits>;

    constructor(copy: InstanceCreateFlagBits);
}

export class InstanceCreateFlags {
    static $gtype: GObject.GType<InstanceCreateFlags>;

    constructor(copy: InstanceCreateFlags);
}

export class MemoryHeapFlagBits {
    static $gtype: GObject.GType<MemoryHeapFlagBits>;

    constructor(copy: MemoryHeapFlagBits);
}

export class MemoryHeapFlags {
    static $gtype: GObject.GType<MemoryHeapFlags>;

    constructor(copy: MemoryHeapFlags);
}

export class MemoryPropertyFlagBits {
    static $gtype: GObject.GType<MemoryPropertyFlagBits>;

    constructor(copy: MemoryPropertyFlagBits);
}

export class MemoryPropertyFlags {
    static $gtype: GObject.GType<MemoryPropertyFlags>;

    constructor(copy: MemoryPropertyFlags);
}

export class QueueFlagBits {
    static $gtype: GObject.GType<QueueFlagBits>;

    constructor(copy: QueueFlagBits);
}

export class QueueFlags {
    static $gtype: GObject.GType<QueueFlags>;

    constructor(copy: QueueFlags);
}

export class DeviceCreateFlags {
    static $gtype: GObject.GType<DeviceCreateFlags>;

    constructor(copy: DeviceCreateFlags);
}

export class DeviceQueueCreateFlagBits {
    static $gtype: GObject.GType<DeviceQueueCreateFlagBits>;

    constructor(copy: DeviceQueueCreateFlagBits);
}

export class DeviceQueueCreateFlags {
    static $gtype: GObject.GType<DeviceQueueCreateFlags>;

    constructor(copy: DeviceQueueCreateFlags);
}

export class PipelineStageFlagBits {
    static $gtype: GObject.GType<PipelineStageFlagBits>;

    constructor(copy: PipelineStageFlagBits);
}

export class PipelineStageFlags {
    static $gtype: GObject.GType<PipelineStageFlags>;

    constructor(copy: PipelineStageFlags);
}

export class MemoryMapFlagBits {
    static $gtype: GObject.GType<MemoryMapFlagBits>;

    constructor(copy: MemoryMapFlagBits);
}

export class MemoryMapFlags {
    static $gtype: GObject.GType<MemoryMapFlags>;

    constructor(copy: MemoryMapFlags);
}

export class SparseMemoryBindFlagBits {
    static $gtype: GObject.GType<SparseMemoryBindFlagBits>;

    constructor(copy: SparseMemoryBindFlagBits);
}

export class SparseMemoryBindFlags {
    static $gtype: GObject.GType<SparseMemoryBindFlags>;

    constructor(copy: SparseMemoryBindFlags);
}

export class SparseImageFormatFlagBits {
    static $gtype: GObject.GType<SparseImageFormatFlagBits>;

    constructor(copy: SparseImageFormatFlagBits);
}

export class SparseImageFormatFlags {
    static $gtype: GObject.GType<SparseImageFormatFlags>;

    constructor(copy: SparseImageFormatFlags);
}

export class FenceCreateFlagBits {
    static $gtype: GObject.GType<FenceCreateFlagBits>;

    constructor(copy: FenceCreateFlagBits);
}

export class FenceCreateFlags {
    static $gtype: GObject.GType<FenceCreateFlags>;

    constructor(copy: FenceCreateFlags);
}

export class SemaphoreCreateFlags {
    static $gtype: GObject.GType<SemaphoreCreateFlags>;

    constructor(copy: SemaphoreCreateFlags);
}

export class EventCreateFlagBits {
    static $gtype: GObject.GType<EventCreateFlagBits>;

    constructor(copy: EventCreateFlagBits);
}

export class EventCreateFlags {
    static $gtype: GObject.GType<EventCreateFlags>;

    constructor(copy: EventCreateFlags);
}

export class QueryPipelineStatisticFlagBits {
    static $gtype: GObject.GType<QueryPipelineStatisticFlagBits>;

    constructor(copy: QueryPipelineStatisticFlagBits);
}

export class QueryPipelineStatisticFlags {
    static $gtype: GObject.GType<QueryPipelineStatisticFlags>;

    constructor(copy: QueryPipelineStatisticFlags);
}

export class QueryPoolCreateFlags {
    static $gtype: GObject.GType<QueryPoolCreateFlags>;

    constructor(copy: QueryPoolCreateFlags);
}

export class QueryResultFlagBits {
    static $gtype: GObject.GType<QueryResultFlagBits>;

    constructor(copy: QueryResultFlagBits);
}

export class QueryResultFlags {
    static $gtype: GObject.GType<QueryResultFlags>;

    constructor(copy: QueryResultFlags);
}

export class BufferCreateFlagBits {
    static $gtype: GObject.GType<BufferCreateFlagBits>;

    constructor(copy: BufferCreateFlagBits);
}

export class BufferCreateFlags {
    static $gtype: GObject.GType<BufferCreateFlags>;

    constructor(copy: BufferCreateFlags);
}

export class BufferUsageFlagBits {
    static $gtype: GObject.GType<BufferUsageFlagBits>;

    constructor(copy: BufferUsageFlagBits);
}

export class BufferUsageFlags {
    static $gtype: GObject.GType<BufferUsageFlags>;

    constructor(copy: BufferUsageFlags);
}

export class BufferViewCreateFlags {
    static $gtype: GObject.GType<BufferViewCreateFlags>;

    constructor(copy: BufferViewCreateFlags);
}

export class ImageViewCreateFlagBits {
    static $gtype: GObject.GType<ImageViewCreateFlagBits>;

    constructor(copy: ImageViewCreateFlagBits);
}

export class ImageViewCreateFlags {
    static $gtype: GObject.GType<ImageViewCreateFlags>;

    constructor(copy: ImageViewCreateFlags);
}

export class ShaderModuleCreateFlags {
    static $gtype: GObject.GType<ShaderModuleCreateFlags>;

    constructor(copy: ShaderModuleCreateFlags);
}

export class PipelineCacheCreateFlagBits {
    static $gtype: GObject.GType<PipelineCacheCreateFlagBits>;

    constructor(copy: PipelineCacheCreateFlagBits);
}

export class PipelineCacheCreateFlags {
    static $gtype: GObject.GType<PipelineCacheCreateFlags>;

    constructor(copy: PipelineCacheCreateFlags);
}

export class ColorComponentFlagBits {
    static $gtype: GObject.GType<ColorComponentFlagBits>;

    constructor(copy: ColorComponentFlagBits);
}

export class ColorComponentFlags {
    static $gtype: GObject.GType<ColorComponentFlags>;

    constructor(copy: ColorComponentFlags);
}

export class PipelineCreateFlagBits {
    static $gtype: GObject.GType<PipelineCreateFlagBits>;

    constructor(copy: PipelineCreateFlagBits);
}

export class PipelineCreateFlags {
    static $gtype: GObject.GType<PipelineCreateFlags>;

    constructor(copy: PipelineCreateFlags);
}

export class PipelineShaderStageCreateFlagBits {
    static $gtype: GObject.GType<PipelineShaderStageCreateFlagBits>;

    constructor(copy: PipelineShaderStageCreateFlagBits);
}

export class PipelineShaderStageCreateFlags {
    static $gtype: GObject.GType<PipelineShaderStageCreateFlags>;

    constructor(copy: PipelineShaderStageCreateFlags);
}

export class ShaderStageFlagBits {
    static $gtype: GObject.GType<ShaderStageFlagBits>;

    constructor(copy: ShaderStageFlagBits);
}

export class CullModeFlagBits {
    static $gtype: GObject.GType<CullModeFlagBits>;

    constructor(copy: CullModeFlagBits);
}

export class CullModeFlags {
    static $gtype: GObject.GType<CullModeFlags>;

    constructor(copy: CullModeFlags);
}

export class PipelineVertexInputStateCreateFlags {
    static $gtype: GObject.GType<PipelineVertexInputStateCreateFlags>;

    constructor(copy: PipelineVertexInputStateCreateFlags);
}

export class PipelineInputAssemblyStateCreateFlags {
    static $gtype: GObject.GType<PipelineInputAssemblyStateCreateFlags>;

    constructor(copy: PipelineInputAssemblyStateCreateFlags);
}

export class PipelineTessellationStateCreateFlags {
    static $gtype: GObject.GType<PipelineTessellationStateCreateFlags>;

    constructor(copy: PipelineTessellationStateCreateFlags);
}

export class PipelineViewportStateCreateFlags {
    static $gtype: GObject.GType<PipelineViewportStateCreateFlags>;

    constructor(copy: PipelineViewportStateCreateFlags);
}

export class PipelineRasterizationStateCreateFlags {
    static $gtype: GObject.GType<PipelineRasterizationStateCreateFlags>;

    constructor(copy: PipelineRasterizationStateCreateFlags);
}

export class PipelineMultisampleStateCreateFlags {
    static $gtype: GObject.GType<PipelineMultisampleStateCreateFlags>;

    constructor(copy: PipelineMultisampleStateCreateFlags);
}

export class PipelineDepthStencilStateCreateFlagBits {
    static $gtype: GObject.GType<PipelineDepthStencilStateCreateFlagBits>;

    constructor(copy: PipelineDepthStencilStateCreateFlagBits);
}

export class PipelineDepthStencilStateCreateFlags {
    static $gtype: GObject.GType<PipelineDepthStencilStateCreateFlags>;

    constructor(copy: PipelineDepthStencilStateCreateFlags);
}

export class PipelineColorBlendStateCreateFlagBits {
    static $gtype: GObject.GType<PipelineColorBlendStateCreateFlagBits>;

    constructor(copy: PipelineColorBlendStateCreateFlagBits);
}

export class PipelineColorBlendStateCreateFlags {
    static $gtype: GObject.GType<PipelineColorBlendStateCreateFlags>;

    constructor(copy: PipelineColorBlendStateCreateFlags);
}

export class PipelineDynamicStateCreateFlags {
    static $gtype: GObject.GType<PipelineDynamicStateCreateFlags>;

    constructor(copy: PipelineDynamicStateCreateFlags);
}

export class PipelineLayoutCreateFlagBits {
    static $gtype: GObject.GType<PipelineLayoutCreateFlagBits>;

    constructor(copy: PipelineLayoutCreateFlagBits);
}

export class PipelineLayoutCreateFlags {
    static $gtype: GObject.GType<PipelineLayoutCreateFlags>;

    constructor(copy: PipelineLayoutCreateFlags);
}

export class ShaderStageFlags {
    static $gtype: GObject.GType<ShaderStageFlags>;

    constructor(copy: ShaderStageFlags);
}

export class SamplerCreateFlagBits {
    static $gtype: GObject.GType<SamplerCreateFlagBits>;

    constructor(copy: SamplerCreateFlagBits);
}

export class SamplerCreateFlags {
    static $gtype: GObject.GType<SamplerCreateFlags>;

    constructor(copy: SamplerCreateFlags);
}

export class DescriptorPoolCreateFlagBits {
    static $gtype: GObject.GType<DescriptorPoolCreateFlagBits>;

    constructor(copy: DescriptorPoolCreateFlagBits);
}

export class DescriptorPoolCreateFlags {
    static $gtype: GObject.GType<DescriptorPoolCreateFlags>;

    constructor(copy: DescriptorPoolCreateFlags);
}

export class DescriptorPoolResetFlags {
    static $gtype: GObject.GType<DescriptorPoolResetFlags>;

    constructor(copy: DescriptorPoolResetFlags);
}

export class DescriptorSetLayoutCreateFlagBits {
    static $gtype: GObject.GType<DescriptorSetLayoutCreateFlagBits>;

    constructor(copy: DescriptorSetLayoutCreateFlagBits);
}

export class DescriptorSetLayoutCreateFlags {
    static $gtype: GObject.GType<DescriptorSetLayoutCreateFlags>;

    constructor(copy: DescriptorSetLayoutCreateFlags);
}

export class AttachmentDescriptionFlagBits {
    static $gtype: GObject.GType<AttachmentDescriptionFlagBits>;

    constructor(copy: AttachmentDescriptionFlagBits);
}

export class AttachmentDescriptionFlags {
    static $gtype: GObject.GType<AttachmentDescriptionFlags>;

    constructor(copy: AttachmentDescriptionFlags);
}

export class DependencyFlagBits {
    static $gtype: GObject.GType<DependencyFlagBits>;

    constructor(copy: DependencyFlagBits);
}

export class DependencyFlags {
    static $gtype: GObject.GType<DependencyFlags>;

    constructor(copy: DependencyFlags);
}

export class FramebufferCreateFlagBits {
    static $gtype: GObject.GType<FramebufferCreateFlagBits>;

    constructor(copy: FramebufferCreateFlagBits);
}

export class FramebufferCreateFlags {
    static $gtype: GObject.GType<FramebufferCreateFlags>;

    constructor(copy: FramebufferCreateFlags);
}

export class RenderPassCreateFlagBits {
    static $gtype: GObject.GType<RenderPassCreateFlagBits>;

    constructor(copy: RenderPassCreateFlagBits);
}

export class RenderPassCreateFlags {
    static $gtype: GObject.GType<RenderPassCreateFlags>;

    constructor(copy: RenderPassCreateFlags);
}

export class SubpassDescriptionFlagBits {
    static $gtype: GObject.GType<SubpassDescriptionFlagBits>;

    constructor(copy: SubpassDescriptionFlagBits);
}

export class SubpassDescriptionFlags {
    static $gtype: GObject.GType<SubpassDescriptionFlags>;

    constructor(copy: SubpassDescriptionFlags);
}

export class CommandPoolCreateFlagBits {
    static $gtype: GObject.GType<CommandPoolCreateFlagBits>;

    constructor(copy: CommandPoolCreateFlagBits);
}

export class CommandPoolCreateFlags {
    static $gtype: GObject.GType<CommandPoolCreateFlags>;

    constructor(copy: CommandPoolCreateFlags);
}

export class CommandPoolResetFlagBits {
    static $gtype: GObject.GType<CommandPoolResetFlagBits>;

    constructor(copy: CommandPoolResetFlagBits);
}

export class CommandPoolResetFlags {
    static $gtype: GObject.GType<CommandPoolResetFlags>;

    constructor(copy: CommandPoolResetFlags);
}

export class CommandBufferUsageFlagBits {
    static $gtype: GObject.GType<CommandBufferUsageFlagBits>;

    constructor(copy: CommandBufferUsageFlagBits);
}

export class CommandBufferUsageFlags {
    static $gtype: GObject.GType<CommandBufferUsageFlags>;

    constructor(copy: CommandBufferUsageFlags);
}

export class QueryControlFlagBits {
    static $gtype: GObject.GType<QueryControlFlagBits>;

    constructor(copy: QueryControlFlagBits);
}

export class QueryControlFlags {
    static $gtype: GObject.GType<QueryControlFlags>;

    constructor(copy: QueryControlFlags);
}

export class CommandBufferResetFlagBits {
    static $gtype: GObject.GType<CommandBufferResetFlagBits>;

    constructor(copy: CommandBufferResetFlagBits);
}

export class CommandBufferResetFlags {
    static $gtype: GObject.GType<CommandBufferResetFlags>;

    constructor(copy: CommandBufferResetFlags);
}

export class StencilFaceFlagBits {
    static $gtype: GObject.GType<StencilFaceFlagBits>;

    constructor(copy: StencilFaceFlagBits);
}

export class StencilFaceFlags {
    static $gtype: GObject.GType<StencilFaceFlags>;

    constructor(copy: StencilFaceFlags);
}

export class Extent2D {
    static $gtype: GObject.GType<Extent2D>;

    constructor(copy: Extent2D);
}

export class Extent3D {
    static $gtype: GObject.GType<Extent3D>;

    constructor(copy: Extent3D);
}

export class Offset2D {
    static $gtype: GObject.GType<Offset2D>;

    constructor(copy: Offset2D);
}

export class Offset3D {
    static $gtype: GObject.GType<Offset3D>;

    constructor(copy: Offset3D);
}

export class Rect2D {
    static $gtype: GObject.GType<Rect2D>;

    constructor(copy: Rect2D);
}

export class BaseInStructure {
    static $gtype: GObject.GType<BaseInStructure>;

    constructor(copy: BaseInStructure);
}

export class BaseOutStructure {
    static $gtype: GObject.GType<BaseOutStructure>;

    constructor(copy: BaseOutStructure);
}

export class BufferMemoryBarrier {
    static $gtype: GObject.GType<BufferMemoryBarrier>;

    constructor(copy: BufferMemoryBarrier);
}

export class DispatchIndirectCommand {
    static $gtype: GObject.GType<DispatchIndirectCommand>;

    constructor(copy: DispatchIndirectCommand);
}

export class DrawIndexedIndirectCommand {
    static $gtype: GObject.GType<DrawIndexedIndirectCommand>;

    constructor(copy: DrawIndexedIndirectCommand);
}

export class DrawIndirectCommand {
    static $gtype: GObject.GType<DrawIndirectCommand>;

    constructor(copy: DrawIndirectCommand);
}

export class ImageSubresourceRange {
    static $gtype: GObject.GType<ImageSubresourceRange>;

    constructor(copy: ImageSubresourceRange);
}

export class ImageMemoryBarrier {
    static $gtype: GObject.GType<ImageMemoryBarrier>;

    constructor(copy: ImageMemoryBarrier);
}

export class MemoryBarrier {
    static $gtype: GObject.GType<MemoryBarrier>;

    constructor(copy: MemoryBarrier);
}

export class PipelineCacheHeaderVersionOne {
    static $gtype: GObject.GType<PipelineCacheHeaderVersionOne>;

    constructor(copy: PipelineCacheHeaderVersionOne);
}

export class AllocationCallbacks {
    static $gtype: GObject.GType<AllocationCallbacks>;

    constructor(copy: AllocationCallbacks);
}

export class ApplicationInfo {
    static $gtype: GObject.GType<ApplicationInfo>;

    constructor(copy: ApplicationInfo);
}

export class FormatProperties {
    static $gtype: GObject.GType<FormatProperties>;

    constructor(copy: FormatProperties);
}

export class ImageFormatProperties {
    static $gtype: GObject.GType<ImageFormatProperties>;

    constructor(copy: ImageFormatProperties);
}

export class InstanceCreateInfo {
    static $gtype: GObject.GType<InstanceCreateInfo>;

    constructor(copy: InstanceCreateInfo);
}

export class MemoryHeap {
    static $gtype: GObject.GType<MemoryHeap>;

    constructor(copy: MemoryHeap);
}

export class MemoryType {
    static $gtype: GObject.GType<MemoryType>;

    constructor(copy: MemoryType);
}

export class PhysicalDeviceFeatures {
    static $gtype: GObject.GType<PhysicalDeviceFeatures>;

    constructor(copy: PhysicalDeviceFeatures);
}

export class PhysicalDeviceLimits {
    static $gtype: GObject.GType<PhysicalDeviceLimits>;

    constructor(copy: PhysicalDeviceLimits);
}

export class PhysicalDeviceMemoryProperties {
    static $gtype: GObject.GType<PhysicalDeviceMemoryProperties>;

    constructor(copy: PhysicalDeviceMemoryProperties);
}

export class PhysicalDeviceSparseProperties {
    static $gtype: GObject.GType<PhysicalDeviceSparseProperties>;

    constructor(copy: PhysicalDeviceSparseProperties);
}

export class PhysicalDeviceProperties {
    static $gtype: GObject.GType<PhysicalDeviceProperties>;

    constructor(copy: PhysicalDeviceProperties);
}

export class QueueFamilyProperties {
    static $gtype: GObject.GType<QueueFamilyProperties>;

    constructor(copy: QueueFamilyProperties);
}

export class DeviceQueueCreateInfo {
    static $gtype: GObject.GType<DeviceQueueCreateInfo>;

    constructor(copy: DeviceQueueCreateInfo);
}

export class DeviceCreateInfo {
    static $gtype: GObject.GType<DeviceCreateInfo>;

    constructor(copy: DeviceCreateInfo);
}

export class ExtensionProperties {
    static $gtype: GObject.GType<ExtensionProperties>;

    constructor(copy: ExtensionProperties);
}

export class LayerProperties {
    static $gtype: GObject.GType<LayerProperties>;

    constructor(copy: LayerProperties);
}

export class SubmitInfo {
    static $gtype: GObject.GType<SubmitInfo>;

    constructor(copy: SubmitInfo);
}

export class MappedMemoryRange {
    static $gtype: GObject.GType<MappedMemoryRange>;

    constructor(copy: MappedMemoryRange);
}

export class MemoryAllocateInfo {
    static $gtype: GObject.GType<MemoryAllocateInfo>;

    constructor(copy: MemoryAllocateInfo);
}

export class MemoryRequirements {
    static $gtype: GObject.GType<MemoryRequirements>;

    constructor(copy: MemoryRequirements);
}

export class SparseMemoryBind {
    static $gtype: GObject.GType<SparseMemoryBind>;

    constructor(copy: SparseMemoryBind);
}

export class SparseBufferMemoryBindInfo {
    static $gtype: GObject.GType<SparseBufferMemoryBindInfo>;

    constructor(copy: SparseBufferMemoryBindInfo);
}

export class SparseImageOpaqueMemoryBindInfo {
    static $gtype: GObject.GType<SparseImageOpaqueMemoryBindInfo>;

    constructor(copy: SparseImageOpaqueMemoryBindInfo);
}

export class ImageSubresource {
    static $gtype: GObject.GType<ImageSubresource>;

    constructor(copy: ImageSubresource);
}

export class SparseImageMemoryBind {
    static $gtype: GObject.GType<SparseImageMemoryBind>;

    constructor(copy: SparseImageMemoryBind);
}

export class SparseImageMemoryBindInfo {
    static $gtype: GObject.GType<SparseImageMemoryBindInfo>;

    constructor(copy: SparseImageMemoryBindInfo);
}

export class BindSparseInfo {
    static $gtype: GObject.GType<BindSparseInfo>;

    constructor(copy: BindSparseInfo);
}

export class SparseImageFormatProperties {
    static $gtype: GObject.GType<SparseImageFormatProperties>;

    constructor(copy: SparseImageFormatProperties);
}

export class SparseImageMemoryRequirements {
    static $gtype: GObject.GType<SparseImageMemoryRequirements>;

    constructor(copy: SparseImageMemoryRequirements);
}

export class FenceCreateInfo {
    static $gtype: GObject.GType<FenceCreateInfo>;

    constructor(copy: FenceCreateInfo);
}

export class SemaphoreCreateInfo {
    static $gtype: GObject.GType<SemaphoreCreateInfo>;

    constructor(copy: SemaphoreCreateInfo);
}

export class EventCreateInfo {
    static $gtype: GObject.GType<EventCreateInfo>;

    constructor(copy: EventCreateInfo);
}

export class QueryPoolCreateInfo {
    static $gtype: GObject.GType<QueryPoolCreateInfo>;

    constructor(copy: QueryPoolCreateInfo);
}

export class BufferCreateInfo {
    static $gtype: GObject.GType<BufferCreateInfo>;

    constructor(copy: BufferCreateInfo);
}

export class BufferViewCreateInfo {
    static $gtype: GObject.GType<BufferViewCreateInfo>;

    constructor(copy: BufferViewCreateInfo);
}

export class ImageCreateInfo {
    static $gtype: GObject.GType<ImageCreateInfo>;

    constructor(copy: ImageCreateInfo);
}

export class SubresourceLayout {
    static $gtype: GObject.GType<SubresourceLayout>;

    constructor(copy: SubresourceLayout);
}

export class ComponentMapping {
    static $gtype: GObject.GType<ComponentMapping>;

    constructor(copy: ComponentMapping);
}

export class ImageViewCreateInfo {
    static $gtype: GObject.GType<ImageViewCreateInfo>;

    constructor(copy: ImageViewCreateInfo);
}

export class ShaderModuleCreateInfo {
    static $gtype: GObject.GType<ShaderModuleCreateInfo>;

    constructor(copy: ShaderModuleCreateInfo);
}

export class PipelineCacheCreateInfo {
    static $gtype: GObject.GType<PipelineCacheCreateInfo>;

    constructor(copy: PipelineCacheCreateInfo);
}

export class SpecializationMapEntry {
    static $gtype: GObject.GType<SpecializationMapEntry>;

    constructor(copy: SpecializationMapEntry);
}

export class SpecializationInfo {
    static $gtype: GObject.GType<SpecializationInfo>;

    constructor(copy: SpecializationInfo);
}

export class PipelineShaderStageCreateInfo {
    static $gtype: GObject.GType<PipelineShaderStageCreateInfo>;

    constructor(copy: PipelineShaderStageCreateInfo);
}

export class ComputePipelineCreateInfo {
    static $gtype: GObject.GType<ComputePipelineCreateInfo>;

    constructor(copy: ComputePipelineCreateInfo);
}

export class VertexInputBindingDescription {
    static $gtype: GObject.GType<VertexInputBindingDescription>;

    constructor(copy: VertexInputBindingDescription);
}

export class VertexInputAttributeDescription {
    static $gtype: GObject.GType<VertexInputAttributeDescription>;

    constructor(copy: VertexInputAttributeDescription);
}

export class PipelineVertexInputStateCreateInfo {
    static $gtype: GObject.GType<PipelineVertexInputStateCreateInfo>;

    constructor(copy: PipelineVertexInputStateCreateInfo);
}

export class PipelineInputAssemblyStateCreateInfo {
    static $gtype: GObject.GType<PipelineInputAssemblyStateCreateInfo>;

    constructor(copy: PipelineInputAssemblyStateCreateInfo);
}

export class PipelineTessellationStateCreateInfo {
    static $gtype: GObject.GType<PipelineTessellationStateCreateInfo>;

    constructor(copy: PipelineTessellationStateCreateInfo);
}

export class Viewport {
    static $gtype: GObject.GType<Viewport>;

    constructor(copy: Viewport);
}

export class PipelineViewportStateCreateInfo {
    static $gtype: GObject.GType<PipelineViewportStateCreateInfo>;

    constructor(copy: PipelineViewportStateCreateInfo);
}

export class PipelineRasterizationStateCreateInfo {
    static $gtype: GObject.GType<PipelineRasterizationStateCreateInfo>;

    constructor(copy: PipelineRasterizationStateCreateInfo);
}

export class PipelineMultisampleStateCreateInfo {
    static $gtype: GObject.GType<PipelineMultisampleStateCreateInfo>;

    constructor(copy: PipelineMultisampleStateCreateInfo);
}

export class StencilOpState {
    static $gtype: GObject.GType<StencilOpState>;

    constructor(copy: StencilOpState);
}

export class PipelineDepthStencilStateCreateInfo {
    static $gtype: GObject.GType<PipelineDepthStencilStateCreateInfo>;

    constructor(copy: PipelineDepthStencilStateCreateInfo);
}

export class PipelineColorBlendAttachmentState {
    static $gtype: GObject.GType<PipelineColorBlendAttachmentState>;

    constructor(copy: PipelineColorBlendAttachmentState);
}

export class PipelineColorBlendStateCreateInfo {
    static $gtype: GObject.GType<PipelineColorBlendStateCreateInfo>;

    constructor(copy: PipelineColorBlendStateCreateInfo);
}

export class PipelineDynamicStateCreateInfo {
    static $gtype: GObject.GType<PipelineDynamicStateCreateInfo>;

    constructor(copy: PipelineDynamicStateCreateInfo);
}

export class GraphicsPipelineCreateInfo {
    static $gtype: GObject.GType<GraphicsPipelineCreateInfo>;

    constructor(copy: GraphicsPipelineCreateInfo);
}

export class PushConstantRange {
    static $gtype: GObject.GType<PushConstantRange>;

    constructor(copy: PushConstantRange);
}

export class PipelineLayoutCreateInfo {
    static $gtype: GObject.GType<PipelineLayoutCreateInfo>;

    constructor(copy: PipelineLayoutCreateInfo);
}

export class SamplerCreateInfo {
    static $gtype: GObject.GType<SamplerCreateInfo>;

    constructor(copy: SamplerCreateInfo);
}

export class CopyDescriptorSet {
    static $gtype: GObject.GType<CopyDescriptorSet>;

    constructor(copy: CopyDescriptorSet);
}

export class DescriptorBufferInfo {
    static $gtype: GObject.GType<DescriptorBufferInfo>;

    constructor(copy: DescriptorBufferInfo);
}

export class DescriptorImageInfo {
    static $gtype: GObject.GType<DescriptorImageInfo>;

    constructor(copy: DescriptorImageInfo);
}

export class DescriptorPoolSize {
    static $gtype: GObject.GType<DescriptorPoolSize>;

    constructor(copy: DescriptorPoolSize);
}

export class DescriptorPoolCreateInfo {
    static $gtype: GObject.GType<DescriptorPoolCreateInfo>;

    constructor(copy: DescriptorPoolCreateInfo);
}

export class DescriptorSetAllocateInfo {
    static $gtype: GObject.GType<DescriptorSetAllocateInfo>;

    constructor(copy: DescriptorSetAllocateInfo);
}

export class DescriptorSetLayoutBinding {
    static $gtype: GObject.GType<DescriptorSetLayoutBinding>;

    constructor(copy: DescriptorSetLayoutBinding);
}

export class DescriptorSetLayoutCreateInfo {
    static $gtype: GObject.GType<DescriptorSetLayoutCreateInfo>;

    constructor(copy: DescriptorSetLayoutCreateInfo);
}

export class WriteDescriptorSet {
    static $gtype: GObject.GType<WriteDescriptorSet>;

    constructor(copy: WriteDescriptorSet);
}

export class AttachmentDescription {
    static $gtype: GObject.GType<AttachmentDescription>;

    constructor(copy: AttachmentDescription);
}

export class AttachmentReference {
    static $gtype: GObject.GType<AttachmentReference>;

    constructor(copy: AttachmentReference);
}

export class FramebufferCreateInfo {
    static $gtype: GObject.GType<FramebufferCreateInfo>;

    constructor(copy: FramebufferCreateInfo);
}

export class SubpassDescription {
    static $gtype: GObject.GType<SubpassDescription>;

    constructor(copy: SubpassDescription);
}

export class SubpassDependency {
    static $gtype: GObject.GType<SubpassDependency>;

    constructor(copy: SubpassDependency);
}

export class RenderPassCreateInfo {
    static $gtype: GObject.GType<RenderPassCreateInfo>;

    constructor(copy: RenderPassCreateInfo);
}

export class CommandPoolCreateInfo {
    static $gtype: GObject.GType<CommandPoolCreateInfo>;

    constructor(copy: CommandPoolCreateInfo);
}

export class CommandBufferAllocateInfo {
    static $gtype: GObject.GType<CommandBufferAllocateInfo>;

    constructor(copy: CommandBufferAllocateInfo);
}

export class CommandBufferInheritanceInfo {
    static $gtype: GObject.GType<CommandBufferInheritanceInfo>;

    constructor(copy: CommandBufferInheritanceInfo);
}

export class CommandBufferBeginInfo {
    static $gtype: GObject.GType<CommandBufferBeginInfo>;

    constructor(copy: CommandBufferBeginInfo);
}

export class BufferCopy {
    static $gtype: GObject.GType<BufferCopy>;

    constructor(copy: BufferCopy);
}

export class ImageSubresourceLayers {
    static $gtype: GObject.GType<ImageSubresourceLayers>;

    constructor(copy: ImageSubresourceLayers);
}

export class BufferImageCopy {
    static $gtype: GObject.GType<BufferImageCopy>;

    constructor(copy: BufferImageCopy);
}

export class ClearColorValue {
    static $gtype: GObject.GType<ClearColorValue>;

    constructor(copy: ClearColorValue);
}

export class ClearDepthStencilValue {
    static $gtype: GObject.GType<ClearDepthStencilValue>;

    constructor(copy: ClearDepthStencilValue);
}

export class ClearValue {
    static $gtype: GObject.GType<ClearValue>;

    constructor(copy: ClearValue);
}

export class ClearAttachment {
    static $gtype: GObject.GType<ClearAttachment>;

    constructor(copy: ClearAttachment);
}

export class ClearRect {
    static $gtype: GObject.GType<ClearRect>;

    constructor(copy: ClearRect);
}

export class ImageBlit {
    static $gtype: GObject.GType<ImageBlit>;

    constructor(copy: ImageBlit);
}

export class ImageCopy {
    static $gtype: GObject.GType<ImageCopy>;

    constructor(copy: ImageCopy);
}

export class ImageResolve {
    static $gtype: GObject.GType<ImageResolve>;

    constructor(copy: ImageResolve);
}

export class RenderPassBeginInfo {
    static $gtype: GObject.GType<RenderPassBeginInfo>;

    constructor(copy: RenderPassBeginInfo);
}

export class SamplerYcbcrConversion {
    static $gtype: GObject.GType<SamplerYcbcrConversion>;

    constructor(copy: SamplerYcbcrConversion);
}

export class DescriptorUpdateTemplate {
    static $gtype: GObject.GType<DescriptorUpdateTemplate>;

    constructor(copy: DescriptorUpdateTemplate);
}

export class PointClippingBehavior {
    static $gtype: GObject.GType<PointClippingBehavior>;

    constructor(copy: PointClippingBehavior);
}

export class TessellationDomainOrigin {
    static $gtype: GObject.GType<TessellationDomainOrigin>;

    constructor(copy: TessellationDomainOrigin);
}

export class SamplerYcbcrModelConversion {
    static $gtype: GObject.GType<SamplerYcbcrModelConversion>;

    constructor(copy: SamplerYcbcrModelConversion);
}

export class SamplerYcbcrRange {
    static $gtype: GObject.GType<SamplerYcbcrRange>;

    constructor(copy: SamplerYcbcrRange);
}

export class ChromaLocation {
    static $gtype: GObject.GType<ChromaLocation>;

    constructor(copy: ChromaLocation);
}

export class DescriptorUpdateTemplateType {
    static $gtype: GObject.GType<DescriptorUpdateTemplateType>;

    constructor(copy: DescriptorUpdateTemplateType);
}

export class SubgroupFeatureFlagBits {
    static $gtype: GObject.GType<SubgroupFeatureFlagBits>;

    constructor(copy: SubgroupFeatureFlagBits);
}

export class SubgroupFeatureFlags {
    static $gtype: GObject.GType<SubgroupFeatureFlags>;

    constructor(copy: SubgroupFeatureFlags);
}

export class PeerMemoryFeatureFlagBits {
    static $gtype: GObject.GType<PeerMemoryFeatureFlagBits>;

    constructor(copy: PeerMemoryFeatureFlagBits);
}

export class PeerMemoryFeatureFlags {
    static $gtype: GObject.GType<PeerMemoryFeatureFlags>;

    constructor(copy: PeerMemoryFeatureFlags);
}

export class MemoryAllocateFlagBits {
    static $gtype: GObject.GType<MemoryAllocateFlagBits>;

    constructor(copy: MemoryAllocateFlagBits);
}

export class MemoryAllocateFlags {
    static $gtype: GObject.GType<MemoryAllocateFlags>;

    constructor(copy: MemoryAllocateFlags);
}

export class CommandPoolTrimFlags {
    static $gtype: GObject.GType<CommandPoolTrimFlags>;

    constructor(copy: CommandPoolTrimFlags);
}

export class DescriptorUpdateTemplateCreateFlags {
    static $gtype: GObject.GType<DescriptorUpdateTemplateCreateFlags>;

    constructor(copy: DescriptorUpdateTemplateCreateFlags);
}

export class ExternalMemoryHandleTypeFlagBits {
    static $gtype: GObject.GType<ExternalMemoryHandleTypeFlagBits>;

    constructor(copy: ExternalMemoryHandleTypeFlagBits);
}

export class ExternalMemoryHandleTypeFlags {
    static $gtype: GObject.GType<ExternalMemoryHandleTypeFlags>;

    constructor(copy: ExternalMemoryHandleTypeFlags);
}

export class ExternalMemoryFeatureFlagBits {
    static $gtype: GObject.GType<ExternalMemoryFeatureFlagBits>;

    constructor(copy: ExternalMemoryFeatureFlagBits);
}

export class ExternalMemoryFeatureFlags {
    static $gtype: GObject.GType<ExternalMemoryFeatureFlags>;

    constructor(copy: ExternalMemoryFeatureFlags);
}

export class ExternalFenceHandleTypeFlagBits {
    static $gtype: GObject.GType<ExternalFenceHandleTypeFlagBits>;

    constructor(copy: ExternalFenceHandleTypeFlagBits);
}

export class ExternalFenceHandleTypeFlags {
    static $gtype: GObject.GType<ExternalFenceHandleTypeFlags>;

    constructor(copy: ExternalFenceHandleTypeFlags);
}

export class ExternalFenceFeatureFlagBits {
    static $gtype: GObject.GType<ExternalFenceFeatureFlagBits>;

    constructor(copy: ExternalFenceFeatureFlagBits);
}

export class ExternalFenceFeatureFlags {
    static $gtype: GObject.GType<ExternalFenceFeatureFlags>;

    constructor(copy: ExternalFenceFeatureFlags);
}

export class FenceImportFlagBits {
    static $gtype: GObject.GType<FenceImportFlagBits>;

    constructor(copy: FenceImportFlagBits);
}

export class FenceImportFlags {
    static $gtype: GObject.GType<FenceImportFlags>;

    constructor(copy: FenceImportFlags);
}

export class SemaphoreImportFlagBits {
    static $gtype: GObject.GType<SemaphoreImportFlagBits>;

    constructor(copy: SemaphoreImportFlagBits);
}

export class SemaphoreImportFlags {
    static $gtype: GObject.GType<SemaphoreImportFlags>;

    constructor(copy: SemaphoreImportFlags);
}

export class ExternalSemaphoreHandleTypeFlagBits {
    static $gtype: GObject.GType<ExternalSemaphoreHandleTypeFlagBits>;

    constructor(copy: ExternalSemaphoreHandleTypeFlagBits);
}

export class ExternalSemaphoreHandleTypeFlags {
    static $gtype: GObject.GType<ExternalSemaphoreHandleTypeFlags>;

    constructor(copy: ExternalSemaphoreHandleTypeFlags);
}

export class ExternalSemaphoreFeatureFlagBits {
    static $gtype: GObject.GType<ExternalSemaphoreFeatureFlagBits>;

    constructor(copy: ExternalSemaphoreFeatureFlagBits);
}

export class ExternalSemaphoreFeatureFlags {
    static $gtype: GObject.GType<ExternalSemaphoreFeatureFlags>;

    constructor(copy: ExternalSemaphoreFeatureFlags);
}

export class PhysicalDeviceSubgroupProperties {
    static $gtype: GObject.GType<PhysicalDeviceSubgroupProperties>;

    constructor(copy: PhysicalDeviceSubgroupProperties);
}

export class BindBufferMemoryInfo {
    static $gtype: GObject.GType<BindBufferMemoryInfo>;

    constructor(copy: BindBufferMemoryInfo);
}

export class BindImageMemoryInfo {
    static $gtype: GObject.GType<BindImageMemoryInfo>;

    constructor(copy: BindImageMemoryInfo);
}

export class PhysicalDevice16BitStorageFeatures {
    static $gtype: GObject.GType<PhysicalDevice16BitStorageFeatures>;

    constructor(copy: PhysicalDevice16BitStorageFeatures);
}

export class MemoryDedicatedRequirements {
    static $gtype: GObject.GType<MemoryDedicatedRequirements>;

    constructor(copy: MemoryDedicatedRequirements);
}

export class MemoryDedicatedAllocateInfo {
    static $gtype: GObject.GType<MemoryDedicatedAllocateInfo>;

    constructor(copy: MemoryDedicatedAllocateInfo);
}

export class MemoryAllocateFlagsInfo {
    static $gtype: GObject.GType<MemoryAllocateFlagsInfo>;

    constructor(copy: MemoryAllocateFlagsInfo);
}

export class DeviceGroupRenderPassBeginInfo {
    static $gtype: GObject.GType<DeviceGroupRenderPassBeginInfo>;

    constructor(copy: DeviceGroupRenderPassBeginInfo);
}

export class DeviceGroupCommandBufferBeginInfo {
    static $gtype: GObject.GType<DeviceGroupCommandBufferBeginInfo>;

    constructor(copy: DeviceGroupCommandBufferBeginInfo);
}

export class DeviceGroupSubmitInfo {
    static $gtype: GObject.GType<DeviceGroupSubmitInfo>;

    constructor(copy: DeviceGroupSubmitInfo);
}

export class DeviceGroupBindSparseInfo {
    static $gtype: GObject.GType<DeviceGroupBindSparseInfo>;

    constructor(copy: DeviceGroupBindSparseInfo);
}

export class BindBufferMemoryDeviceGroupInfo {
    static $gtype: GObject.GType<BindBufferMemoryDeviceGroupInfo>;

    constructor(copy: BindBufferMemoryDeviceGroupInfo);
}

export class BindImageMemoryDeviceGroupInfo {
    static $gtype: GObject.GType<BindImageMemoryDeviceGroupInfo>;

    constructor(copy: BindImageMemoryDeviceGroupInfo);
}

export class PhysicalDeviceGroupProperties {
    static $gtype: GObject.GType<PhysicalDeviceGroupProperties>;

    constructor(copy: PhysicalDeviceGroupProperties);
}

export class DeviceGroupDeviceCreateInfo {
    static $gtype: GObject.GType<DeviceGroupDeviceCreateInfo>;

    constructor(copy: DeviceGroupDeviceCreateInfo);
}

export class BufferMemoryRequirementsInfo2 {
    static $gtype: GObject.GType<BufferMemoryRequirementsInfo2>;

    constructor(copy: BufferMemoryRequirementsInfo2);
}

export class ImageMemoryRequirementsInfo2 {
    static $gtype: GObject.GType<ImageMemoryRequirementsInfo2>;

    constructor(copy: ImageMemoryRequirementsInfo2);
}

export class ImageSparseMemoryRequirementsInfo2 {
    static $gtype: GObject.GType<ImageSparseMemoryRequirementsInfo2>;

    constructor(copy: ImageSparseMemoryRequirementsInfo2);
}

export class MemoryRequirements2 {
    static $gtype: GObject.GType<MemoryRequirements2>;

    constructor(copy: MemoryRequirements2);
}

export class SparseImageMemoryRequirements2 {
    static $gtype: GObject.GType<SparseImageMemoryRequirements2>;

    constructor(copy: SparseImageMemoryRequirements2);
}

export class PhysicalDeviceFeatures2 {
    static $gtype: GObject.GType<PhysicalDeviceFeatures2>;

    constructor(copy: PhysicalDeviceFeatures2);
}

export class PhysicalDeviceProperties2 {
    static $gtype: GObject.GType<PhysicalDeviceProperties2>;

    constructor(copy: PhysicalDeviceProperties2);
}

export class FormatProperties2 {
    static $gtype: GObject.GType<FormatProperties2>;

    constructor(copy: FormatProperties2);
}

export class ImageFormatProperties2 {
    static $gtype: GObject.GType<ImageFormatProperties2>;

    constructor(copy: ImageFormatProperties2);
}

export class PhysicalDeviceImageFormatInfo2 {
    static $gtype: GObject.GType<PhysicalDeviceImageFormatInfo2>;

    constructor(copy: PhysicalDeviceImageFormatInfo2);
}

export class QueueFamilyProperties2 {
    static $gtype: GObject.GType<QueueFamilyProperties2>;

    constructor(copy: QueueFamilyProperties2);
}

export class PhysicalDeviceMemoryProperties2 {
    static $gtype: GObject.GType<PhysicalDeviceMemoryProperties2>;

    constructor(copy: PhysicalDeviceMemoryProperties2);
}

export class SparseImageFormatProperties2 {
    static $gtype: GObject.GType<SparseImageFormatProperties2>;

    constructor(copy: SparseImageFormatProperties2);
}

export class PhysicalDeviceSparseImageFormatInfo2 {
    static $gtype: GObject.GType<PhysicalDeviceSparseImageFormatInfo2>;

    constructor(copy: PhysicalDeviceSparseImageFormatInfo2);
}

export class PhysicalDevicePointClippingProperties {
    static $gtype: GObject.GType<PhysicalDevicePointClippingProperties>;

    constructor(copy: PhysicalDevicePointClippingProperties);
}

export class InputAttachmentAspectReference {
    static $gtype: GObject.GType<InputAttachmentAspectReference>;

    constructor(copy: InputAttachmentAspectReference);
}

export class RenderPassInputAttachmentAspectCreateInfo {
    static $gtype: GObject.GType<RenderPassInputAttachmentAspectCreateInfo>;

    constructor(copy: RenderPassInputAttachmentAspectCreateInfo);
}

export class ImageViewUsageCreateInfo {
    static $gtype: GObject.GType<ImageViewUsageCreateInfo>;

    constructor(copy: ImageViewUsageCreateInfo);
}

export class PipelineTessellationDomainOriginStateCreateInfo {
    static $gtype: GObject.GType<PipelineTessellationDomainOriginStateCreateInfo>;

    constructor(copy: PipelineTessellationDomainOriginStateCreateInfo);
}

export class RenderPassMultiviewCreateInfo {
    static $gtype: GObject.GType<RenderPassMultiviewCreateInfo>;

    constructor(copy: RenderPassMultiviewCreateInfo);
}

export class PhysicalDeviceMultiviewFeatures {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewFeatures>;

    constructor(copy: PhysicalDeviceMultiviewFeatures);
}

export class PhysicalDeviceMultiviewProperties {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewProperties>;

    constructor(copy: PhysicalDeviceMultiviewProperties);
}

export class PhysicalDeviceVariablePointersFeatures {
    static $gtype: GObject.GType<PhysicalDeviceVariablePointersFeatures>;

    constructor(copy: PhysicalDeviceVariablePointersFeatures);
}

export class PhysicalDeviceVariablePointerFeatures {
    static $gtype: GObject.GType<PhysicalDeviceVariablePointerFeatures>;

    constructor(copy: PhysicalDeviceVariablePointerFeatures);
}

export class PhysicalDeviceProtectedMemoryFeatures {
    static $gtype: GObject.GType<PhysicalDeviceProtectedMemoryFeatures>;

    constructor(copy: PhysicalDeviceProtectedMemoryFeatures);
}

export class PhysicalDeviceProtectedMemoryProperties {
    static $gtype: GObject.GType<PhysicalDeviceProtectedMemoryProperties>;

    constructor(copy: PhysicalDeviceProtectedMemoryProperties);
}

export class DeviceQueueInfo2 {
    static $gtype: GObject.GType<DeviceQueueInfo2>;

    constructor(copy: DeviceQueueInfo2);
}

export class ProtectedSubmitInfo {
    static $gtype: GObject.GType<ProtectedSubmitInfo>;

    constructor(copy: ProtectedSubmitInfo);
}

export class SamplerYcbcrConversionCreateInfo {
    static $gtype: GObject.GType<SamplerYcbcrConversionCreateInfo>;

    constructor(copy: SamplerYcbcrConversionCreateInfo);
}

export class SamplerYcbcrConversionInfo {
    static $gtype: GObject.GType<SamplerYcbcrConversionInfo>;

    constructor(copy: SamplerYcbcrConversionInfo);
}

export class BindImagePlaneMemoryInfo {
    static $gtype: GObject.GType<BindImagePlaneMemoryInfo>;

    constructor(copy: BindImagePlaneMemoryInfo);
}

export class ImagePlaneMemoryRequirementsInfo {
    static $gtype: GObject.GType<ImagePlaneMemoryRequirementsInfo>;

    constructor(copy: ImagePlaneMemoryRequirementsInfo);
}

export class PhysicalDeviceSamplerYcbcrConversionFeatures {
    static $gtype: GObject.GType<PhysicalDeviceSamplerYcbcrConversionFeatures>;

    constructor(copy: PhysicalDeviceSamplerYcbcrConversionFeatures);
}

export class SamplerYcbcrConversionImageFormatProperties {
    static $gtype: GObject.GType<SamplerYcbcrConversionImageFormatProperties>;

    constructor(copy: SamplerYcbcrConversionImageFormatProperties);
}

export class DescriptorUpdateTemplateEntry {
    static $gtype: GObject.GType<DescriptorUpdateTemplateEntry>;

    constructor(copy: DescriptorUpdateTemplateEntry);
}

export class DescriptorUpdateTemplateCreateInfo {
    static $gtype: GObject.GType<DescriptorUpdateTemplateCreateInfo>;

    constructor(copy: DescriptorUpdateTemplateCreateInfo);
}

export class ExternalMemoryProperties {
    static $gtype: GObject.GType<ExternalMemoryProperties>;

    constructor(copy: ExternalMemoryProperties);
}

export class PhysicalDeviceExternalImageFormatInfo {
    static $gtype: GObject.GType<PhysicalDeviceExternalImageFormatInfo>;

    constructor(copy: PhysicalDeviceExternalImageFormatInfo);
}

export class ExternalImageFormatProperties {
    static $gtype: GObject.GType<ExternalImageFormatProperties>;

    constructor(copy: ExternalImageFormatProperties);
}

export class PhysicalDeviceExternalBufferInfo {
    static $gtype: GObject.GType<PhysicalDeviceExternalBufferInfo>;

    constructor(copy: PhysicalDeviceExternalBufferInfo);
}

export class ExternalBufferProperties {
    static $gtype: GObject.GType<ExternalBufferProperties>;

    constructor(copy: ExternalBufferProperties);
}

export class PhysicalDeviceIDProperties {
    static $gtype: GObject.GType<PhysicalDeviceIDProperties>;

    constructor(copy: PhysicalDeviceIDProperties);
}

export class ExternalMemoryImageCreateInfo {
    static $gtype: GObject.GType<ExternalMemoryImageCreateInfo>;

    constructor(copy: ExternalMemoryImageCreateInfo);
}

export class ExternalMemoryBufferCreateInfo {
    static $gtype: GObject.GType<ExternalMemoryBufferCreateInfo>;

    constructor(copy: ExternalMemoryBufferCreateInfo);
}

export class ExportMemoryAllocateInfo {
    static $gtype: GObject.GType<ExportMemoryAllocateInfo>;

    constructor(copy: ExportMemoryAllocateInfo);
}

export class PhysicalDeviceExternalFenceInfo {
    static $gtype: GObject.GType<PhysicalDeviceExternalFenceInfo>;

    constructor(copy: PhysicalDeviceExternalFenceInfo);
}

export class ExternalFenceProperties {
    static $gtype: GObject.GType<ExternalFenceProperties>;

    constructor(copy: ExternalFenceProperties);
}

export class ExportFenceCreateInfo {
    static $gtype: GObject.GType<ExportFenceCreateInfo>;

    constructor(copy: ExportFenceCreateInfo);
}

export class ExportSemaphoreCreateInfo {
    static $gtype: GObject.GType<ExportSemaphoreCreateInfo>;

    constructor(copy: ExportSemaphoreCreateInfo);
}

export class PhysicalDeviceExternalSemaphoreInfo {
    static $gtype: GObject.GType<PhysicalDeviceExternalSemaphoreInfo>;

    constructor(copy: PhysicalDeviceExternalSemaphoreInfo);
}

export class ExternalSemaphoreProperties {
    static $gtype: GObject.GType<ExternalSemaphoreProperties>;

    constructor(copy: ExternalSemaphoreProperties);
}

export class PhysicalDeviceMaintenance3Properties {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance3Properties>;

    constructor(copy: PhysicalDeviceMaintenance3Properties);
}

export class DescriptorSetLayoutSupport {
    static $gtype: GObject.GType<DescriptorSetLayoutSupport>;

    constructor(copy: DescriptorSetLayoutSupport);
}

export class PhysicalDeviceShaderDrawParametersFeatures {
    static $gtype: GObject.GType<PhysicalDeviceShaderDrawParametersFeatures>;

    constructor(copy: PhysicalDeviceShaderDrawParametersFeatures);
}

export class PhysicalDeviceShaderDrawParameterFeatures {
    static $gtype: GObject.GType<PhysicalDeviceShaderDrawParameterFeatures>;

    constructor(copy: PhysicalDeviceShaderDrawParameterFeatures);
}

export class DriverId {
    static $gtype: GObject.GType<DriverId>;

    constructor(copy: DriverId);
}

export class ShaderFloatControlsIndependence {
    static $gtype: GObject.GType<ShaderFloatControlsIndependence>;

    constructor(copy: ShaderFloatControlsIndependence);
}

export class SamplerReductionMode {
    static $gtype: GObject.GType<SamplerReductionMode>;

    constructor(copy: SamplerReductionMode);
}

export class SemaphoreType {
    static $gtype: GObject.GType<SemaphoreType>;

    constructor(copy: SemaphoreType);
}

export class ResolveModeFlagBits {
    static $gtype: GObject.GType<ResolveModeFlagBits>;

    constructor(copy: ResolveModeFlagBits);
}

export class ResolveModeFlags {
    static $gtype: GObject.GType<ResolveModeFlags>;

    constructor(copy: ResolveModeFlags);
}

export class DescriptorBindingFlagBits {
    static $gtype: GObject.GType<DescriptorBindingFlagBits>;

    constructor(copy: DescriptorBindingFlagBits);
}

export class DescriptorBindingFlags {
    static $gtype: GObject.GType<DescriptorBindingFlags>;

    constructor(copy: DescriptorBindingFlags);
}

export class SemaphoreWaitFlagBits {
    static $gtype: GObject.GType<SemaphoreWaitFlagBits>;

    constructor(copy: SemaphoreWaitFlagBits);
}

export class SemaphoreWaitFlags {
    static $gtype: GObject.GType<SemaphoreWaitFlags>;

    constructor(copy: SemaphoreWaitFlags);
}

export class PhysicalDeviceVulkan11Features {
    static $gtype: GObject.GType<PhysicalDeviceVulkan11Features>;

    constructor(copy: PhysicalDeviceVulkan11Features);
}

export class PhysicalDeviceVulkan11Properties {
    static $gtype: GObject.GType<PhysicalDeviceVulkan11Properties>;

    constructor(copy: PhysicalDeviceVulkan11Properties);
}

export class PhysicalDeviceVulkan12Features {
    static $gtype: GObject.GType<PhysicalDeviceVulkan12Features>;

    constructor(copy: PhysicalDeviceVulkan12Features);
}

export class ConformanceVersion {
    static $gtype: GObject.GType<ConformanceVersion>;

    constructor(copy: ConformanceVersion);
}

export class PhysicalDeviceVulkan12Properties {
    static $gtype: GObject.GType<PhysicalDeviceVulkan12Properties>;

    constructor(copy: PhysicalDeviceVulkan12Properties);
}

export class ImageFormatListCreateInfo {
    static $gtype: GObject.GType<ImageFormatListCreateInfo>;

    constructor(copy: ImageFormatListCreateInfo);
}

export class AttachmentDescription2 {
    static $gtype: GObject.GType<AttachmentDescription2>;

    constructor(copy: AttachmentDescription2);
}

export class AttachmentReference2 {
    static $gtype: GObject.GType<AttachmentReference2>;

    constructor(copy: AttachmentReference2);
}

export class SubpassDescription2 {
    static $gtype: GObject.GType<SubpassDescription2>;

    constructor(copy: SubpassDescription2);
}

export class SubpassDependency2 {
    static $gtype: GObject.GType<SubpassDependency2>;

    constructor(copy: SubpassDependency2);
}

export class RenderPassCreateInfo2 {
    static $gtype: GObject.GType<RenderPassCreateInfo2>;

    constructor(copy: RenderPassCreateInfo2);
}

export class SubpassBeginInfo {
    static $gtype: GObject.GType<SubpassBeginInfo>;

    constructor(copy: SubpassBeginInfo);
}

export class SubpassEndInfo {
    static $gtype: GObject.GType<SubpassEndInfo>;

    constructor(copy: SubpassEndInfo);
}

export class PhysicalDevice8BitStorageFeatures {
    static $gtype: GObject.GType<PhysicalDevice8BitStorageFeatures>;

    constructor(copy: PhysicalDevice8BitStorageFeatures);
}

export class PhysicalDeviceDriverProperties {
    static $gtype: GObject.GType<PhysicalDeviceDriverProperties>;

    constructor(copy: PhysicalDeviceDriverProperties);
}

export class PhysicalDeviceShaderAtomicInt64Features {
    static $gtype: GObject.GType<PhysicalDeviceShaderAtomicInt64Features>;

    constructor(copy: PhysicalDeviceShaderAtomicInt64Features);
}

export class PhysicalDeviceShaderFloat16Int8Features {
    static $gtype: GObject.GType<PhysicalDeviceShaderFloat16Int8Features>;

    constructor(copy: PhysicalDeviceShaderFloat16Int8Features);
}

export class PhysicalDeviceFloatControlsProperties {
    static $gtype: GObject.GType<PhysicalDeviceFloatControlsProperties>;

    constructor(copy: PhysicalDeviceFloatControlsProperties);
}

export class DescriptorSetLayoutBindingFlagsCreateInfo {
    static $gtype: GObject.GType<DescriptorSetLayoutBindingFlagsCreateInfo>;

    constructor(copy: DescriptorSetLayoutBindingFlagsCreateInfo);
}

export class PhysicalDeviceDescriptorIndexingFeatures {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorIndexingFeatures>;

    constructor(copy: PhysicalDeviceDescriptorIndexingFeatures);
}

export class PhysicalDeviceDescriptorIndexingProperties {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorIndexingProperties>;

    constructor(copy: PhysicalDeviceDescriptorIndexingProperties);
}

export class DescriptorSetVariableDescriptorCountAllocateInfo {
    static $gtype: GObject.GType<DescriptorSetVariableDescriptorCountAllocateInfo>;

    constructor(copy: DescriptorSetVariableDescriptorCountAllocateInfo);
}

export class DescriptorSetVariableDescriptorCountLayoutSupport {
    static $gtype: GObject.GType<DescriptorSetVariableDescriptorCountLayoutSupport>;

    constructor(copy: DescriptorSetVariableDescriptorCountLayoutSupport);
}

export class SubpassDescriptionDepthStencilResolve {
    static $gtype: GObject.GType<SubpassDescriptionDepthStencilResolve>;

    constructor(copy: SubpassDescriptionDepthStencilResolve);
}

export class PhysicalDeviceDepthStencilResolveProperties {
    static $gtype: GObject.GType<PhysicalDeviceDepthStencilResolveProperties>;

    constructor(copy: PhysicalDeviceDepthStencilResolveProperties);
}

export class PhysicalDeviceScalarBlockLayoutFeatures {
    static $gtype: GObject.GType<PhysicalDeviceScalarBlockLayoutFeatures>;

    constructor(copy: PhysicalDeviceScalarBlockLayoutFeatures);
}

export class ImageStencilUsageCreateInfo {
    static $gtype: GObject.GType<ImageStencilUsageCreateInfo>;

    constructor(copy: ImageStencilUsageCreateInfo);
}

export class SamplerReductionModeCreateInfo {
    static $gtype: GObject.GType<SamplerReductionModeCreateInfo>;

    constructor(copy: SamplerReductionModeCreateInfo);
}

export class PhysicalDeviceSamplerFilterMinmaxProperties {
    static $gtype: GObject.GType<PhysicalDeviceSamplerFilterMinmaxProperties>;

    constructor(copy: PhysicalDeviceSamplerFilterMinmaxProperties);
}

export class PhysicalDeviceVulkanMemoryModelFeatures {
    static $gtype: GObject.GType<PhysicalDeviceVulkanMemoryModelFeatures>;

    constructor(copy: PhysicalDeviceVulkanMemoryModelFeatures);
}

export class PhysicalDeviceImagelessFramebufferFeatures {
    static $gtype: GObject.GType<PhysicalDeviceImagelessFramebufferFeatures>;

    constructor(copy: PhysicalDeviceImagelessFramebufferFeatures);
}

export class FramebufferAttachmentImageInfo {
    static $gtype: GObject.GType<FramebufferAttachmentImageInfo>;

    constructor(copy: FramebufferAttachmentImageInfo);
}

export class FramebufferAttachmentsCreateInfo {
    static $gtype: GObject.GType<FramebufferAttachmentsCreateInfo>;

    constructor(copy: FramebufferAttachmentsCreateInfo);
}

export class RenderPassAttachmentBeginInfo {
    static $gtype: GObject.GType<RenderPassAttachmentBeginInfo>;

    constructor(copy: RenderPassAttachmentBeginInfo);
}

export class PhysicalDeviceUniformBufferStandardLayoutFeatures {
    static $gtype: GObject.GType<PhysicalDeviceUniformBufferStandardLayoutFeatures>;

    constructor(copy: PhysicalDeviceUniformBufferStandardLayoutFeatures);
}

export class PhysicalDeviceShaderSubgroupExtendedTypesFeatures {
    static $gtype: GObject.GType<PhysicalDeviceShaderSubgroupExtendedTypesFeatures>;

    constructor(copy: PhysicalDeviceShaderSubgroupExtendedTypesFeatures);
}

export class PhysicalDeviceSeparateDepthStencilLayoutsFeatures {
    static $gtype: GObject.GType<PhysicalDeviceSeparateDepthStencilLayoutsFeatures>;

    constructor(copy: PhysicalDeviceSeparateDepthStencilLayoutsFeatures);
}

export class AttachmentReferenceStencilLayout {
    static $gtype: GObject.GType<AttachmentReferenceStencilLayout>;

    constructor(copy: AttachmentReferenceStencilLayout);
}

export class AttachmentDescriptionStencilLayout {
    static $gtype: GObject.GType<AttachmentDescriptionStencilLayout>;

    constructor(copy: AttachmentDescriptionStencilLayout);
}

export class PhysicalDeviceHostQueryResetFeatures {
    static $gtype: GObject.GType<PhysicalDeviceHostQueryResetFeatures>;

    constructor(copy: PhysicalDeviceHostQueryResetFeatures);
}

export class PhysicalDeviceTimelineSemaphoreFeatures {
    static $gtype: GObject.GType<PhysicalDeviceTimelineSemaphoreFeatures>;

    constructor(copy: PhysicalDeviceTimelineSemaphoreFeatures);
}

export class PhysicalDeviceTimelineSemaphoreProperties {
    static $gtype: GObject.GType<PhysicalDeviceTimelineSemaphoreProperties>;

    constructor(copy: PhysicalDeviceTimelineSemaphoreProperties);
}

export class SemaphoreTypeCreateInfo {
    static $gtype: GObject.GType<SemaphoreTypeCreateInfo>;

    constructor(copy: SemaphoreTypeCreateInfo);
}

export class TimelineSemaphoreSubmitInfo {
    static $gtype: GObject.GType<TimelineSemaphoreSubmitInfo>;

    constructor(copy: TimelineSemaphoreSubmitInfo);
}

export class SemaphoreWaitInfo {
    static $gtype: GObject.GType<SemaphoreWaitInfo>;

    constructor(copy: SemaphoreWaitInfo);
}

export class SemaphoreSignalInfo {
    static $gtype: GObject.GType<SemaphoreSignalInfo>;

    constructor(copy: SemaphoreSignalInfo);
}

export class PhysicalDeviceBufferDeviceAddressFeatures {
    static $gtype: GObject.GType<PhysicalDeviceBufferDeviceAddressFeatures>;

    constructor(copy: PhysicalDeviceBufferDeviceAddressFeatures);
}

export class BufferDeviceAddressInfo {
    static $gtype: GObject.GType<BufferDeviceAddressInfo>;

    constructor(copy: BufferDeviceAddressInfo);
}

export class BufferOpaqueCaptureAddressCreateInfo {
    static $gtype: GObject.GType<BufferOpaqueCaptureAddressCreateInfo>;

    constructor(copy: BufferOpaqueCaptureAddressCreateInfo);
}

export class MemoryOpaqueCaptureAddressAllocateInfo {
    static $gtype: GObject.GType<MemoryOpaqueCaptureAddressAllocateInfo>;

    constructor(copy: MemoryOpaqueCaptureAddressAllocateInfo);
}

export class DeviceMemoryOpaqueCaptureAddressInfo {
    static $gtype: GObject.GType<DeviceMemoryOpaqueCaptureAddressInfo>;

    constructor(copy: DeviceMemoryOpaqueCaptureAddressInfo);
}

export class Flags64 {
    static $gtype: GObject.GType<Flags64>;

    constructor(copy: Flags64);
}

export class PrivateDataSlot {
    static $gtype: GObject.GType<PrivateDataSlot>;

    constructor(copy: PrivateDataSlot);
}

export class PipelineCreationFeedbackFlagBits {
    static $gtype: GObject.GType<PipelineCreationFeedbackFlagBits>;

    constructor(copy: PipelineCreationFeedbackFlagBits);
}

export class PipelineCreationFeedbackFlags {
    static $gtype: GObject.GType<PipelineCreationFeedbackFlags>;

    constructor(copy: PipelineCreationFeedbackFlags);
}

export class ToolPurposeFlagBits {
    static $gtype: GObject.GType<ToolPurposeFlagBits>;

    constructor(copy: ToolPurposeFlagBits);
}

export class ToolPurposeFlags {
    static $gtype: GObject.GType<ToolPurposeFlags>;

    constructor(copy: ToolPurposeFlags);
}

export class PrivateDataSlotCreateFlags {
    static $gtype: GObject.GType<PrivateDataSlotCreateFlags>;

    constructor(copy: PrivateDataSlotCreateFlags);
}

export class PipelineStageFlags2 {
    static $gtype: GObject.GType<PipelineStageFlags2>;

    constructor(copy: PipelineStageFlags2);
}

export class PipelineStageFlagBits2 {
    static $gtype: GObject.GType<PipelineStageFlagBits2>;

    constructor(copy: PipelineStageFlagBits2);
}

export class AccessFlags2 {
    static $gtype: GObject.GType<AccessFlags2>;

    constructor(copy: AccessFlags2);
}

export class AccessFlagBits2 {
    static $gtype: GObject.GType<AccessFlagBits2>;

    constructor(copy: AccessFlagBits2);
}

export class SubmitFlagBits {
    static $gtype: GObject.GType<SubmitFlagBits>;

    constructor(copy: SubmitFlagBits);
}

export class SubmitFlags {
    static $gtype: GObject.GType<SubmitFlags>;

    constructor(copy: SubmitFlags);
}

export class RenderingFlagBits {
    static $gtype: GObject.GType<RenderingFlagBits>;

    constructor(copy: RenderingFlagBits);
}

export class RenderingFlags {
    static $gtype: GObject.GType<RenderingFlags>;

    constructor(copy: RenderingFlags);
}

export class FormatFeatureFlags2 {
    static $gtype: GObject.GType<FormatFeatureFlags2>;

    constructor(copy: FormatFeatureFlags2);
}

export class FormatFeatureFlagBits2 {
    static $gtype: GObject.GType<FormatFeatureFlagBits2>;

    constructor(copy: FormatFeatureFlagBits2);
}

export class PhysicalDeviceVulkan13Features {
    static $gtype: GObject.GType<PhysicalDeviceVulkan13Features>;

    constructor(copy: PhysicalDeviceVulkan13Features);
}

export class PhysicalDeviceVulkan13Properties {
    static $gtype: GObject.GType<PhysicalDeviceVulkan13Properties>;

    constructor(copy: PhysicalDeviceVulkan13Properties);
}

export class PipelineCreationFeedback {
    static $gtype: GObject.GType<PipelineCreationFeedback>;

    constructor(copy: PipelineCreationFeedback);
}

export class PipelineCreationFeedbackCreateInfo {
    static $gtype: GObject.GType<PipelineCreationFeedbackCreateInfo>;

    constructor(copy: PipelineCreationFeedbackCreateInfo);
}

export class PhysicalDeviceShaderTerminateInvocationFeatures {
    static $gtype: GObject.GType<PhysicalDeviceShaderTerminateInvocationFeatures>;

    constructor(copy: PhysicalDeviceShaderTerminateInvocationFeatures);
}

export class PhysicalDeviceToolProperties {
    static $gtype: GObject.GType<PhysicalDeviceToolProperties>;

    constructor(copy: PhysicalDeviceToolProperties);
}

export class PhysicalDeviceShaderDemoteToHelperInvocationFeatures {
    static $gtype: GObject.GType<PhysicalDeviceShaderDemoteToHelperInvocationFeatures>;

    constructor(copy: PhysicalDeviceShaderDemoteToHelperInvocationFeatures);
}

export class PhysicalDevicePrivateDataFeatures {
    static $gtype: GObject.GType<PhysicalDevicePrivateDataFeatures>;

    constructor(copy: PhysicalDevicePrivateDataFeatures);
}

export class DevicePrivateDataCreateInfo {
    static $gtype: GObject.GType<DevicePrivateDataCreateInfo>;

    constructor(copy: DevicePrivateDataCreateInfo);
}

export class PrivateDataSlotCreateInfo {
    static $gtype: GObject.GType<PrivateDataSlotCreateInfo>;

    constructor(copy: PrivateDataSlotCreateInfo);
}

export class PhysicalDevicePipelineCreationCacheControlFeatures {
    static $gtype: GObject.GType<PhysicalDevicePipelineCreationCacheControlFeatures>;

    constructor(copy: PhysicalDevicePipelineCreationCacheControlFeatures);
}

export class MemoryBarrier2 {
    static $gtype: GObject.GType<MemoryBarrier2>;

    constructor(copy: MemoryBarrier2);
}

export class BufferMemoryBarrier2 {
    static $gtype: GObject.GType<BufferMemoryBarrier2>;

    constructor(copy: BufferMemoryBarrier2);
}

export class ImageMemoryBarrier2 {
    static $gtype: GObject.GType<ImageMemoryBarrier2>;

    constructor(copy: ImageMemoryBarrier2);
}

export class DependencyInfo {
    static $gtype: GObject.GType<DependencyInfo>;

    constructor(copy: DependencyInfo);
}

export class SemaphoreSubmitInfo {
    static $gtype: GObject.GType<SemaphoreSubmitInfo>;

    constructor(copy: SemaphoreSubmitInfo);
}

export class CommandBufferSubmitInfo {
    static $gtype: GObject.GType<CommandBufferSubmitInfo>;

    constructor(copy: CommandBufferSubmitInfo);
}

export class SubmitInfo2 {
    static $gtype: GObject.GType<SubmitInfo2>;

    constructor(copy: SubmitInfo2);
}

export class PhysicalDeviceSynchronization2Features {
    static $gtype: GObject.GType<PhysicalDeviceSynchronization2Features>;

    constructor(copy: PhysicalDeviceSynchronization2Features);
}

export class PhysicalDeviceZeroInitializeWorkgroupMemoryFeatures {
    static $gtype: GObject.GType<PhysicalDeviceZeroInitializeWorkgroupMemoryFeatures>;

    constructor(copy: PhysicalDeviceZeroInitializeWorkgroupMemoryFeatures);
}

export class PhysicalDeviceImageRobustnessFeatures {
    static $gtype: GObject.GType<PhysicalDeviceImageRobustnessFeatures>;

    constructor(copy: PhysicalDeviceImageRobustnessFeatures);
}

export class BufferCopy2 {
    static $gtype: GObject.GType<BufferCopy2>;

    constructor(copy: BufferCopy2);
}

export class CopyBufferInfo2 {
    static $gtype: GObject.GType<CopyBufferInfo2>;

    constructor(copy: CopyBufferInfo2);
}

export class ImageCopy2 {
    static $gtype: GObject.GType<ImageCopy2>;

    constructor(copy: ImageCopy2);
}

export class CopyImageInfo2 {
    static $gtype: GObject.GType<CopyImageInfo2>;

    constructor(copy: CopyImageInfo2);
}

export class BufferImageCopy2 {
    static $gtype: GObject.GType<BufferImageCopy2>;

    constructor(copy: BufferImageCopy2);
}

export class CopyBufferToImageInfo2 {
    static $gtype: GObject.GType<CopyBufferToImageInfo2>;

    constructor(copy: CopyBufferToImageInfo2);
}

export class CopyImageToBufferInfo2 {
    static $gtype: GObject.GType<CopyImageToBufferInfo2>;

    constructor(copy: CopyImageToBufferInfo2);
}

export class ImageBlit2 {
    static $gtype: GObject.GType<ImageBlit2>;

    constructor(copy: ImageBlit2);
}

export class BlitImageInfo2 {
    static $gtype: GObject.GType<BlitImageInfo2>;

    constructor(copy: BlitImageInfo2);
}

export class ImageResolve2 {
    static $gtype: GObject.GType<ImageResolve2>;

    constructor(copy: ImageResolve2);
}

export class ResolveImageInfo2 {
    static $gtype: GObject.GType<ResolveImageInfo2>;

    constructor(copy: ResolveImageInfo2);
}

export class PhysicalDeviceSubgroupSizeControlFeatures {
    static $gtype: GObject.GType<PhysicalDeviceSubgroupSizeControlFeatures>;

    constructor(copy: PhysicalDeviceSubgroupSizeControlFeatures);
}

export class PhysicalDeviceSubgroupSizeControlProperties {
    static $gtype: GObject.GType<PhysicalDeviceSubgroupSizeControlProperties>;

    constructor(copy: PhysicalDeviceSubgroupSizeControlProperties);
}

export class PipelineShaderStageRequiredSubgroupSizeCreateInfo {
    static $gtype: GObject.GType<PipelineShaderStageRequiredSubgroupSizeCreateInfo>;

    constructor(copy: PipelineShaderStageRequiredSubgroupSizeCreateInfo);
}

export class PhysicalDeviceInlineUniformBlockFeatures {
    static $gtype: GObject.GType<PhysicalDeviceInlineUniformBlockFeatures>;

    constructor(copy: PhysicalDeviceInlineUniformBlockFeatures);
}

export class PhysicalDeviceInlineUniformBlockProperties {
    static $gtype: GObject.GType<PhysicalDeviceInlineUniformBlockProperties>;

    constructor(copy: PhysicalDeviceInlineUniformBlockProperties);
}

export class WriteDescriptorSetInlineUniformBlock {
    static $gtype: GObject.GType<WriteDescriptorSetInlineUniformBlock>;

    constructor(copy: WriteDescriptorSetInlineUniformBlock);
}

export class DescriptorPoolInlineUniformBlockCreateInfo {
    static $gtype: GObject.GType<DescriptorPoolInlineUniformBlockCreateInfo>;

    constructor(copy: DescriptorPoolInlineUniformBlockCreateInfo);
}

export class PhysicalDeviceTextureCompressionASTCHDRFeatures {
    static $gtype: GObject.GType<PhysicalDeviceTextureCompressionASTCHDRFeatures>;

    constructor(copy: PhysicalDeviceTextureCompressionASTCHDRFeatures);
}

export class RenderingAttachmentInfo {
    static $gtype: GObject.GType<RenderingAttachmentInfo>;

    constructor(copy: RenderingAttachmentInfo);
}

export class RenderingInfo {
    static $gtype: GObject.GType<RenderingInfo>;

    constructor(copy: RenderingInfo);
}

export class PipelineRenderingCreateInfo {
    static $gtype: GObject.GType<PipelineRenderingCreateInfo>;

    constructor(copy: PipelineRenderingCreateInfo);
}

export class PhysicalDeviceDynamicRenderingFeatures {
    static $gtype: GObject.GType<PhysicalDeviceDynamicRenderingFeatures>;

    constructor(copy: PhysicalDeviceDynamicRenderingFeatures);
}

export class CommandBufferInheritanceRenderingInfo {
    static $gtype: GObject.GType<CommandBufferInheritanceRenderingInfo>;

    constructor(copy: CommandBufferInheritanceRenderingInfo);
}

export class PhysicalDeviceShaderIntegerDotProductFeatures {
    static $gtype: GObject.GType<PhysicalDeviceShaderIntegerDotProductFeatures>;

    constructor(copy: PhysicalDeviceShaderIntegerDotProductFeatures);
}

export class PhysicalDeviceShaderIntegerDotProductProperties {
    static $gtype: GObject.GType<PhysicalDeviceShaderIntegerDotProductProperties>;

    constructor(copy: PhysicalDeviceShaderIntegerDotProductProperties);
}

export class PhysicalDeviceTexelBufferAlignmentProperties {
    static $gtype: GObject.GType<PhysicalDeviceTexelBufferAlignmentProperties>;

    constructor(copy: PhysicalDeviceTexelBufferAlignmentProperties);
}

export class FormatProperties3 {
    static $gtype: GObject.GType<FormatProperties3>;

    constructor(copy: FormatProperties3);
}

export class PhysicalDeviceMaintenance4Features {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance4Features>;

    constructor(copy: PhysicalDeviceMaintenance4Features);
}

export class PhysicalDeviceMaintenance4Properties {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance4Properties>;

    constructor(copy: PhysicalDeviceMaintenance4Properties);
}

export class DeviceBufferMemoryRequirements {
    static $gtype: GObject.GType<DeviceBufferMemoryRequirements>;

    constructor(copy: DeviceBufferMemoryRequirements);
}

export class DeviceImageMemoryRequirements {
    static $gtype: GObject.GType<DeviceImageMemoryRequirements>;

    constructor(copy: DeviceImageMemoryRequirements);
}

export class SurfaceKHR {
    static $gtype: GObject.GType<SurfaceKHR>;

    constructor(copy: SurfaceKHR);
}

export class PresentModeKHR {
    static $gtype: GObject.GType<PresentModeKHR>;

    constructor(copy: PresentModeKHR);
}

export class ColorSpaceKHR {
    static $gtype: GObject.GType<ColorSpaceKHR>;

    constructor(copy: ColorSpaceKHR);
}

export class SurfaceTransformFlagBitsKHR {
    static $gtype: GObject.GType<SurfaceTransformFlagBitsKHR>;

    constructor(copy: SurfaceTransformFlagBitsKHR);
}

export class CompositeAlphaFlagBitsKHR {
    static $gtype: GObject.GType<CompositeAlphaFlagBitsKHR>;

    constructor(copy: CompositeAlphaFlagBitsKHR);
}

export class CompositeAlphaFlagsKHR {
    static $gtype: GObject.GType<CompositeAlphaFlagsKHR>;

    constructor(copy: CompositeAlphaFlagsKHR);
}

export class SurfaceTransformFlagsKHR {
    static $gtype: GObject.GType<SurfaceTransformFlagsKHR>;

    constructor(copy: SurfaceTransformFlagsKHR);
}

export class SurfaceCapabilitiesKHR {
    static $gtype: GObject.GType<SurfaceCapabilitiesKHR>;

    constructor(copy: SurfaceCapabilitiesKHR);
}

export class SurfaceFormatKHR {
    static $gtype: GObject.GType<SurfaceFormatKHR>;

    constructor(copy: SurfaceFormatKHR);
}

export class SwapchainKHR {
    static $gtype: GObject.GType<SwapchainKHR>;

    constructor(copy: SwapchainKHR);
}

export class SwapchainCreateFlagBitsKHR {
    static $gtype: GObject.GType<SwapchainCreateFlagBitsKHR>;

    constructor(copy: SwapchainCreateFlagBitsKHR);
}

export class SwapchainCreateFlagsKHR {
    static $gtype: GObject.GType<SwapchainCreateFlagsKHR>;

    constructor(copy: SwapchainCreateFlagsKHR);
}

export class DeviceGroupPresentModeFlagBitsKHR {
    static $gtype: GObject.GType<DeviceGroupPresentModeFlagBitsKHR>;

    constructor(copy: DeviceGroupPresentModeFlagBitsKHR);
}

export class DeviceGroupPresentModeFlagsKHR {
    static $gtype: GObject.GType<DeviceGroupPresentModeFlagsKHR>;

    constructor(copy: DeviceGroupPresentModeFlagsKHR);
}

export class SwapchainCreateInfoKHR {
    static $gtype: GObject.GType<SwapchainCreateInfoKHR>;

    constructor(copy: SwapchainCreateInfoKHR);
}

export class PresentInfoKHR {
    static $gtype: GObject.GType<PresentInfoKHR>;

    constructor(copy: PresentInfoKHR);
}

export class ImageSwapchainCreateInfoKHR {
    static $gtype: GObject.GType<ImageSwapchainCreateInfoKHR>;

    constructor(copy: ImageSwapchainCreateInfoKHR);
}

export class BindImageMemorySwapchainInfoKHR {
    static $gtype: GObject.GType<BindImageMemorySwapchainInfoKHR>;

    constructor(copy: BindImageMemorySwapchainInfoKHR);
}

export class AcquireNextImageInfoKHR {
    static $gtype: GObject.GType<AcquireNextImageInfoKHR>;

    constructor(copy: AcquireNextImageInfoKHR);
}

export class DeviceGroupPresentCapabilitiesKHR {
    static $gtype: GObject.GType<DeviceGroupPresentCapabilitiesKHR>;

    constructor(copy: DeviceGroupPresentCapabilitiesKHR);
}

export class DeviceGroupPresentInfoKHR {
    static $gtype: GObject.GType<DeviceGroupPresentInfoKHR>;

    constructor(copy: DeviceGroupPresentInfoKHR);
}

export class DeviceGroupSwapchainCreateInfoKHR {
    static $gtype: GObject.GType<DeviceGroupSwapchainCreateInfoKHR>;

    constructor(copy: DeviceGroupSwapchainCreateInfoKHR);
}

export class DisplayKHR {
    static $gtype: GObject.GType<DisplayKHR>;

    constructor(copy: DisplayKHR);
}

export class DisplayModeKHR {
    static $gtype: GObject.GType<DisplayModeKHR>;

    constructor(copy: DisplayModeKHR);
}

export class DisplayModeCreateFlagsKHR {
    static $gtype: GObject.GType<DisplayModeCreateFlagsKHR>;

    constructor(copy: DisplayModeCreateFlagsKHR);
}

export class DisplayPlaneAlphaFlagBitsKHR {
    static $gtype: GObject.GType<DisplayPlaneAlphaFlagBitsKHR>;

    constructor(copy: DisplayPlaneAlphaFlagBitsKHR);
}

export class DisplayPlaneAlphaFlagsKHR {
    static $gtype: GObject.GType<DisplayPlaneAlphaFlagsKHR>;

    constructor(copy: DisplayPlaneAlphaFlagsKHR);
}

export class DisplaySurfaceCreateFlagsKHR {
    static $gtype: GObject.GType<DisplaySurfaceCreateFlagsKHR>;

    constructor(copy: DisplaySurfaceCreateFlagsKHR);
}

export class DisplayModeParametersKHR {
    static $gtype: GObject.GType<DisplayModeParametersKHR>;

    constructor(copy: DisplayModeParametersKHR);
}

export class DisplayModeCreateInfoKHR {
    static $gtype: GObject.GType<DisplayModeCreateInfoKHR>;

    constructor(copy: DisplayModeCreateInfoKHR);
}

export class DisplayModePropertiesKHR {
    static $gtype: GObject.GType<DisplayModePropertiesKHR>;

    constructor(copy: DisplayModePropertiesKHR);
}

export class DisplayPlaneCapabilitiesKHR {
    static $gtype: GObject.GType<DisplayPlaneCapabilitiesKHR>;

    constructor(copy: DisplayPlaneCapabilitiesKHR);
}

export class DisplayPlanePropertiesKHR {
    static $gtype: GObject.GType<DisplayPlanePropertiesKHR>;

    constructor(copy: DisplayPlanePropertiesKHR);
}

export class DisplayPropertiesKHR {
    static $gtype: GObject.GType<DisplayPropertiesKHR>;

    constructor(copy: DisplayPropertiesKHR);
}

export class DisplaySurfaceCreateInfoKHR {
    static $gtype: GObject.GType<DisplaySurfaceCreateInfoKHR>;

    constructor(copy: DisplaySurfaceCreateInfoKHR);
}

export class DisplayPresentInfoKHR {
    static $gtype: GObject.GType<DisplayPresentInfoKHR>;

    constructor(copy: DisplayPresentInfoKHR);
}

export class VideoSessionKHR {
    static $gtype: GObject.GType<VideoSessionKHR>;

    constructor(copy: VideoSessionKHR);
}

export class VideoSessionParametersKHR {
    static $gtype: GObject.GType<VideoSessionParametersKHR>;

    constructor(copy: VideoSessionParametersKHR);
}

export class QueryResultStatusKHR {
    static $gtype: GObject.GType<QueryResultStatusKHR>;

    constructor(copy: QueryResultStatusKHR);
}

export class VideoCodecOperationFlagBitsKHR {
    static $gtype: GObject.GType<VideoCodecOperationFlagBitsKHR>;

    constructor(copy: VideoCodecOperationFlagBitsKHR);
}

export class VideoCodecOperationFlagsKHR {
    static $gtype: GObject.GType<VideoCodecOperationFlagsKHR>;

    constructor(copy: VideoCodecOperationFlagsKHR);
}

export class VideoChromaSubsamplingFlagBitsKHR {
    static $gtype: GObject.GType<VideoChromaSubsamplingFlagBitsKHR>;

    constructor(copy: VideoChromaSubsamplingFlagBitsKHR);
}

export class VideoChromaSubsamplingFlagsKHR {
    static $gtype: GObject.GType<VideoChromaSubsamplingFlagsKHR>;

    constructor(copy: VideoChromaSubsamplingFlagsKHR);
}

export class VideoComponentBitDepthFlagBitsKHR {
    static $gtype: GObject.GType<VideoComponentBitDepthFlagBitsKHR>;

    constructor(copy: VideoComponentBitDepthFlagBitsKHR);
}

export class VideoComponentBitDepthFlagsKHR {
    static $gtype: GObject.GType<VideoComponentBitDepthFlagsKHR>;

    constructor(copy: VideoComponentBitDepthFlagsKHR);
}

export class VideoCapabilityFlagBitsKHR {
    static $gtype: GObject.GType<VideoCapabilityFlagBitsKHR>;

    constructor(copy: VideoCapabilityFlagBitsKHR);
}

export class VideoCapabilityFlagsKHR {
    static $gtype: GObject.GType<VideoCapabilityFlagsKHR>;

    constructor(copy: VideoCapabilityFlagsKHR);
}

export class VideoSessionCreateFlagBitsKHR {
    static $gtype: GObject.GType<VideoSessionCreateFlagBitsKHR>;

    constructor(copy: VideoSessionCreateFlagBitsKHR);
}

export class VideoSessionCreateFlagsKHR {
    static $gtype: GObject.GType<VideoSessionCreateFlagsKHR>;

    constructor(copy: VideoSessionCreateFlagsKHR);
}

export class VideoSessionParametersCreateFlagsKHR {
    static $gtype: GObject.GType<VideoSessionParametersCreateFlagsKHR>;

    constructor(copy: VideoSessionParametersCreateFlagsKHR);
}

export class VideoBeginCodingFlagsKHR {
    static $gtype: GObject.GType<VideoBeginCodingFlagsKHR>;

    constructor(copy: VideoBeginCodingFlagsKHR);
}

export class VideoEndCodingFlagsKHR {
    static $gtype: GObject.GType<VideoEndCodingFlagsKHR>;

    constructor(copy: VideoEndCodingFlagsKHR);
}

export class VideoCodingControlFlagBitsKHR {
    static $gtype: GObject.GType<VideoCodingControlFlagBitsKHR>;

    constructor(copy: VideoCodingControlFlagBitsKHR);
}

export class VideoCodingControlFlagsKHR {
    static $gtype: GObject.GType<VideoCodingControlFlagsKHR>;

    constructor(copy: VideoCodingControlFlagsKHR);
}

export class QueueFamilyQueryResultStatusPropertiesKHR {
    static $gtype: GObject.GType<QueueFamilyQueryResultStatusPropertiesKHR>;

    constructor(copy: QueueFamilyQueryResultStatusPropertiesKHR);
}

export class QueueFamilyVideoPropertiesKHR {
    static $gtype: GObject.GType<QueueFamilyVideoPropertiesKHR>;

    constructor(copy: QueueFamilyVideoPropertiesKHR);
}

export class VideoProfileInfoKHR {
    static $gtype: GObject.GType<VideoProfileInfoKHR>;

    constructor(copy: VideoProfileInfoKHR);
}

export class VideoProfileListInfoKHR {
    static $gtype: GObject.GType<VideoProfileListInfoKHR>;

    constructor(copy: VideoProfileListInfoKHR);
}

export class VideoCapabilitiesKHR {
    static $gtype: GObject.GType<VideoCapabilitiesKHR>;

    constructor(copy: VideoCapabilitiesKHR);
}

export class PhysicalDeviceVideoFormatInfoKHR {
    static $gtype: GObject.GType<PhysicalDeviceVideoFormatInfoKHR>;

    constructor(copy: PhysicalDeviceVideoFormatInfoKHR);
}

export class VideoFormatPropertiesKHR {
    static $gtype: GObject.GType<VideoFormatPropertiesKHR>;

    constructor(copy: VideoFormatPropertiesKHR);
}

export class VideoPictureResourceInfoKHR {
    static $gtype: GObject.GType<VideoPictureResourceInfoKHR>;

    constructor(copy: VideoPictureResourceInfoKHR);
}

export class VideoReferenceSlotInfoKHR {
    static $gtype: GObject.GType<VideoReferenceSlotInfoKHR>;

    constructor(copy: VideoReferenceSlotInfoKHR);
}

export class VideoSessionMemoryRequirementsKHR {
    static $gtype: GObject.GType<VideoSessionMemoryRequirementsKHR>;

    constructor(copy: VideoSessionMemoryRequirementsKHR);
}

export class BindVideoSessionMemoryInfoKHR {
    static $gtype: GObject.GType<BindVideoSessionMemoryInfoKHR>;

    constructor(copy: BindVideoSessionMemoryInfoKHR);
}

export class VideoSessionCreateInfoKHR {
    static $gtype: GObject.GType<VideoSessionCreateInfoKHR>;

    constructor(copy: VideoSessionCreateInfoKHR);
}

export class VideoSessionParametersCreateInfoKHR {
    static $gtype: GObject.GType<VideoSessionParametersCreateInfoKHR>;

    constructor(copy: VideoSessionParametersCreateInfoKHR);
}

export class VideoSessionParametersUpdateInfoKHR {
    static $gtype: GObject.GType<VideoSessionParametersUpdateInfoKHR>;

    constructor(copy: VideoSessionParametersUpdateInfoKHR);
}

export class VideoBeginCodingInfoKHR {
    static $gtype: GObject.GType<VideoBeginCodingInfoKHR>;

    constructor(copy: VideoBeginCodingInfoKHR);
}

export class VideoEndCodingInfoKHR {
    static $gtype: GObject.GType<VideoEndCodingInfoKHR>;

    constructor(copy: VideoEndCodingInfoKHR);
}

export class VideoCodingControlInfoKHR {
    static $gtype: GObject.GType<VideoCodingControlInfoKHR>;

    constructor(copy: VideoCodingControlInfoKHR);
}

export class VideoDecodeCapabilityFlagBitsKHR {
    static $gtype: GObject.GType<VideoDecodeCapabilityFlagBitsKHR>;

    constructor(copy: VideoDecodeCapabilityFlagBitsKHR);
}

export class VideoDecodeCapabilityFlagsKHR {
    static $gtype: GObject.GType<VideoDecodeCapabilityFlagsKHR>;

    constructor(copy: VideoDecodeCapabilityFlagsKHR);
}

export class VideoDecodeUsageFlagBitsKHR {
    static $gtype: GObject.GType<VideoDecodeUsageFlagBitsKHR>;

    constructor(copy: VideoDecodeUsageFlagBitsKHR);
}

export class VideoDecodeUsageFlagsKHR {
    static $gtype: GObject.GType<VideoDecodeUsageFlagsKHR>;

    constructor(copy: VideoDecodeUsageFlagsKHR);
}

export class VideoDecodeFlagsKHR {
    static $gtype: GObject.GType<VideoDecodeFlagsKHR>;

    constructor(copy: VideoDecodeFlagsKHR);
}

export class VideoDecodeCapabilitiesKHR {
    static $gtype: GObject.GType<VideoDecodeCapabilitiesKHR>;

    constructor(copy: VideoDecodeCapabilitiesKHR);
}

export class VideoDecodeUsageInfoKHR {
    static $gtype: GObject.GType<VideoDecodeUsageInfoKHR>;

    constructor(copy: VideoDecodeUsageInfoKHR);
}

export class VideoDecodeInfoKHR {
    static $gtype: GObject.GType<VideoDecodeInfoKHR>;

    constructor(copy: VideoDecodeInfoKHR);
}

export class VideoEncodeH264CapabilityFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH264CapabilityFlagBitsKHR>;

    constructor(copy: VideoEncodeH264CapabilityFlagBitsKHR);
}

export class VideoEncodeH264CapabilityFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH264CapabilityFlagsKHR>;

    constructor(copy: VideoEncodeH264CapabilityFlagsKHR);
}

export class VideoEncodeH264StdFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH264StdFlagBitsKHR>;

    constructor(copy: VideoEncodeH264StdFlagBitsKHR);
}

export class VideoEncodeH264StdFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH264StdFlagsKHR>;

    constructor(copy: VideoEncodeH264StdFlagsKHR);
}

export class VideoEncodeH264RateControlFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH264RateControlFlagBitsKHR>;

    constructor(copy: VideoEncodeH264RateControlFlagBitsKHR);
}

export class VideoEncodeH264RateControlFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH264RateControlFlagsKHR>;

    constructor(copy: VideoEncodeH264RateControlFlagsKHR);
}

export class VideoEncodeH264CapabilitiesKHR {
    static $gtype: GObject.GType<VideoEncodeH264CapabilitiesKHR>;

    constructor(copy: VideoEncodeH264CapabilitiesKHR);
}

export class VideoEncodeH264QpKHR {
    static $gtype: GObject.GType<VideoEncodeH264QpKHR>;

    constructor(copy: VideoEncodeH264QpKHR);
}

export class VideoEncodeH264QualityLevelPropertiesKHR {
    static $gtype: GObject.GType<VideoEncodeH264QualityLevelPropertiesKHR>;

    constructor(copy: VideoEncodeH264QualityLevelPropertiesKHR);
}

export class VideoEncodeH264SessionCreateInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264SessionCreateInfoKHR>;

    constructor(copy: VideoEncodeH264SessionCreateInfoKHR);
}

export class VideoEncodeH264SessionParametersAddInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264SessionParametersAddInfoKHR>;

    constructor(copy: VideoEncodeH264SessionParametersAddInfoKHR);
}

export class VideoEncodeH264SessionParametersCreateInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264SessionParametersCreateInfoKHR>;

    constructor(copy: VideoEncodeH264SessionParametersCreateInfoKHR);
}

export class VideoEncodeH264SessionParametersGetInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264SessionParametersGetInfoKHR>;

    constructor(copy: VideoEncodeH264SessionParametersGetInfoKHR);
}

export class VideoEncodeH264SessionParametersFeedbackInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264SessionParametersFeedbackInfoKHR>;

    constructor(copy: VideoEncodeH264SessionParametersFeedbackInfoKHR);
}

export class VideoEncodeH264NaluSliceInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264NaluSliceInfoKHR>;

    constructor(copy: VideoEncodeH264NaluSliceInfoKHR);
}

export class VideoEncodeH264PictureInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264PictureInfoKHR>;

    constructor(copy: VideoEncodeH264PictureInfoKHR);
}

export class VideoEncodeH264DpbSlotInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264DpbSlotInfoKHR>;

    constructor(copy: VideoEncodeH264DpbSlotInfoKHR);
}

export class VideoEncodeH264ProfileInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264ProfileInfoKHR>;

    constructor(copy: VideoEncodeH264ProfileInfoKHR);
}

export class VideoEncodeH264RateControlInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264RateControlInfoKHR>;

    constructor(copy: VideoEncodeH264RateControlInfoKHR);
}

export class VideoEncodeH264FrameSizeKHR {
    static $gtype: GObject.GType<VideoEncodeH264FrameSizeKHR>;

    constructor(copy: VideoEncodeH264FrameSizeKHR);
}

export class VideoEncodeH264RateControlLayerInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264RateControlLayerInfoKHR>;

    constructor(copy: VideoEncodeH264RateControlLayerInfoKHR);
}

export class VideoEncodeH264GopRemainingFrameInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH264GopRemainingFrameInfoKHR>;

    constructor(copy: VideoEncodeH264GopRemainingFrameInfoKHR);
}

export class VideoEncodeH265CapabilityFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH265CapabilityFlagBitsKHR>;

    constructor(copy: VideoEncodeH265CapabilityFlagBitsKHR);
}

export class VideoEncodeH265CapabilityFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH265CapabilityFlagsKHR>;

    constructor(copy: VideoEncodeH265CapabilityFlagsKHR);
}

export class VideoEncodeH265StdFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH265StdFlagBitsKHR>;

    constructor(copy: VideoEncodeH265StdFlagBitsKHR);
}

export class VideoEncodeH265StdFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH265StdFlagsKHR>;

    constructor(copy: VideoEncodeH265StdFlagsKHR);
}

export class VideoEncodeH265CtbSizeFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH265CtbSizeFlagBitsKHR>;

    constructor(copy: VideoEncodeH265CtbSizeFlagBitsKHR);
}

export class VideoEncodeH265CtbSizeFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH265CtbSizeFlagsKHR>;

    constructor(copy: VideoEncodeH265CtbSizeFlagsKHR);
}

export class VideoEncodeH265TransformBlockSizeFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH265TransformBlockSizeFlagBitsKHR>;

    constructor(copy: VideoEncodeH265TransformBlockSizeFlagBitsKHR);
}

export class VideoEncodeH265TransformBlockSizeFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH265TransformBlockSizeFlagsKHR>;

    constructor(copy: VideoEncodeH265TransformBlockSizeFlagsKHR);
}

export class VideoEncodeH265RateControlFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeH265RateControlFlagBitsKHR>;

    constructor(copy: VideoEncodeH265RateControlFlagBitsKHR);
}

export class VideoEncodeH265RateControlFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeH265RateControlFlagsKHR>;

    constructor(copy: VideoEncodeH265RateControlFlagsKHR);
}

export class VideoEncodeH265CapabilitiesKHR {
    static $gtype: GObject.GType<VideoEncodeH265CapabilitiesKHR>;

    constructor(copy: VideoEncodeH265CapabilitiesKHR);
}

export class VideoEncodeH265SessionCreateInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265SessionCreateInfoKHR>;

    constructor(copy: VideoEncodeH265SessionCreateInfoKHR);
}

export class VideoEncodeH265QpKHR {
    static $gtype: GObject.GType<VideoEncodeH265QpKHR>;

    constructor(copy: VideoEncodeH265QpKHR);
}

export class VideoEncodeH265QualityLevelPropertiesKHR {
    static $gtype: GObject.GType<VideoEncodeH265QualityLevelPropertiesKHR>;

    constructor(copy: VideoEncodeH265QualityLevelPropertiesKHR);
}

export class VideoEncodeH265SessionParametersAddInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265SessionParametersAddInfoKHR>;

    constructor(copy: VideoEncodeH265SessionParametersAddInfoKHR);
}

export class VideoEncodeH265SessionParametersCreateInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265SessionParametersCreateInfoKHR>;

    constructor(copy: VideoEncodeH265SessionParametersCreateInfoKHR);
}

export class VideoEncodeH265SessionParametersGetInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265SessionParametersGetInfoKHR>;

    constructor(copy: VideoEncodeH265SessionParametersGetInfoKHR);
}

export class VideoEncodeH265SessionParametersFeedbackInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265SessionParametersFeedbackInfoKHR>;

    constructor(copy: VideoEncodeH265SessionParametersFeedbackInfoKHR);
}

export class VideoEncodeH265NaluSliceSegmentInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265NaluSliceSegmentInfoKHR>;

    constructor(copy: VideoEncodeH265NaluSliceSegmentInfoKHR);
}

export class VideoEncodeH265PictureInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265PictureInfoKHR>;

    constructor(copy: VideoEncodeH265PictureInfoKHR);
}

export class VideoEncodeH265DpbSlotInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265DpbSlotInfoKHR>;

    constructor(copy: VideoEncodeH265DpbSlotInfoKHR);
}

export class VideoEncodeH265ProfileInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265ProfileInfoKHR>;

    constructor(copy: VideoEncodeH265ProfileInfoKHR);
}

export class VideoEncodeH265RateControlInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265RateControlInfoKHR>;

    constructor(copy: VideoEncodeH265RateControlInfoKHR);
}

export class VideoEncodeH265FrameSizeKHR {
    static $gtype: GObject.GType<VideoEncodeH265FrameSizeKHR>;

    constructor(copy: VideoEncodeH265FrameSizeKHR);
}

export class VideoEncodeH265RateControlLayerInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265RateControlLayerInfoKHR>;

    constructor(copy: VideoEncodeH265RateControlLayerInfoKHR);
}

export class VideoEncodeH265GopRemainingFrameInfoKHR {
    static $gtype: GObject.GType<VideoEncodeH265GopRemainingFrameInfoKHR>;

    constructor(copy: VideoEncodeH265GopRemainingFrameInfoKHR);
}

export class VideoDecodeH264PictureLayoutFlagBitsKHR {
    static $gtype: GObject.GType<VideoDecodeH264PictureLayoutFlagBitsKHR>;

    constructor(copy: VideoDecodeH264PictureLayoutFlagBitsKHR);
}

export class VideoDecodeH264PictureLayoutFlagsKHR {
    static $gtype: GObject.GType<VideoDecodeH264PictureLayoutFlagsKHR>;

    constructor(copy: VideoDecodeH264PictureLayoutFlagsKHR);
}

export class VideoDecodeH264ProfileInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH264ProfileInfoKHR>;

    constructor(copy: VideoDecodeH264ProfileInfoKHR);
}

export class VideoDecodeH264CapabilitiesKHR {
    static $gtype: GObject.GType<VideoDecodeH264CapabilitiesKHR>;

    constructor(copy: VideoDecodeH264CapabilitiesKHR);
}

export class VideoDecodeH264SessionParametersAddInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH264SessionParametersAddInfoKHR>;

    constructor(copy: VideoDecodeH264SessionParametersAddInfoKHR);
}

export class VideoDecodeH264SessionParametersCreateInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH264SessionParametersCreateInfoKHR>;

    constructor(copy: VideoDecodeH264SessionParametersCreateInfoKHR);
}

export class VideoDecodeH264PictureInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH264PictureInfoKHR>;

    constructor(copy: VideoDecodeH264PictureInfoKHR);
}

export class VideoDecodeH264DpbSlotInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH264DpbSlotInfoKHR>;

    constructor(copy: VideoDecodeH264DpbSlotInfoKHR);
}

export class RenderingFlagsKHR {
    static $gtype: GObject.GType<RenderingFlagsKHR>;

    constructor(copy: RenderingFlagsKHR);
}

export class RenderingFlagBitsKHR {
    static $gtype: GObject.GType<RenderingFlagBitsKHR>;

    constructor(copy: RenderingFlagBitsKHR);
}

export class RenderingInfoKHR {
    static $gtype: GObject.GType<RenderingInfoKHR>;

    constructor(copy: RenderingInfoKHR);
}

export class RenderingAttachmentInfoKHR {
    static $gtype: GObject.GType<RenderingAttachmentInfoKHR>;

    constructor(copy: RenderingAttachmentInfoKHR);
}

export class PipelineRenderingCreateInfoKHR {
    static $gtype: GObject.GType<PipelineRenderingCreateInfoKHR>;

    constructor(copy: PipelineRenderingCreateInfoKHR);
}

export class PhysicalDeviceDynamicRenderingFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceDynamicRenderingFeaturesKHR>;

    constructor(copy: PhysicalDeviceDynamicRenderingFeaturesKHR);
}

export class CommandBufferInheritanceRenderingInfoKHR {
    static $gtype: GObject.GType<CommandBufferInheritanceRenderingInfoKHR>;

    constructor(copy: CommandBufferInheritanceRenderingInfoKHR);
}

export class RenderingFragmentShadingRateAttachmentInfoKHR {
    static $gtype: GObject.GType<RenderingFragmentShadingRateAttachmentInfoKHR>;

    constructor(copy: RenderingFragmentShadingRateAttachmentInfoKHR);
}

export class RenderingFragmentDensityMapAttachmentInfoEXT {
    static $gtype: GObject.GType<RenderingFragmentDensityMapAttachmentInfoEXT>;

    constructor(copy: RenderingFragmentDensityMapAttachmentInfoEXT);
}

export class AttachmentSampleCountInfoAMD {
    static $gtype: GObject.GType<AttachmentSampleCountInfoAMD>;

    constructor(copy: AttachmentSampleCountInfoAMD);
}

export class AttachmentSampleCountInfoNV {
    static $gtype: GObject.GType<AttachmentSampleCountInfoNV>;

    constructor(copy: AttachmentSampleCountInfoNV);
}

export class MultiviewPerViewAttributesInfoNVX {
    static $gtype: GObject.GType<MultiviewPerViewAttributesInfoNVX>;

    constructor(copy: MultiviewPerViewAttributesInfoNVX);
}

export class RenderPassMultiviewCreateInfoKHR {
    static $gtype: GObject.GType<RenderPassMultiviewCreateInfoKHR>;

    constructor(copy: RenderPassMultiviewCreateInfoKHR);
}

export class PhysicalDeviceMultiviewFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewFeaturesKHR>;

    constructor(copy: PhysicalDeviceMultiviewFeaturesKHR);
}

export class PhysicalDeviceMultiviewPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewPropertiesKHR>;

    constructor(copy: PhysicalDeviceMultiviewPropertiesKHR);
}

export class PhysicalDeviceFeatures2KHR {
    static $gtype: GObject.GType<PhysicalDeviceFeatures2KHR>;

    constructor(copy: PhysicalDeviceFeatures2KHR);
}

export class PhysicalDeviceProperties2KHR {
    static $gtype: GObject.GType<PhysicalDeviceProperties2KHR>;

    constructor(copy: PhysicalDeviceProperties2KHR);
}

export class FormatProperties2KHR {
    static $gtype: GObject.GType<FormatProperties2KHR>;

    constructor(copy: FormatProperties2KHR);
}

export class ImageFormatProperties2KHR {
    static $gtype: GObject.GType<ImageFormatProperties2KHR>;

    constructor(copy: ImageFormatProperties2KHR);
}

export class PhysicalDeviceImageFormatInfo2KHR {
    static $gtype: GObject.GType<PhysicalDeviceImageFormatInfo2KHR>;

    constructor(copy: PhysicalDeviceImageFormatInfo2KHR);
}

export class QueueFamilyProperties2KHR {
    static $gtype: GObject.GType<QueueFamilyProperties2KHR>;

    constructor(copy: QueueFamilyProperties2KHR);
}

export class PhysicalDeviceMemoryProperties2KHR {
    static $gtype: GObject.GType<PhysicalDeviceMemoryProperties2KHR>;

    constructor(copy: PhysicalDeviceMemoryProperties2KHR);
}

export class SparseImageFormatProperties2KHR {
    static $gtype: GObject.GType<SparseImageFormatProperties2KHR>;

    constructor(copy: SparseImageFormatProperties2KHR);
}

export class PhysicalDeviceSparseImageFormatInfo2KHR {
    static $gtype: GObject.GType<PhysicalDeviceSparseImageFormatInfo2KHR>;

    constructor(copy: PhysicalDeviceSparseImageFormatInfo2KHR);
}

export class PeerMemoryFeatureFlagsKHR {
    static $gtype: GObject.GType<PeerMemoryFeatureFlagsKHR>;

    constructor(copy: PeerMemoryFeatureFlagsKHR);
}

export class PeerMemoryFeatureFlagBitsKHR {
    static $gtype: GObject.GType<PeerMemoryFeatureFlagBitsKHR>;

    constructor(copy: PeerMemoryFeatureFlagBitsKHR);
}

export class MemoryAllocateFlagsKHR {
    static $gtype: GObject.GType<MemoryAllocateFlagsKHR>;

    constructor(copy: MemoryAllocateFlagsKHR);
}

export class MemoryAllocateFlagBitsKHR {
    static $gtype: GObject.GType<MemoryAllocateFlagBitsKHR>;

    constructor(copy: MemoryAllocateFlagBitsKHR);
}

export class MemoryAllocateFlagsInfoKHR {
    static $gtype: GObject.GType<MemoryAllocateFlagsInfoKHR>;

    constructor(copy: MemoryAllocateFlagsInfoKHR);
}

export class DeviceGroupRenderPassBeginInfoKHR {
    static $gtype: GObject.GType<DeviceGroupRenderPassBeginInfoKHR>;

    constructor(copy: DeviceGroupRenderPassBeginInfoKHR);
}

export class DeviceGroupCommandBufferBeginInfoKHR {
    static $gtype: GObject.GType<DeviceGroupCommandBufferBeginInfoKHR>;

    constructor(copy: DeviceGroupCommandBufferBeginInfoKHR);
}

export class DeviceGroupSubmitInfoKHR {
    static $gtype: GObject.GType<DeviceGroupSubmitInfoKHR>;

    constructor(copy: DeviceGroupSubmitInfoKHR);
}

export class DeviceGroupBindSparseInfoKHR {
    static $gtype: GObject.GType<DeviceGroupBindSparseInfoKHR>;

    constructor(copy: DeviceGroupBindSparseInfoKHR);
}

export class BindBufferMemoryDeviceGroupInfoKHR {
    static $gtype: GObject.GType<BindBufferMemoryDeviceGroupInfoKHR>;

    constructor(copy: BindBufferMemoryDeviceGroupInfoKHR);
}

export class BindImageMemoryDeviceGroupInfoKHR {
    static $gtype: GObject.GType<BindImageMemoryDeviceGroupInfoKHR>;

    constructor(copy: BindImageMemoryDeviceGroupInfoKHR);
}

export class CommandPoolTrimFlagsKHR {
    static $gtype: GObject.GType<CommandPoolTrimFlagsKHR>;

    constructor(copy: CommandPoolTrimFlagsKHR);
}

export class PhysicalDeviceGroupPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceGroupPropertiesKHR>;

    constructor(copy: PhysicalDeviceGroupPropertiesKHR);
}

export class DeviceGroupDeviceCreateInfoKHR {
    static $gtype: GObject.GType<DeviceGroupDeviceCreateInfoKHR>;

    constructor(copy: DeviceGroupDeviceCreateInfoKHR);
}

export class ExternalMemoryHandleTypeFlagsKHR {
    static $gtype: GObject.GType<ExternalMemoryHandleTypeFlagsKHR>;

    constructor(copy: ExternalMemoryHandleTypeFlagsKHR);
}

export class ExternalMemoryHandleTypeFlagBitsKHR {
    static $gtype: GObject.GType<ExternalMemoryHandleTypeFlagBitsKHR>;

    constructor(copy: ExternalMemoryHandleTypeFlagBitsKHR);
}

export class ExternalMemoryFeatureFlagsKHR {
    static $gtype: GObject.GType<ExternalMemoryFeatureFlagsKHR>;

    constructor(copy: ExternalMemoryFeatureFlagsKHR);
}

export class ExternalMemoryFeatureFlagBitsKHR {
    static $gtype: GObject.GType<ExternalMemoryFeatureFlagBitsKHR>;

    constructor(copy: ExternalMemoryFeatureFlagBitsKHR);
}

export class ExternalMemoryPropertiesKHR {
    static $gtype: GObject.GType<ExternalMemoryPropertiesKHR>;

    constructor(copy: ExternalMemoryPropertiesKHR);
}

export class PhysicalDeviceExternalImageFormatInfoKHR {
    static $gtype: GObject.GType<PhysicalDeviceExternalImageFormatInfoKHR>;

    constructor(copy: PhysicalDeviceExternalImageFormatInfoKHR);
}

export class ExternalImageFormatPropertiesKHR {
    static $gtype: GObject.GType<ExternalImageFormatPropertiesKHR>;

    constructor(copy: ExternalImageFormatPropertiesKHR);
}

export class PhysicalDeviceExternalBufferInfoKHR {
    static $gtype: GObject.GType<PhysicalDeviceExternalBufferInfoKHR>;

    constructor(copy: PhysicalDeviceExternalBufferInfoKHR);
}

export class ExternalBufferPropertiesKHR {
    static $gtype: GObject.GType<ExternalBufferPropertiesKHR>;

    constructor(copy: ExternalBufferPropertiesKHR);
}

export class PhysicalDeviceIDPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceIDPropertiesKHR>;

    constructor(copy: PhysicalDeviceIDPropertiesKHR);
}

export class ExternalMemoryImageCreateInfoKHR {
    static $gtype: GObject.GType<ExternalMemoryImageCreateInfoKHR>;

    constructor(copy: ExternalMemoryImageCreateInfoKHR);
}

export class ExternalMemoryBufferCreateInfoKHR {
    static $gtype: GObject.GType<ExternalMemoryBufferCreateInfoKHR>;

    constructor(copy: ExternalMemoryBufferCreateInfoKHR);
}

export class ExportMemoryAllocateInfoKHR {
    static $gtype: GObject.GType<ExportMemoryAllocateInfoKHR>;

    constructor(copy: ExportMemoryAllocateInfoKHR);
}

export class ImportMemoryFdInfoKHR {
    static $gtype: GObject.GType<ImportMemoryFdInfoKHR>;

    constructor(copy: ImportMemoryFdInfoKHR);
}

export class MemoryFdPropertiesKHR {
    static $gtype: GObject.GType<MemoryFdPropertiesKHR>;

    constructor(copy: MemoryFdPropertiesKHR);
}

export class MemoryGetFdInfoKHR {
    static $gtype: GObject.GType<MemoryGetFdInfoKHR>;

    constructor(copy: MemoryGetFdInfoKHR);
}

export class ExternalSemaphoreHandleTypeFlagsKHR {
    static $gtype: GObject.GType<ExternalSemaphoreHandleTypeFlagsKHR>;

    constructor(copy: ExternalSemaphoreHandleTypeFlagsKHR);
}

export class ExternalSemaphoreHandleTypeFlagBitsKHR {
    static $gtype: GObject.GType<ExternalSemaphoreHandleTypeFlagBitsKHR>;

    constructor(copy: ExternalSemaphoreHandleTypeFlagBitsKHR);
}

export class ExternalSemaphoreFeatureFlagsKHR {
    static $gtype: GObject.GType<ExternalSemaphoreFeatureFlagsKHR>;

    constructor(copy: ExternalSemaphoreFeatureFlagsKHR);
}

export class ExternalSemaphoreFeatureFlagBitsKHR {
    static $gtype: GObject.GType<ExternalSemaphoreFeatureFlagBitsKHR>;

    constructor(copy: ExternalSemaphoreFeatureFlagBitsKHR);
}

export class PhysicalDeviceExternalSemaphoreInfoKHR {
    static $gtype: GObject.GType<PhysicalDeviceExternalSemaphoreInfoKHR>;

    constructor(copy: PhysicalDeviceExternalSemaphoreInfoKHR);
}

export class ExternalSemaphorePropertiesKHR {
    static $gtype: GObject.GType<ExternalSemaphorePropertiesKHR>;

    constructor(copy: ExternalSemaphorePropertiesKHR);
}

export class SemaphoreImportFlagsKHR {
    static $gtype: GObject.GType<SemaphoreImportFlagsKHR>;

    constructor(copy: SemaphoreImportFlagsKHR);
}

export class SemaphoreImportFlagBitsKHR {
    static $gtype: GObject.GType<SemaphoreImportFlagBitsKHR>;

    constructor(copy: SemaphoreImportFlagBitsKHR);
}

export class ExportSemaphoreCreateInfoKHR {
    static $gtype: GObject.GType<ExportSemaphoreCreateInfoKHR>;

    constructor(copy: ExportSemaphoreCreateInfoKHR);
}

export class ImportSemaphoreFdInfoKHR {
    static $gtype: GObject.GType<ImportSemaphoreFdInfoKHR>;

    constructor(copy: ImportSemaphoreFdInfoKHR);
}

export class SemaphoreGetFdInfoKHR {
    static $gtype: GObject.GType<SemaphoreGetFdInfoKHR>;

    constructor(copy: SemaphoreGetFdInfoKHR);
}

export class PhysicalDevicePushDescriptorPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDevicePushDescriptorPropertiesKHR>;

    constructor(copy: PhysicalDevicePushDescriptorPropertiesKHR);
}

export class PhysicalDeviceShaderFloat16Int8FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderFloat16Int8FeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderFloat16Int8FeaturesKHR);
}

export class PhysicalDeviceFloat16Int8FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceFloat16Int8FeaturesKHR>;

    constructor(copy: PhysicalDeviceFloat16Int8FeaturesKHR);
}

export class PhysicalDevice16BitStorageFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDevice16BitStorageFeaturesKHR>;

    constructor(copy: PhysicalDevice16BitStorageFeaturesKHR);
}

export class RectLayerKHR {
    static $gtype: GObject.GType<RectLayerKHR>;

    constructor(copy: RectLayerKHR);
}

export class PresentRegionKHR {
    static $gtype: GObject.GType<PresentRegionKHR>;

    constructor(copy: PresentRegionKHR);
}

export class PresentRegionsKHR {
    static $gtype: GObject.GType<PresentRegionsKHR>;

    constructor(copy: PresentRegionsKHR);
}

export class DescriptorUpdateTemplateKHR {
    static $gtype: GObject.GType<DescriptorUpdateTemplateKHR>;

    constructor(copy: DescriptorUpdateTemplateKHR);
}

export class DescriptorUpdateTemplateTypeKHR {
    static $gtype: GObject.GType<DescriptorUpdateTemplateTypeKHR>;

    constructor(copy: DescriptorUpdateTemplateTypeKHR);
}

export class DescriptorUpdateTemplateCreateFlagsKHR {
    static $gtype: GObject.GType<DescriptorUpdateTemplateCreateFlagsKHR>;

    constructor(copy: DescriptorUpdateTemplateCreateFlagsKHR);
}

export class DescriptorUpdateTemplateEntryKHR {
    static $gtype: GObject.GType<DescriptorUpdateTemplateEntryKHR>;

    constructor(copy: DescriptorUpdateTemplateEntryKHR);
}

export class DescriptorUpdateTemplateCreateInfoKHR {
    static $gtype: GObject.GType<DescriptorUpdateTemplateCreateInfoKHR>;

    constructor(copy: DescriptorUpdateTemplateCreateInfoKHR);
}

export class PhysicalDeviceImagelessFramebufferFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceImagelessFramebufferFeaturesKHR>;

    constructor(copy: PhysicalDeviceImagelessFramebufferFeaturesKHR);
}

export class FramebufferAttachmentsCreateInfoKHR {
    static $gtype: GObject.GType<FramebufferAttachmentsCreateInfoKHR>;

    constructor(copy: FramebufferAttachmentsCreateInfoKHR);
}

export class FramebufferAttachmentImageInfoKHR {
    static $gtype: GObject.GType<FramebufferAttachmentImageInfoKHR>;

    constructor(copy: FramebufferAttachmentImageInfoKHR);
}

export class RenderPassAttachmentBeginInfoKHR {
    static $gtype: GObject.GType<RenderPassAttachmentBeginInfoKHR>;

    constructor(copy: RenderPassAttachmentBeginInfoKHR);
}

export class RenderPassCreateInfo2KHR {
    static $gtype: GObject.GType<RenderPassCreateInfo2KHR>;

    constructor(copy: RenderPassCreateInfo2KHR);
}

export class AttachmentDescription2KHR {
    static $gtype: GObject.GType<AttachmentDescription2KHR>;

    constructor(copy: AttachmentDescription2KHR);
}

export class AttachmentReference2KHR {
    static $gtype: GObject.GType<AttachmentReference2KHR>;

    constructor(copy: AttachmentReference2KHR);
}

export class SubpassDescription2KHR {
    static $gtype: GObject.GType<SubpassDescription2KHR>;

    constructor(copy: SubpassDescription2KHR);
}

export class SubpassDependency2KHR {
    static $gtype: GObject.GType<SubpassDependency2KHR>;

    constructor(copy: SubpassDependency2KHR);
}

export class SubpassBeginInfoKHR {
    static $gtype: GObject.GType<SubpassBeginInfoKHR>;

    constructor(copy: SubpassBeginInfoKHR);
}

export class SubpassEndInfoKHR {
    static $gtype: GObject.GType<SubpassEndInfoKHR>;

    constructor(copy: SubpassEndInfoKHR);
}

export class SharedPresentSurfaceCapabilitiesKHR {
    static $gtype: GObject.GType<SharedPresentSurfaceCapabilitiesKHR>;

    constructor(copy: SharedPresentSurfaceCapabilitiesKHR);
}

export class ExternalFenceHandleTypeFlagsKHR {
    static $gtype: GObject.GType<ExternalFenceHandleTypeFlagsKHR>;

    constructor(copy: ExternalFenceHandleTypeFlagsKHR);
}

export class ExternalFenceHandleTypeFlagBitsKHR {
    static $gtype: GObject.GType<ExternalFenceHandleTypeFlagBitsKHR>;

    constructor(copy: ExternalFenceHandleTypeFlagBitsKHR);
}

export class ExternalFenceFeatureFlagsKHR {
    static $gtype: GObject.GType<ExternalFenceFeatureFlagsKHR>;

    constructor(copy: ExternalFenceFeatureFlagsKHR);
}

export class ExternalFenceFeatureFlagBitsKHR {
    static $gtype: GObject.GType<ExternalFenceFeatureFlagBitsKHR>;

    constructor(copy: ExternalFenceFeatureFlagBitsKHR);
}

export class PhysicalDeviceExternalFenceInfoKHR {
    static $gtype: GObject.GType<PhysicalDeviceExternalFenceInfoKHR>;

    constructor(copy: PhysicalDeviceExternalFenceInfoKHR);
}

export class ExternalFencePropertiesKHR {
    static $gtype: GObject.GType<ExternalFencePropertiesKHR>;

    constructor(copy: ExternalFencePropertiesKHR);
}

export class FenceImportFlagsKHR {
    static $gtype: GObject.GType<FenceImportFlagsKHR>;

    constructor(copy: FenceImportFlagsKHR);
}

export class FenceImportFlagBitsKHR {
    static $gtype: GObject.GType<FenceImportFlagBitsKHR>;

    constructor(copy: FenceImportFlagBitsKHR);
}

export class ExportFenceCreateInfoKHR {
    static $gtype: GObject.GType<ExportFenceCreateInfoKHR>;

    constructor(copy: ExportFenceCreateInfoKHR);
}

export class ImportFenceFdInfoKHR {
    static $gtype: GObject.GType<ImportFenceFdInfoKHR>;

    constructor(copy: ImportFenceFdInfoKHR);
}

export class FenceGetFdInfoKHR {
    static $gtype: GObject.GType<FenceGetFdInfoKHR>;

    constructor(copy: FenceGetFdInfoKHR);
}

export class PerformanceCounterUnitKHR {
    static $gtype: GObject.GType<PerformanceCounterUnitKHR>;

    constructor(copy: PerformanceCounterUnitKHR);
}

export class PerformanceCounterScopeKHR {
    static $gtype: GObject.GType<PerformanceCounterScopeKHR>;

    constructor(copy: PerformanceCounterScopeKHR);
}

export class PerformanceCounterStorageKHR {
    static $gtype: GObject.GType<PerformanceCounterStorageKHR>;

    constructor(copy: PerformanceCounterStorageKHR);
}

export class PerformanceCounterDescriptionFlagBitsKHR {
    static $gtype: GObject.GType<PerformanceCounterDescriptionFlagBitsKHR>;

    constructor(copy: PerformanceCounterDescriptionFlagBitsKHR);
}

export class PerformanceCounterDescriptionFlagsKHR {
    static $gtype: GObject.GType<PerformanceCounterDescriptionFlagsKHR>;

    constructor(copy: PerformanceCounterDescriptionFlagsKHR);
}

export class AcquireProfilingLockFlagBitsKHR {
    static $gtype: GObject.GType<AcquireProfilingLockFlagBitsKHR>;

    constructor(copy: AcquireProfilingLockFlagBitsKHR);
}

export class AcquireProfilingLockFlagsKHR {
    static $gtype: GObject.GType<AcquireProfilingLockFlagsKHR>;

    constructor(copy: AcquireProfilingLockFlagsKHR);
}

export class PhysicalDevicePerformanceQueryFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDevicePerformanceQueryFeaturesKHR>;

    constructor(copy: PhysicalDevicePerformanceQueryFeaturesKHR);
}

export class PhysicalDevicePerformanceQueryPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDevicePerformanceQueryPropertiesKHR>;

    constructor(copy: PhysicalDevicePerformanceQueryPropertiesKHR);
}

export class PerformanceCounterKHR {
    static $gtype: GObject.GType<PerformanceCounterKHR>;

    constructor(copy: PerformanceCounterKHR);
}

export class PerformanceCounterDescriptionKHR {
    static $gtype: GObject.GType<PerformanceCounterDescriptionKHR>;

    constructor(copy: PerformanceCounterDescriptionKHR);
}

export class QueryPoolPerformanceCreateInfoKHR {
    static $gtype: GObject.GType<QueryPoolPerformanceCreateInfoKHR>;

    constructor(copy: QueryPoolPerformanceCreateInfoKHR);
}

export class PerformanceCounterResultKHR {
    static $gtype: GObject.GType<PerformanceCounterResultKHR>;

    constructor(copy: PerformanceCounterResultKHR);
}

export class AcquireProfilingLockInfoKHR {
    static $gtype: GObject.GType<AcquireProfilingLockInfoKHR>;

    constructor(copy: AcquireProfilingLockInfoKHR);
}

export class PerformanceQuerySubmitInfoKHR {
    static $gtype: GObject.GType<PerformanceQuerySubmitInfoKHR>;

    constructor(copy: PerformanceQuerySubmitInfoKHR);
}

export class PointClippingBehaviorKHR {
    static $gtype: GObject.GType<PointClippingBehaviorKHR>;

    constructor(copy: PointClippingBehaviorKHR);
}

export class TessellationDomainOriginKHR {
    static $gtype: GObject.GType<TessellationDomainOriginKHR>;

    constructor(copy: TessellationDomainOriginKHR);
}

export class PhysicalDevicePointClippingPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDevicePointClippingPropertiesKHR>;

    constructor(copy: PhysicalDevicePointClippingPropertiesKHR);
}

export class RenderPassInputAttachmentAspectCreateInfoKHR {
    static $gtype: GObject.GType<RenderPassInputAttachmentAspectCreateInfoKHR>;

    constructor(copy: RenderPassInputAttachmentAspectCreateInfoKHR);
}

export class InputAttachmentAspectReferenceKHR {
    static $gtype: GObject.GType<InputAttachmentAspectReferenceKHR>;

    constructor(copy: InputAttachmentAspectReferenceKHR);
}

export class ImageViewUsageCreateInfoKHR {
    static $gtype: GObject.GType<ImageViewUsageCreateInfoKHR>;

    constructor(copy: ImageViewUsageCreateInfoKHR);
}

export class PipelineTessellationDomainOriginStateCreateInfoKHR {
    static $gtype: GObject.GType<PipelineTessellationDomainOriginStateCreateInfoKHR>;

    constructor(copy: PipelineTessellationDomainOriginStateCreateInfoKHR);
}

export class PhysicalDeviceSurfaceInfo2KHR {
    static $gtype: GObject.GType<PhysicalDeviceSurfaceInfo2KHR>;

    constructor(copy: PhysicalDeviceSurfaceInfo2KHR);
}

export class SurfaceCapabilities2KHR {
    static $gtype: GObject.GType<SurfaceCapabilities2KHR>;

    constructor(copy: SurfaceCapabilities2KHR);
}

export class SurfaceFormat2KHR {
    static $gtype: GObject.GType<SurfaceFormat2KHR>;

    constructor(copy: SurfaceFormat2KHR);
}

export class PhysicalDeviceVariablePointerFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceVariablePointerFeaturesKHR>;

    constructor(copy: PhysicalDeviceVariablePointerFeaturesKHR);
}

export class PhysicalDeviceVariablePointersFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceVariablePointersFeaturesKHR>;

    constructor(copy: PhysicalDeviceVariablePointersFeaturesKHR);
}

export class DisplayProperties2KHR {
    static $gtype: GObject.GType<DisplayProperties2KHR>;

    constructor(copy: DisplayProperties2KHR);
}

export class DisplayPlaneProperties2KHR {
    static $gtype: GObject.GType<DisplayPlaneProperties2KHR>;

    constructor(copy: DisplayPlaneProperties2KHR);
}

export class DisplayModeProperties2KHR {
    static $gtype: GObject.GType<DisplayModeProperties2KHR>;

    constructor(copy: DisplayModeProperties2KHR);
}

export class DisplayPlaneInfo2KHR {
    static $gtype: GObject.GType<DisplayPlaneInfo2KHR>;

    constructor(copy: DisplayPlaneInfo2KHR);
}

export class DisplayPlaneCapabilities2KHR {
    static $gtype: GObject.GType<DisplayPlaneCapabilities2KHR>;

    constructor(copy: DisplayPlaneCapabilities2KHR);
}

export class MemoryDedicatedRequirementsKHR {
    static $gtype: GObject.GType<MemoryDedicatedRequirementsKHR>;

    constructor(copy: MemoryDedicatedRequirementsKHR);
}

export class MemoryDedicatedAllocateInfoKHR {
    static $gtype: GObject.GType<MemoryDedicatedAllocateInfoKHR>;

    constructor(copy: MemoryDedicatedAllocateInfoKHR);
}

export class BufferMemoryRequirementsInfo2KHR {
    static $gtype: GObject.GType<BufferMemoryRequirementsInfo2KHR>;

    constructor(copy: BufferMemoryRequirementsInfo2KHR);
}

export class ImageMemoryRequirementsInfo2KHR {
    static $gtype: GObject.GType<ImageMemoryRequirementsInfo2KHR>;

    constructor(copy: ImageMemoryRequirementsInfo2KHR);
}

export class ImageSparseMemoryRequirementsInfo2KHR {
    static $gtype: GObject.GType<ImageSparseMemoryRequirementsInfo2KHR>;

    constructor(copy: ImageSparseMemoryRequirementsInfo2KHR);
}

export class MemoryRequirements2KHR {
    static $gtype: GObject.GType<MemoryRequirements2KHR>;

    constructor(copy: MemoryRequirements2KHR);
}

export class SparseImageMemoryRequirements2KHR {
    static $gtype: GObject.GType<SparseImageMemoryRequirements2KHR>;

    constructor(copy: SparseImageMemoryRequirements2KHR);
}

export class ImageFormatListCreateInfoKHR {
    static $gtype: GObject.GType<ImageFormatListCreateInfoKHR>;

    constructor(copy: ImageFormatListCreateInfoKHR);
}

export class SamplerYcbcrConversionKHR {
    static $gtype: GObject.GType<SamplerYcbcrConversionKHR>;

    constructor(copy: SamplerYcbcrConversionKHR);
}

export class SamplerYcbcrModelConversionKHR {
    static $gtype: GObject.GType<SamplerYcbcrModelConversionKHR>;

    constructor(copy: SamplerYcbcrModelConversionKHR);
}

export class SamplerYcbcrRangeKHR {
    static $gtype: GObject.GType<SamplerYcbcrRangeKHR>;

    constructor(copy: SamplerYcbcrRangeKHR);
}

export class ChromaLocationKHR {
    static $gtype: GObject.GType<ChromaLocationKHR>;

    constructor(copy: ChromaLocationKHR);
}

export class SamplerYcbcrConversionCreateInfoKHR {
    static $gtype: GObject.GType<SamplerYcbcrConversionCreateInfoKHR>;

    constructor(copy: SamplerYcbcrConversionCreateInfoKHR);
}

export class SamplerYcbcrConversionInfoKHR {
    static $gtype: GObject.GType<SamplerYcbcrConversionInfoKHR>;

    constructor(copy: SamplerYcbcrConversionInfoKHR);
}

export class BindImagePlaneMemoryInfoKHR {
    static $gtype: GObject.GType<BindImagePlaneMemoryInfoKHR>;

    constructor(copy: BindImagePlaneMemoryInfoKHR);
}

export class ImagePlaneMemoryRequirementsInfoKHR {
    static $gtype: GObject.GType<ImagePlaneMemoryRequirementsInfoKHR>;

    constructor(copy: ImagePlaneMemoryRequirementsInfoKHR);
}

export class PhysicalDeviceSamplerYcbcrConversionFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceSamplerYcbcrConversionFeaturesKHR>;

    constructor(copy: PhysicalDeviceSamplerYcbcrConversionFeaturesKHR);
}

export class SamplerYcbcrConversionImageFormatPropertiesKHR {
    static $gtype: GObject.GType<SamplerYcbcrConversionImageFormatPropertiesKHR>;

    constructor(copy: SamplerYcbcrConversionImageFormatPropertiesKHR);
}

export class BindBufferMemoryInfoKHR {
    static $gtype: GObject.GType<BindBufferMemoryInfoKHR>;

    constructor(copy: BindBufferMemoryInfoKHR);
}

export class BindImageMemoryInfoKHR {
    static $gtype: GObject.GType<BindImageMemoryInfoKHR>;

    constructor(copy: BindImageMemoryInfoKHR);
}

export class PhysicalDeviceMaintenance3PropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance3PropertiesKHR>;

    constructor(copy: PhysicalDeviceMaintenance3PropertiesKHR);
}

export class DescriptorSetLayoutSupportKHR {
    static $gtype: GObject.GType<DescriptorSetLayoutSupportKHR>;

    constructor(copy: DescriptorSetLayoutSupportKHR);
}

export class PhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR);
}

export class PhysicalDevice8BitStorageFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDevice8BitStorageFeaturesKHR>;

    constructor(copy: PhysicalDevice8BitStorageFeaturesKHR);
}

export class PhysicalDeviceShaderAtomicInt64FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderAtomicInt64FeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderAtomicInt64FeaturesKHR);
}

export class PhysicalDeviceShaderClockFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderClockFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderClockFeaturesKHR);
}

export class VideoDecodeH265ProfileInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH265ProfileInfoKHR>;

    constructor(copy: VideoDecodeH265ProfileInfoKHR);
}

export class VideoDecodeH265CapabilitiesKHR {
    static $gtype: GObject.GType<VideoDecodeH265CapabilitiesKHR>;

    constructor(copy: VideoDecodeH265CapabilitiesKHR);
}

export class VideoDecodeH265SessionParametersAddInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH265SessionParametersAddInfoKHR>;

    constructor(copy: VideoDecodeH265SessionParametersAddInfoKHR);
}

export class VideoDecodeH265SessionParametersCreateInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH265SessionParametersCreateInfoKHR>;

    constructor(copy: VideoDecodeH265SessionParametersCreateInfoKHR);
}

export class VideoDecodeH265PictureInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH265PictureInfoKHR>;

    constructor(copy: VideoDecodeH265PictureInfoKHR);
}

export class VideoDecodeH265DpbSlotInfoKHR {
    static $gtype: GObject.GType<VideoDecodeH265DpbSlotInfoKHR>;

    constructor(copy: VideoDecodeH265DpbSlotInfoKHR);
}

export class QueueGlobalPriorityKHR {
    static $gtype: GObject.GType<QueueGlobalPriorityKHR>;

    constructor(copy: QueueGlobalPriorityKHR);
}

export class DeviceQueueGlobalPriorityCreateInfoKHR {
    static $gtype: GObject.GType<DeviceQueueGlobalPriorityCreateInfoKHR>;

    constructor(copy: DeviceQueueGlobalPriorityCreateInfoKHR);
}

export class PhysicalDeviceGlobalPriorityQueryFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceGlobalPriorityQueryFeaturesKHR>;

    constructor(copy: PhysicalDeviceGlobalPriorityQueryFeaturesKHR);
}

export class QueueFamilyGlobalPriorityPropertiesKHR {
    static $gtype: GObject.GType<QueueFamilyGlobalPriorityPropertiesKHR>;

    constructor(copy: QueueFamilyGlobalPriorityPropertiesKHR);
}

export class DriverIdKHR {
    static $gtype: GObject.GType<DriverIdKHR>;

    constructor(copy: DriverIdKHR);
}

export class ConformanceVersionKHR {
    static $gtype: GObject.GType<ConformanceVersionKHR>;

    constructor(copy: ConformanceVersionKHR);
}

export class PhysicalDeviceDriverPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceDriverPropertiesKHR>;

    constructor(copy: PhysicalDeviceDriverPropertiesKHR);
}

export class ShaderFloatControlsIndependenceKHR {
    static $gtype: GObject.GType<ShaderFloatControlsIndependenceKHR>;

    constructor(copy: ShaderFloatControlsIndependenceKHR);
}

export class PhysicalDeviceFloatControlsPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceFloatControlsPropertiesKHR>;

    constructor(copy: PhysicalDeviceFloatControlsPropertiesKHR);
}

export class ResolveModeFlagBitsKHR {
    static $gtype: GObject.GType<ResolveModeFlagBitsKHR>;

    constructor(copy: ResolveModeFlagBitsKHR);
}

export class ResolveModeFlagsKHR {
    static $gtype: GObject.GType<ResolveModeFlagsKHR>;

    constructor(copy: ResolveModeFlagsKHR);
}

export class SubpassDescriptionDepthStencilResolveKHR {
    static $gtype: GObject.GType<SubpassDescriptionDepthStencilResolveKHR>;

    constructor(copy: SubpassDescriptionDepthStencilResolveKHR);
}

export class PhysicalDeviceDepthStencilResolvePropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceDepthStencilResolvePropertiesKHR>;

    constructor(copy: PhysicalDeviceDepthStencilResolvePropertiesKHR);
}

export class SemaphoreTypeKHR {
    static $gtype: GObject.GType<SemaphoreTypeKHR>;

    constructor(copy: SemaphoreTypeKHR);
}

export class SemaphoreWaitFlagBitsKHR {
    static $gtype: GObject.GType<SemaphoreWaitFlagBitsKHR>;

    constructor(copy: SemaphoreWaitFlagBitsKHR);
}

export class SemaphoreWaitFlagsKHR {
    static $gtype: GObject.GType<SemaphoreWaitFlagsKHR>;

    constructor(copy: SemaphoreWaitFlagsKHR);
}

export class PhysicalDeviceTimelineSemaphoreFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceTimelineSemaphoreFeaturesKHR>;

    constructor(copy: PhysicalDeviceTimelineSemaphoreFeaturesKHR);
}

export class PhysicalDeviceTimelineSemaphorePropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceTimelineSemaphorePropertiesKHR>;

    constructor(copy: PhysicalDeviceTimelineSemaphorePropertiesKHR);
}

export class SemaphoreTypeCreateInfoKHR {
    static $gtype: GObject.GType<SemaphoreTypeCreateInfoKHR>;

    constructor(copy: SemaphoreTypeCreateInfoKHR);
}

export class TimelineSemaphoreSubmitInfoKHR {
    static $gtype: GObject.GType<TimelineSemaphoreSubmitInfoKHR>;

    constructor(copy: TimelineSemaphoreSubmitInfoKHR);
}

export class SemaphoreWaitInfoKHR {
    static $gtype: GObject.GType<SemaphoreWaitInfoKHR>;

    constructor(copy: SemaphoreWaitInfoKHR);
}

export class SemaphoreSignalInfoKHR {
    static $gtype: GObject.GType<SemaphoreSignalInfoKHR>;

    constructor(copy: SemaphoreSignalInfoKHR);
}

export class PhysicalDeviceVulkanMemoryModelFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceVulkanMemoryModelFeaturesKHR>;

    constructor(copy: PhysicalDeviceVulkanMemoryModelFeaturesKHR);
}

export class PhysicalDeviceShaderTerminateInvocationFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderTerminateInvocationFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderTerminateInvocationFeaturesKHR);
}

export class FragmentShadingRateCombinerOpKHR {
    static $gtype: GObject.GType<FragmentShadingRateCombinerOpKHR>;

    constructor(copy: FragmentShadingRateCombinerOpKHR);
}

export class FragmentShadingRateAttachmentInfoKHR {
    static $gtype: GObject.GType<FragmentShadingRateAttachmentInfoKHR>;

    constructor(copy: FragmentShadingRateAttachmentInfoKHR);
}

export class PipelineFragmentShadingRateStateCreateInfoKHR {
    static $gtype: GObject.GType<PipelineFragmentShadingRateStateCreateInfoKHR>;

    constructor(copy: PipelineFragmentShadingRateStateCreateInfoKHR);
}

export class PhysicalDeviceFragmentShadingRateFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShadingRateFeaturesKHR>;

    constructor(copy: PhysicalDeviceFragmentShadingRateFeaturesKHR);
}

export class PhysicalDeviceFragmentShadingRatePropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShadingRatePropertiesKHR>;

    constructor(copy: PhysicalDeviceFragmentShadingRatePropertiesKHR);
}

export class PhysicalDeviceFragmentShadingRateKHR {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShadingRateKHR>;

    constructor(copy: PhysicalDeviceFragmentShadingRateKHR);
}

export class PhysicalDeviceDynamicRenderingLocalReadFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceDynamicRenderingLocalReadFeaturesKHR>;

    constructor(copy: PhysicalDeviceDynamicRenderingLocalReadFeaturesKHR);
}

export class RenderingAttachmentLocationInfoKHR {
    static $gtype: GObject.GType<RenderingAttachmentLocationInfoKHR>;

    constructor(copy: RenderingAttachmentLocationInfoKHR);
}

export class RenderingInputAttachmentIndexInfoKHR {
    static $gtype: GObject.GType<RenderingInputAttachmentIndexInfoKHR>;

    constructor(copy: RenderingInputAttachmentIndexInfoKHR);
}

export class PhysicalDeviceShaderQuadControlFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderQuadControlFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderQuadControlFeaturesKHR);
}

export class SurfaceProtectedCapabilitiesKHR {
    static $gtype: GObject.GType<SurfaceProtectedCapabilitiesKHR>;

    constructor(copy: SurfaceProtectedCapabilitiesKHR);
}

export class PhysicalDeviceSeparateDepthStencilLayoutsFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceSeparateDepthStencilLayoutsFeaturesKHR>;

    constructor(copy: PhysicalDeviceSeparateDepthStencilLayoutsFeaturesKHR);
}

export class AttachmentReferenceStencilLayoutKHR {
    static $gtype: GObject.GType<AttachmentReferenceStencilLayoutKHR>;

    constructor(copy: AttachmentReferenceStencilLayoutKHR);
}

export class AttachmentDescriptionStencilLayoutKHR {
    static $gtype: GObject.GType<AttachmentDescriptionStencilLayoutKHR>;

    constructor(copy: AttachmentDescriptionStencilLayoutKHR);
}

export class PhysicalDevicePresentWaitFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDevicePresentWaitFeaturesKHR>;

    constructor(copy: PhysicalDevicePresentWaitFeaturesKHR);
}

export class PhysicalDeviceUniformBufferStandardLayoutFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceUniformBufferStandardLayoutFeaturesKHR>;

    constructor(copy: PhysicalDeviceUniformBufferStandardLayoutFeaturesKHR);
}

export class PhysicalDeviceBufferDeviceAddressFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceBufferDeviceAddressFeaturesKHR>;

    constructor(copy: PhysicalDeviceBufferDeviceAddressFeaturesKHR);
}

export class BufferDeviceAddressInfoKHR {
    static $gtype: GObject.GType<BufferDeviceAddressInfoKHR>;

    constructor(copy: BufferDeviceAddressInfoKHR);
}

export class BufferOpaqueCaptureAddressCreateInfoKHR {
    static $gtype: GObject.GType<BufferOpaqueCaptureAddressCreateInfoKHR>;

    constructor(copy: BufferOpaqueCaptureAddressCreateInfoKHR);
}

export class MemoryOpaqueCaptureAddressAllocateInfoKHR {
    static $gtype: GObject.GType<MemoryOpaqueCaptureAddressAllocateInfoKHR>;

    constructor(copy: MemoryOpaqueCaptureAddressAllocateInfoKHR);
}

export class DeviceMemoryOpaqueCaptureAddressInfoKHR {
    static $gtype: GObject.GType<DeviceMemoryOpaqueCaptureAddressInfoKHR>;

    constructor(copy: DeviceMemoryOpaqueCaptureAddressInfoKHR);
}

export class DeferredOperationKHR {
    static $gtype: GObject.GType<DeferredOperationKHR>;

    constructor(copy: DeferredOperationKHR);
}

export class PipelineExecutableStatisticFormatKHR {
    static $gtype: GObject.GType<PipelineExecutableStatisticFormatKHR>;

    constructor(copy: PipelineExecutableStatisticFormatKHR);
}

export class PhysicalDevicePipelineExecutablePropertiesFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDevicePipelineExecutablePropertiesFeaturesKHR>;

    constructor(copy: PhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
}

export class PipelineInfoKHR {
    static $gtype: GObject.GType<PipelineInfoKHR>;

    constructor(copy: PipelineInfoKHR);
}

export class PipelineExecutablePropertiesKHR {
    static $gtype: GObject.GType<PipelineExecutablePropertiesKHR>;

    constructor(copy: PipelineExecutablePropertiesKHR);
}

export class PipelineExecutableInfoKHR {
    static $gtype: GObject.GType<PipelineExecutableInfoKHR>;

    constructor(copy: PipelineExecutableInfoKHR);
}

export class PipelineExecutableStatisticValueKHR {
    static $gtype: GObject.GType<PipelineExecutableStatisticValueKHR>;

    constructor(copy: PipelineExecutableStatisticValueKHR);
}

export class PipelineExecutableStatisticKHR {
    static $gtype: GObject.GType<PipelineExecutableStatisticKHR>;

    constructor(copy: PipelineExecutableStatisticKHR);
}

export class PipelineExecutableInternalRepresentationKHR {
    static $gtype: GObject.GType<PipelineExecutableInternalRepresentationKHR>;

    constructor(copy: PipelineExecutableInternalRepresentationKHR);
}

export class MemoryUnmapFlagBitsKHR {
    static $gtype: GObject.GType<MemoryUnmapFlagBitsKHR>;

    constructor(copy: MemoryUnmapFlagBitsKHR);
}

export class MemoryUnmapFlagsKHR {
    static $gtype: GObject.GType<MemoryUnmapFlagsKHR>;

    constructor(copy: MemoryUnmapFlagsKHR);
}

export class MemoryMapInfoKHR {
    static $gtype: GObject.GType<MemoryMapInfoKHR>;

    constructor(copy: MemoryMapInfoKHR);
}

export class MemoryUnmapInfoKHR {
    static $gtype: GObject.GType<MemoryUnmapInfoKHR>;

    constructor(copy: MemoryUnmapInfoKHR);
}

export class PhysicalDeviceShaderIntegerDotProductFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderIntegerDotProductFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderIntegerDotProductFeaturesKHR);
}

export class PhysicalDeviceShaderIntegerDotProductPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderIntegerDotProductPropertiesKHR>;

    constructor(copy: PhysicalDeviceShaderIntegerDotProductPropertiesKHR);
}

export class PipelineLibraryCreateInfoKHR {
    static $gtype: GObject.GType<PipelineLibraryCreateInfoKHR>;

    constructor(copy: PipelineLibraryCreateInfoKHR);
}

export class PresentIdKHR {
    static $gtype: GObject.GType<PresentIdKHR>;

    constructor(copy: PresentIdKHR);
}

export class PhysicalDevicePresentIdFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDevicePresentIdFeaturesKHR>;

    constructor(copy: PhysicalDevicePresentIdFeaturesKHR);
}

export class VideoEncodeTuningModeKHR {
    static $gtype: GObject.GType<VideoEncodeTuningModeKHR>;

    constructor(copy: VideoEncodeTuningModeKHR);
}

export class VideoEncodeFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeFlagBitsKHR>;

    constructor(copy: VideoEncodeFlagBitsKHR);
}

export class VideoEncodeFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeFlagsKHR>;

    constructor(copy: VideoEncodeFlagsKHR);
}

export class VideoEncodeCapabilityFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeCapabilityFlagBitsKHR>;

    constructor(copy: VideoEncodeCapabilityFlagBitsKHR);
}

export class VideoEncodeCapabilityFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeCapabilityFlagsKHR>;

    constructor(copy: VideoEncodeCapabilityFlagsKHR);
}

export class VideoEncodeRateControlModeFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeRateControlModeFlagBitsKHR>;

    constructor(copy: VideoEncodeRateControlModeFlagBitsKHR);
}

export class VideoEncodeRateControlModeFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeRateControlModeFlagsKHR>;

    constructor(copy: VideoEncodeRateControlModeFlagsKHR);
}

export class VideoEncodeFeedbackFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeFeedbackFlagBitsKHR>;

    constructor(copy: VideoEncodeFeedbackFlagBitsKHR);
}

export class VideoEncodeFeedbackFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeFeedbackFlagsKHR>;

    constructor(copy: VideoEncodeFeedbackFlagsKHR);
}

export class VideoEncodeUsageFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeUsageFlagBitsKHR>;

    constructor(copy: VideoEncodeUsageFlagBitsKHR);
}

export class VideoEncodeUsageFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeUsageFlagsKHR>;

    constructor(copy: VideoEncodeUsageFlagsKHR);
}

export class VideoEncodeContentFlagBitsKHR {
    static $gtype: GObject.GType<VideoEncodeContentFlagBitsKHR>;

    constructor(copy: VideoEncodeContentFlagBitsKHR);
}

export class VideoEncodeContentFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeContentFlagsKHR>;

    constructor(copy: VideoEncodeContentFlagsKHR);
}

export class VideoEncodeRateControlFlagsKHR {
    static $gtype: GObject.GType<VideoEncodeRateControlFlagsKHR>;

    constructor(copy: VideoEncodeRateControlFlagsKHR);
}

export class VideoEncodeInfoKHR {
    static $gtype: GObject.GType<VideoEncodeInfoKHR>;

    constructor(copy: VideoEncodeInfoKHR);
}

export class VideoEncodeCapabilitiesKHR {
    static $gtype: GObject.GType<VideoEncodeCapabilitiesKHR>;

    constructor(copy: VideoEncodeCapabilitiesKHR);
}

export class QueryPoolVideoEncodeFeedbackCreateInfoKHR {
    static $gtype: GObject.GType<QueryPoolVideoEncodeFeedbackCreateInfoKHR>;

    constructor(copy: QueryPoolVideoEncodeFeedbackCreateInfoKHR);
}

export class VideoEncodeUsageInfoKHR {
    static $gtype: GObject.GType<VideoEncodeUsageInfoKHR>;

    constructor(copy: VideoEncodeUsageInfoKHR);
}

export class VideoEncodeRateControlLayerInfoKHR {
    static $gtype: GObject.GType<VideoEncodeRateControlLayerInfoKHR>;

    constructor(copy: VideoEncodeRateControlLayerInfoKHR);
}

export class VideoEncodeRateControlInfoKHR {
    static $gtype: GObject.GType<VideoEncodeRateControlInfoKHR>;

    constructor(copy: VideoEncodeRateControlInfoKHR);
}

export class PhysicalDeviceVideoEncodeQualityLevelInfoKHR {
    static $gtype: GObject.GType<PhysicalDeviceVideoEncodeQualityLevelInfoKHR>;

    constructor(copy: PhysicalDeviceVideoEncodeQualityLevelInfoKHR);
}

export class VideoEncodeQualityLevelPropertiesKHR {
    static $gtype: GObject.GType<VideoEncodeQualityLevelPropertiesKHR>;

    constructor(copy: VideoEncodeQualityLevelPropertiesKHR);
}

export class VideoEncodeQualityLevelInfoKHR {
    static $gtype: GObject.GType<VideoEncodeQualityLevelInfoKHR>;

    constructor(copy: VideoEncodeQualityLevelInfoKHR);
}

export class VideoEncodeSessionParametersGetInfoKHR {
    static $gtype: GObject.GType<VideoEncodeSessionParametersGetInfoKHR>;

    constructor(copy: VideoEncodeSessionParametersGetInfoKHR);
}

export class VideoEncodeSessionParametersFeedbackInfoKHR {
    static $gtype: GObject.GType<VideoEncodeSessionParametersFeedbackInfoKHR>;

    constructor(copy: VideoEncodeSessionParametersFeedbackInfoKHR);
}

export class PipelineStageFlags2KHR {
    static $gtype: GObject.GType<PipelineStageFlags2KHR>;

    constructor(copy: PipelineStageFlags2KHR);
}

export class PipelineStageFlagBits2KHR {
    static $gtype: GObject.GType<PipelineStageFlagBits2KHR>;

    constructor(copy: PipelineStageFlagBits2KHR);
}

export class AccessFlags2KHR {
    static $gtype: GObject.GType<AccessFlags2KHR>;

    constructor(copy: AccessFlags2KHR);
}

export class AccessFlagBits2KHR {
    static $gtype: GObject.GType<AccessFlagBits2KHR>;

    constructor(copy: AccessFlagBits2KHR);
}

export class SubmitFlagBitsKHR {
    static $gtype: GObject.GType<SubmitFlagBitsKHR>;

    constructor(copy: SubmitFlagBitsKHR);
}

export class SubmitFlagsKHR {
    static $gtype: GObject.GType<SubmitFlagsKHR>;

    constructor(copy: SubmitFlagsKHR);
}

export class MemoryBarrier2KHR {
    static $gtype: GObject.GType<MemoryBarrier2KHR>;

    constructor(copy: MemoryBarrier2KHR);
}

export class BufferMemoryBarrier2KHR {
    static $gtype: GObject.GType<BufferMemoryBarrier2KHR>;

    constructor(copy: BufferMemoryBarrier2KHR);
}

export class ImageMemoryBarrier2KHR {
    static $gtype: GObject.GType<ImageMemoryBarrier2KHR>;

    constructor(copy: ImageMemoryBarrier2KHR);
}

export class DependencyInfoKHR {
    static $gtype: GObject.GType<DependencyInfoKHR>;

    constructor(copy: DependencyInfoKHR);
}

export class SubmitInfo2KHR {
    static $gtype: GObject.GType<SubmitInfo2KHR>;

    constructor(copy: SubmitInfo2KHR);
}

export class SemaphoreSubmitInfoKHR {
    static $gtype: GObject.GType<SemaphoreSubmitInfoKHR>;

    constructor(copy: SemaphoreSubmitInfoKHR);
}

export class CommandBufferSubmitInfoKHR {
    static $gtype: GObject.GType<CommandBufferSubmitInfoKHR>;

    constructor(copy: CommandBufferSubmitInfoKHR);
}

export class PhysicalDeviceSynchronization2FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceSynchronization2FeaturesKHR>;

    constructor(copy: PhysicalDeviceSynchronization2FeaturesKHR);
}

export class QueueFamilyCheckpointProperties2NV {
    static $gtype: GObject.GType<QueueFamilyCheckpointProperties2NV>;

    constructor(copy: QueueFamilyCheckpointProperties2NV);
}

export class CheckpointData2NV {
    static $gtype: GObject.GType<CheckpointData2NV>;

    constructor(copy: CheckpointData2NV);
}

export class PhysicalDeviceFragmentShaderBarycentricFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShaderBarycentricFeaturesKHR>;

    constructor(copy: PhysicalDeviceFragmentShaderBarycentricFeaturesKHR);
}

export class PhysicalDeviceFragmentShaderBarycentricPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShaderBarycentricPropertiesKHR>;

    constructor(copy: PhysicalDeviceFragmentShaderBarycentricPropertiesKHR);
}

export class PhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR);
}

export class PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR>;

    constructor(copy: PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR);
}

export class PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR>;

    constructor(copy: PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR);
}

export class CopyBufferInfo2KHR {
    static $gtype: GObject.GType<CopyBufferInfo2KHR>;

    constructor(copy: CopyBufferInfo2KHR);
}

export class CopyImageInfo2KHR {
    static $gtype: GObject.GType<CopyImageInfo2KHR>;

    constructor(copy: CopyImageInfo2KHR);
}

export class CopyBufferToImageInfo2KHR {
    static $gtype: GObject.GType<CopyBufferToImageInfo2KHR>;

    constructor(copy: CopyBufferToImageInfo2KHR);
}

export class CopyImageToBufferInfo2KHR {
    static $gtype: GObject.GType<CopyImageToBufferInfo2KHR>;

    constructor(copy: CopyImageToBufferInfo2KHR);
}

export class BlitImageInfo2KHR {
    static $gtype: GObject.GType<BlitImageInfo2KHR>;

    constructor(copy: BlitImageInfo2KHR);
}

export class ResolveImageInfo2KHR {
    static $gtype: GObject.GType<ResolveImageInfo2KHR>;

    constructor(copy: ResolveImageInfo2KHR);
}

export class BufferCopy2KHR {
    static $gtype: GObject.GType<BufferCopy2KHR>;

    constructor(copy: BufferCopy2KHR);
}

export class ImageCopy2KHR {
    static $gtype: GObject.GType<ImageCopy2KHR>;

    constructor(copy: ImageCopy2KHR);
}

export class ImageBlit2KHR {
    static $gtype: GObject.GType<ImageBlit2KHR>;

    constructor(copy: ImageBlit2KHR);
}

export class BufferImageCopy2KHR {
    static $gtype: GObject.GType<BufferImageCopy2KHR>;

    constructor(copy: BufferImageCopy2KHR);
}

export class ImageResolve2KHR {
    static $gtype: GObject.GType<ImageResolve2KHR>;

    constructor(copy: ImageResolve2KHR);
}

export class FormatFeatureFlags2KHR {
    static $gtype: GObject.GType<FormatFeatureFlags2KHR>;

    constructor(copy: FormatFeatureFlags2KHR);
}

export class FormatFeatureFlagBits2KHR {
    static $gtype: GObject.GType<FormatFeatureFlagBits2KHR>;

    constructor(copy: FormatFeatureFlagBits2KHR);
}

export class FormatProperties3KHR {
    static $gtype: GObject.GType<FormatProperties3KHR>;

    constructor(copy: FormatProperties3KHR);
}

export class PhysicalDeviceRayTracingMaintenance1FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingMaintenance1FeaturesKHR>;

    constructor(copy: PhysicalDeviceRayTracingMaintenance1FeaturesKHR);
}

export class TraceRaysIndirectCommand2KHR {
    static $gtype: GObject.GType<TraceRaysIndirectCommand2KHR>;

    constructor(copy: TraceRaysIndirectCommand2KHR);
}

export class PhysicalDeviceMaintenance4FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance4FeaturesKHR>;

    constructor(copy: PhysicalDeviceMaintenance4FeaturesKHR);
}

export class PhysicalDeviceMaintenance4PropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance4PropertiesKHR>;

    constructor(copy: PhysicalDeviceMaintenance4PropertiesKHR);
}

export class DeviceBufferMemoryRequirementsKHR {
    static $gtype: GObject.GType<DeviceBufferMemoryRequirementsKHR>;

    constructor(copy: DeviceBufferMemoryRequirementsKHR);
}

export class DeviceImageMemoryRequirementsKHR {
    static $gtype: GObject.GType<DeviceImageMemoryRequirementsKHR>;

    constructor(copy: DeviceImageMemoryRequirementsKHR);
}

export class PhysicalDeviceShaderSubgroupRotateFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderSubgroupRotateFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderSubgroupRotateFeaturesKHR);
}

export class PhysicalDeviceShaderMaximalReconvergenceFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderMaximalReconvergenceFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderMaximalReconvergenceFeaturesKHR);
}

export class PipelineCreateFlags2KHR {
    static $gtype: GObject.GType<PipelineCreateFlags2KHR>;

    constructor(copy: PipelineCreateFlags2KHR);
}

export class PipelineCreateFlagBits2KHR {
    static $gtype: GObject.GType<PipelineCreateFlagBits2KHR>;

    constructor(copy: PipelineCreateFlagBits2KHR);
}

export class BufferUsageFlags2KHR {
    static $gtype: GObject.GType<BufferUsageFlags2KHR>;

    constructor(copy: BufferUsageFlags2KHR);
}

export class BufferUsageFlagBits2KHR {
    static $gtype: GObject.GType<BufferUsageFlagBits2KHR>;

    constructor(copy: BufferUsageFlagBits2KHR);
}

export class PhysicalDeviceMaintenance5FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance5FeaturesKHR>;

    constructor(copy: PhysicalDeviceMaintenance5FeaturesKHR);
}

export class PhysicalDeviceMaintenance5PropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance5PropertiesKHR>;

    constructor(copy: PhysicalDeviceMaintenance5PropertiesKHR);
}

export class RenderingAreaInfoKHR {
    static $gtype: GObject.GType<RenderingAreaInfoKHR>;

    constructor(copy: RenderingAreaInfoKHR);
}

export class ImageSubresource2KHR {
    static $gtype: GObject.GType<ImageSubresource2KHR>;

    constructor(copy: ImageSubresource2KHR);
}

export class DeviceImageSubresourceInfoKHR {
    static $gtype: GObject.GType<DeviceImageSubresourceInfoKHR>;

    constructor(copy: DeviceImageSubresourceInfoKHR);
}

export class SubresourceLayout2KHR {
    static $gtype: GObject.GType<SubresourceLayout2KHR>;

    constructor(copy: SubresourceLayout2KHR);
}

export class PipelineCreateFlags2CreateInfoKHR {
    static $gtype: GObject.GType<PipelineCreateFlags2CreateInfoKHR>;

    constructor(copy: PipelineCreateFlags2CreateInfoKHR);
}

export class BufferUsageFlags2CreateInfoKHR {
    static $gtype: GObject.GType<BufferUsageFlags2CreateInfoKHR>;

    constructor(copy: BufferUsageFlags2CreateInfoKHR);
}

export class PhysicalDeviceRayTracingPositionFetchFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingPositionFetchFeaturesKHR>;

    constructor(copy: PhysicalDeviceRayTracingPositionFetchFeaturesKHR);
}

export class ComponentTypeKHR {
    static $gtype: GObject.GType<ComponentTypeKHR>;

    constructor(copy: ComponentTypeKHR);
}

export class ScopeKHR {
    static $gtype: GObject.GType<ScopeKHR>;

    constructor(copy: ScopeKHR);
}

export class CooperativeMatrixPropertiesKHR {
    static $gtype: GObject.GType<CooperativeMatrixPropertiesKHR>;

    constructor(copy: CooperativeMatrixPropertiesKHR);
}

export class PhysicalDeviceCooperativeMatrixFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceCooperativeMatrixFeaturesKHR>;

    constructor(copy: PhysicalDeviceCooperativeMatrixFeaturesKHR);
}

export class PhysicalDeviceCooperativeMatrixPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceCooperativeMatrixPropertiesKHR>;

    constructor(copy: PhysicalDeviceCooperativeMatrixPropertiesKHR);
}

export class VideoDecodeAV1ProfileInfoKHR {
    static $gtype: GObject.GType<VideoDecodeAV1ProfileInfoKHR>;

    constructor(copy: VideoDecodeAV1ProfileInfoKHR);
}

export class VideoDecodeAV1CapabilitiesKHR {
    static $gtype: GObject.GType<VideoDecodeAV1CapabilitiesKHR>;

    constructor(copy: VideoDecodeAV1CapabilitiesKHR);
}

export class VideoDecodeAV1SessionParametersCreateInfoKHR {
    static $gtype: GObject.GType<VideoDecodeAV1SessionParametersCreateInfoKHR>;

    constructor(copy: VideoDecodeAV1SessionParametersCreateInfoKHR);
}

export class VideoDecodeAV1PictureInfoKHR {
    static $gtype: GObject.GType<VideoDecodeAV1PictureInfoKHR>;

    constructor(copy: VideoDecodeAV1PictureInfoKHR);
}

export class VideoDecodeAV1DpbSlotInfoKHR {
    static $gtype: GObject.GType<VideoDecodeAV1DpbSlotInfoKHR>;

    constructor(copy: VideoDecodeAV1DpbSlotInfoKHR);
}

export class PhysicalDeviceVideoMaintenance1FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceVideoMaintenance1FeaturesKHR>;

    constructor(copy: PhysicalDeviceVideoMaintenance1FeaturesKHR);
}

export class VideoInlineQueryInfoKHR {
    static $gtype: GObject.GType<VideoInlineQueryInfoKHR>;

    constructor(copy: VideoInlineQueryInfoKHR);
}

export class PhysicalDeviceVertexAttributeDivisorPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceVertexAttributeDivisorPropertiesKHR>;

    constructor(copy: PhysicalDeviceVertexAttributeDivisorPropertiesKHR);
}

export class VertexInputBindingDivisorDescriptionKHR {
    static $gtype: GObject.GType<VertexInputBindingDivisorDescriptionKHR>;

    constructor(copy: VertexInputBindingDivisorDescriptionKHR);
}

export class PipelineVertexInputDivisorStateCreateInfoKHR {
    static $gtype: GObject.GType<PipelineVertexInputDivisorStateCreateInfoKHR>;

    constructor(copy: PipelineVertexInputDivisorStateCreateInfoKHR);
}

export class PhysicalDeviceVertexAttributeDivisorFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceVertexAttributeDivisorFeaturesKHR>;

    constructor(copy: PhysicalDeviceVertexAttributeDivisorFeaturesKHR);
}

export class PhysicalDeviceShaderFloatControls2FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderFloatControls2FeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderFloatControls2FeaturesKHR);
}

export class PhysicalDeviceIndexTypeUint8FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceIndexTypeUint8FeaturesKHR>;

    constructor(copy: PhysicalDeviceIndexTypeUint8FeaturesKHR);
}

export class LineRasterizationModeKHR {
    static $gtype: GObject.GType<LineRasterizationModeKHR>;

    constructor(copy: LineRasterizationModeKHR);
}

export class PhysicalDeviceLineRasterizationFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceLineRasterizationFeaturesKHR>;

    constructor(copy: PhysicalDeviceLineRasterizationFeaturesKHR);
}

export class PhysicalDeviceLineRasterizationPropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceLineRasterizationPropertiesKHR>;

    constructor(copy: PhysicalDeviceLineRasterizationPropertiesKHR);
}

export class PipelineRasterizationLineStateCreateInfoKHR {
    static $gtype: GObject.GType<PipelineRasterizationLineStateCreateInfoKHR>;

    constructor(copy: PipelineRasterizationLineStateCreateInfoKHR);
}

export class TimeDomainKHR {
    static $gtype: GObject.GType<TimeDomainKHR>;

    constructor(copy: TimeDomainKHR);
}

export class CalibratedTimestampInfoKHR {
    static $gtype: GObject.GType<CalibratedTimestampInfoKHR>;

    constructor(copy: CalibratedTimestampInfoKHR);
}

export class PhysicalDeviceShaderExpectAssumeFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceShaderExpectAssumeFeaturesKHR>;

    constructor(copy: PhysicalDeviceShaderExpectAssumeFeaturesKHR);
}

export class PhysicalDeviceMaintenance6FeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance6FeaturesKHR>;

    constructor(copy: PhysicalDeviceMaintenance6FeaturesKHR);
}

export class PhysicalDeviceMaintenance6PropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceMaintenance6PropertiesKHR>;

    constructor(copy: PhysicalDeviceMaintenance6PropertiesKHR);
}

export class BindMemoryStatusKHR {
    static $gtype: GObject.GType<BindMemoryStatusKHR>;

    constructor(copy: BindMemoryStatusKHR);
}

export class BindDescriptorSetsInfoKHR {
    static $gtype: GObject.GType<BindDescriptorSetsInfoKHR>;

    constructor(copy: BindDescriptorSetsInfoKHR);
}

export class PushConstantsInfoKHR {
    static $gtype: GObject.GType<PushConstantsInfoKHR>;

    constructor(copy: PushConstantsInfoKHR);
}

export class PushDescriptorSetInfoKHR {
    static $gtype: GObject.GType<PushDescriptorSetInfoKHR>;

    constructor(copy: PushDescriptorSetInfoKHR);
}

export class PushDescriptorSetWithTemplateInfoKHR {
    static $gtype: GObject.GType<PushDescriptorSetWithTemplateInfoKHR>;

    constructor(copy: PushDescriptorSetWithTemplateInfoKHR);
}

export class SetDescriptorBufferOffsetsInfoEXT {
    static $gtype: GObject.GType<SetDescriptorBufferOffsetsInfoEXT>;

    constructor(copy: SetDescriptorBufferOffsetsInfoEXT);
}

export class BindDescriptorBufferEmbeddedSamplersInfoEXT {
    static $gtype: GObject.GType<BindDescriptorBufferEmbeddedSamplersInfoEXT>;

    constructor(copy: BindDescriptorBufferEmbeddedSamplersInfoEXT);
}

export class DebugReportCallbackEXT {
    static $gtype: GObject.GType<DebugReportCallbackEXT>;

    constructor(copy: DebugReportCallbackEXT);
}

export class DebugReportObjectTypeEXT {
    static $gtype: GObject.GType<DebugReportObjectTypeEXT>;

    constructor(copy: DebugReportObjectTypeEXT);
}

export class DebugReportFlagBitsEXT {
    static $gtype: GObject.GType<DebugReportFlagBitsEXT>;

    constructor(copy: DebugReportFlagBitsEXT);
}

export class DebugReportFlagsEXT {
    static $gtype: GObject.GType<DebugReportFlagsEXT>;

    constructor(copy: DebugReportFlagsEXT);
}

export class DebugReportCallbackCreateInfoEXT {
    static $gtype: GObject.GType<DebugReportCallbackCreateInfoEXT>;

    constructor(copy: DebugReportCallbackCreateInfoEXT);
}

export class RasterizationOrderAMD {
    static $gtype: GObject.GType<RasterizationOrderAMD>;

    constructor(copy: RasterizationOrderAMD);
}

export class PipelineRasterizationStateRasterizationOrderAMD {
    static $gtype: GObject.GType<PipelineRasterizationStateRasterizationOrderAMD>;

    constructor(copy: PipelineRasterizationStateRasterizationOrderAMD);
}

export class DebugMarkerObjectNameInfoEXT {
    static $gtype: GObject.GType<DebugMarkerObjectNameInfoEXT>;

    constructor(copy: DebugMarkerObjectNameInfoEXT);
}

export class DebugMarkerObjectTagInfoEXT {
    static $gtype: GObject.GType<DebugMarkerObjectTagInfoEXT>;

    constructor(copy: DebugMarkerObjectTagInfoEXT);
}

export class DebugMarkerMarkerInfoEXT {
    static $gtype: GObject.GType<DebugMarkerMarkerInfoEXT>;

    constructor(copy: DebugMarkerMarkerInfoEXT);
}

export class DedicatedAllocationImageCreateInfoNV {
    static $gtype: GObject.GType<DedicatedAllocationImageCreateInfoNV>;

    constructor(copy: DedicatedAllocationImageCreateInfoNV);
}

export class DedicatedAllocationBufferCreateInfoNV {
    static $gtype: GObject.GType<DedicatedAllocationBufferCreateInfoNV>;

    constructor(copy: DedicatedAllocationBufferCreateInfoNV);
}

export class DedicatedAllocationMemoryAllocateInfoNV {
    static $gtype: GObject.GType<DedicatedAllocationMemoryAllocateInfoNV>;

    constructor(copy: DedicatedAllocationMemoryAllocateInfoNV);
}

export class PipelineRasterizationStateStreamCreateFlagsEXT {
    static $gtype: GObject.GType<PipelineRasterizationStateStreamCreateFlagsEXT>;

    constructor(copy: PipelineRasterizationStateStreamCreateFlagsEXT);
}

export class PhysicalDeviceTransformFeedbackFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceTransformFeedbackFeaturesEXT>;

    constructor(copy: PhysicalDeviceTransformFeedbackFeaturesEXT);
}

export class PhysicalDeviceTransformFeedbackPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceTransformFeedbackPropertiesEXT>;

    constructor(copy: PhysicalDeviceTransformFeedbackPropertiesEXT);
}

export class PipelineRasterizationStateStreamCreateInfoEXT {
    static $gtype: GObject.GType<PipelineRasterizationStateStreamCreateInfoEXT>;

    constructor(copy: PipelineRasterizationStateStreamCreateInfoEXT);
}

export class CuModuleNVX {
    static $gtype: GObject.GType<CuModuleNVX>;

    constructor(copy: CuModuleNVX);
}

export class CuFunctionNVX {
    static $gtype: GObject.GType<CuFunctionNVX>;

    constructor(copy: CuFunctionNVX);
}

export class CuModuleCreateInfoNVX {
    static $gtype: GObject.GType<CuModuleCreateInfoNVX>;

    constructor(copy: CuModuleCreateInfoNVX);
}

export class CuFunctionCreateInfoNVX {
    static $gtype: GObject.GType<CuFunctionCreateInfoNVX>;

    constructor(copy: CuFunctionCreateInfoNVX);
}

export class CuLaunchInfoNVX {
    static $gtype: GObject.GType<CuLaunchInfoNVX>;

    constructor(copy: CuLaunchInfoNVX);
}

export class ImageViewHandleInfoNVX {
    static $gtype: GObject.GType<ImageViewHandleInfoNVX>;

    constructor(copy: ImageViewHandleInfoNVX);
}

export class ImageViewAddressPropertiesNVX {
    static $gtype: GObject.GType<ImageViewAddressPropertiesNVX>;

    constructor(copy: ImageViewAddressPropertiesNVX);
}

export class TextureLODGatherFormatPropertiesAMD {
    static $gtype: GObject.GType<TextureLODGatherFormatPropertiesAMD>;

    constructor(copy: TextureLODGatherFormatPropertiesAMD);
}

export class ShaderInfoTypeAMD {
    static $gtype: GObject.GType<ShaderInfoTypeAMD>;

    constructor(copy: ShaderInfoTypeAMD);
}

export class ShaderResourceUsageAMD {
    static $gtype: GObject.GType<ShaderResourceUsageAMD>;

    constructor(copy: ShaderResourceUsageAMD);
}

export class ShaderStatisticsInfoAMD {
    static $gtype: GObject.GType<ShaderStatisticsInfoAMD>;

    constructor(copy: ShaderStatisticsInfoAMD);
}

export class PhysicalDeviceCornerSampledImageFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceCornerSampledImageFeaturesNV>;

    constructor(copy: PhysicalDeviceCornerSampledImageFeaturesNV);
}

export class ExternalMemoryHandleTypeFlagBitsNV {
    static $gtype: GObject.GType<ExternalMemoryHandleTypeFlagBitsNV>;

    constructor(copy: ExternalMemoryHandleTypeFlagBitsNV);
}

export class ExternalMemoryHandleTypeFlagsNV {
    static $gtype: GObject.GType<ExternalMemoryHandleTypeFlagsNV>;

    constructor(copy: ExternalMemoryHandleTypeFlagsNV);
}

export class ExternalMemoryFeatureFlagBitsNV {
    static $gtype: GObject.GType<ExternalMemoryFeatureFlagBitsNV>;

    constructor(copy: ExternalMemoryFeatureFlagBitsNV);
}

export class ExternalMemoryFeatureFlagsNV {
    static $gtype: GObject.GType<ExternalMemoryFeatureFlagsNV>;

    constructor(copy: ExternalMemoryFeatureFlagsNV);
}

export class ExternalImageFormatPropertiesNV {
    static $gtype: GObject.GType<ExternalImageFormatPropertiesNV>;

    constructor(copy: ExternalImageFormatPropertiesNV);
}

export class ExternalMemoryImageCreateInfoNV {
    static $gtype: GObject.GType<ExternalMemoryImageCreateInfoNV>;

    constructor(copy: ExternalMemoryImageCreateInfoNV);
}

export class ExportMemoryAllocateInfoNV {
    static $gtype: GObject.GType<ExportMemoryAllocateInfoNV>;

    constructor(copy: ExportMemoryAllocateInfoNV);
}

export class ValidationCheckEXT {
    static $gtype: GObject.GType<ValidationCheckEXT>;

    constructor(copy: ValidationCheckEXT);
}

export class ValidationFlagsEXT {
    static $gtype: GObject.GType<ValidationFlagsEXT>;

    constructor(copy: ValidationFlagsEXT);
}

export class PhysicalDeviceTextureCompressionASTCHDRFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceTextureCompressionASTCHDRFeaturesEXT>;

    constructor(copy: PhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
}

export class ImageViewASTCDecodeModeEXT {
    static $gtype: GObject.GType<ImageViewASTCDecodeModeEXT>;

    constructor(copy: ImageViewASTCDecodeModeEXT);
}

export class PhysicalDeviceASTCDecodeFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceASTCDecodeFeaturesEXT>;

    constructor(copy: PhysicalDeviceASTCDecodeFeaturesEXT);
}

export class PipelineRobustnessBufferBehaviorEXT {
    static $gtype: GObject.GType<PipelineRobustnessBufferBehaviorEXT>;

    constructor(copy: PipelineRobustnessBufferBehaviorEXT);
}

export class PipelineRobustnessImageBehaviorEXT {
    static $gtype: GObject.GType<PipelineRobustnessImageBehaviorEXT>;

    constructor(copy: PipelineRobustnessImageBehaviorEXT);
}

export class PhysicalDevicePipelineRobustnessFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePipelineRobustnessFeaturesEXT>;

    constructor(copy: PhysicalDevicePipelineRobustnessFeaturesEXT);
}

export class PhysicalDevicePipelineRobustnessPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDevicePipelineRobustnessPropertiesEXT>;

    constructor(copy: PhysicalDevicePipelineRobustnessPropertiesEXT);
}

export class PipelineRobustnessCreateInfoEXT {
    static $gtype: GObject.GType<PipelineRobustnessCreateInfoEXT>;

    constructor(copy: PipelineRobustnessCreateInfoEXT);
}

export class ConditionalRenderingFlagBitsEXT {
    static $gtype: GObject.GType<ConditionalRenderingFlagBitsEXT>;

    constructor(copy: ConditionalRenderingFlagBitsEXT);
}

export class ConditionalRenderingFlagsEXT {
    static $gtype: GObject.GType<ConditionalRenderingFlagsEXT>;

    constructor(copy: ConditionalRenderingFlagsEXT);
}

export class ConditionalRenderingBeginInfoEXT {
    static $gtype: GObject.GType<ConditionalRenderingBeginInfoEXT>;

    constructor(copy: ConditionalRenderingBeginInfoEXT);
}

export class PhysicalDeviceConditionalRenderingFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceConditionalRenderingFeaturesEXT>;

    constructor(copy: PhysicalDeviceConditionalRenderingFeaturesEXT);
}

export class CommandBufferInheritanceConditionalRenderingInfoEXT {
    static $gtype: GObject.GType<CommandBufferInheritanceConditionalRenderingInfoEXT>;

    constructor(copy: CommandBufferInheritanceConditionalRenderingInfoEXT);
}

export class ViewportWScalingNV {
    static $gtype: GObject.GType<ViewportWScalingNV>;

    constructor(copy: ViewportWScalingNV);
}

export class PipelineViewportWScalingStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineViewportWScalingStateCreateInfoNV>;

    constructor(copy: PipelineViewportWScalingStateCreateInfoNV);
}

export class SurfaceCounterFlagBitsEXT {
    static $gtype: GObject.GType<SurfaceCounterFlagBitsEXT>;

    constructor(copy: SurfaceCounterFlagBitsEXT);
}

export class SurfaceCounterFlagsEXT {
    static $gtype: GObject.GType<SurfaceCounterFlagsEXT>;

    constructor(copy: SurfaceCounterFlagsEXT);
}

export class SurfaceCapabilities2EXT {
    static $gtype: GObject.GType<SurfaceCapabilities2EXT>;

    constructor(copy: SurfaceCapabilities2EXT);
}

export class DisplayPowerStateEXT {
    static $gtype: GObject.GType<DisplayPowerStateEXT>;

    constructor(copy: DisplayPowerStateEXT);
}

export class DeviceEventTypeEXT {
    static $gtype: GObject.GType<DeviceEventTypeEXT>;

    constructor(copy: DeviceEventTypeEXT);
}

export class DisplayEventTypeEXT {
    static $gtype: GObject.GType<DisplayEventTypeEXT>;

    constructor(copy: DisplayEventTypeEXT);
}

export class DisplayPowerInfoEXT {
    static $gtype: GObject.GType<DisplayPowerInfoEXT>;

    constructor(copy: DisplayPowerInfoEXT);
}

export class DeviceEventInfoEXT {
    static $gtype: GObject.GType<DeviceEventInfoEXT>;

    constructor(copy: DeviceEventInfoEXT);
}

export class DisplayEventInfoEXT {
    static $gtype: GObject.GType<DisplayEventInfoEXT>;

    constructor(copy: DisplayEventInfoEXT);
}

export class SwapchainCounterCreateInfoEXT {
    static $gtype: GObject.GType<SwapchainCounterCreateInfoEXT>;

    constructor(copy: SwapchainCounterCreateInfoEXT);
}

export class RefreshCycleDurationGOOGLE {
    static $gtype: GObject.GType<RefreshCycleDurationGOOGLE>;

    constructor(copy: RefreshCycleDurationGOOGLE);
}

export class PastPresentationTimingGOOGLE {
    static $gtype: GObject.GType<PastPresentationTimingGOOGLE>;

    constructor(copy: PastPresentationTimingGOOGLE);
}

export class PresentTimeGOOGLE {
    static $gtype: GObject.GType<PresentTimeGOOGLE>;

    constructor(copy: PresentTimeGOOGLE);
}

export class PresentTimesInfoGOOGLE {
    static $gtype: GObject.GType<PresentTimesInfoGOOGLE>;

    constructor(copy: PresentTimesInfoGOOGLE);
}

export class PhysicalDeviceMultiviewPerViewAttributesPropertiesNVX {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewPerViewAttributesPropertiesNVX>;

    constructor(copy: PhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
}

export class ViewportCoordinateSwizzleNV {
    static $gtype: GObject.GType<ViewportCoordinateSwizzleNV>;

    constructor(copy: ViewportCoordinateSwizzleNV);
}

export class PipelineViewportSwizzleStateCreateFlagsNV {
    static $gtype: GObject.GType<PipelineViewportSwizzleStateCreateFlagsNV>;

    constructor(copy: PipelineViewportSwizzleStateCreateFlagsNV);
}

export class ViewportSwizzleNV {
    static $gtype: GObject.GType<ViewportSwizzleNV>;

    constructor(copy: ViewportSwizzleNV);
}

export class PipelineViewportSwizzleStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineViewportSwizzleStateCreateInfoNV>;

    constructor(copy: PipelineViewportSwizzleStateCreateInfoNV);
}

export class DiscardRectangleModeEXT {
    static $gtype: GObject.GType<DiscardRectangleModeEXT>;

    constructor(copy: DiscardRectangleModeEXT);
}

export class PipelineDiscardRectangleStateCreateFlagsEXT {
    static $gtype: GObject.GType<PipelineDiscardRectangleStateCreateFlagsEXT>;

    constructor(copy: PipelineDiscardRectangleStateCreateFlagsEXT);
}

export class PhysicalDeviceDiscardRectanglePropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDiscardRectanglePropertiesEXT>;

    constructor(copy: PhysicalDeviceDiscardRectanglePropertiesEXT);
}

export class PipelineDiscardRectangleStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineDiscardRectangleStateCreateInfoEXT>;

    constructor(copy: PipelineDiscardRectangleStateCreateInfoEXT);
}

export class ConservativeRasterizationModeEXT {
    static $gtype: GObject.GType<ConservativeRasterizationModeEXT>;

    constructor(copy: ConservativeRasterizationModeEXT);
}

export class PipelineRasterizationConservativeStateCreateFlagsEXT {
    static $gtype: GObject.GType<PipelineRasterizationConservativeStateCreateFlagsEXT>;

    constructor(copy: PipelineRasterizationConservativeStateCreateFlagsEXT);
}

export class PhysicalDeviceConservativeRasterizationPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceConservativeRasterizationPropertiesEXT>;

    constructor(copy: PhysicalDeviceConservativeRasterizationPropertiesEXT);
}

export class PipelineRasterizationConservativeStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineRasterizationConservativeStateCreateInfoEXT>;

    constructor(copy: PipelineRasterizationConservativeStateCreateInfoEXT);
}

export class PipelineRasterizationDepthClipStateCreateFlagsEXT {
    static $gtype: GObject.GType<PipelineRasterizationDepthClipStateCreateFlagsEXT>;

    constructor(copy: PipelineRasterizationDepthClipStateCreateFlagsEXT);
}

export class PhysicalDeviceDepthClipEnableFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDepthClipEnableFeaturesEXT>;

    constructor(copy: PhysicalDeviceDepthClipEnableFeaturesEXT);
}

export class PipelineRasterizationDepthClipStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineRasterizationDepthClipStateCreateInfoEXT>;

    constructor(copy: PipelineRasterizationDepthClipStateCreateInfoEXT);
}

export class XYColorEXT {
    static $gtype: GObject.GType<XYColorEXT>;

    constructor(copy: XYColorEXT);
}

export class HdrMetadataEXT {
    static $gtype: GObject.GType<HdrMetadataEXT>;

    constructor(copy: HdrMetadataEXT);
}

export class PhysicalDeviceRelaxedLineRasterizationFeaturesIMG {
    static $gtype: GObject.GType<PhysicalDeviceRelaxedLineRasterizationFeaturesIMG>;

    constructor(copy: PhysicalDeviceRelaxedLineRasterizationFeaturesIMG);
}

export class DebugUtilsMessengerEXT {
    static $gtype: GObject.GType<DebugUtilsMessengerEXT>;

    constructor(copy: DebugUtilsMessengerEXT);
}

export class DebugUtilsMessengerCallbackDataFlagsEXT {
    static $gtype: GObject.GType<DebugUtilsMessengerCallbackDataFlagsEXT>;

    constructor(copy: DebugUtilsMessengerCallbackDataFlagsEXT);
}

export class DebugUtilsMessageSeverityFlagBitsEXT {
    static $gtype: GObject.GType<DebugUtilsMessageSeverityFlagBitsEXT>;

    constructor(copy: DebugUtilsMessageSeverityFlagBitsEXT);
}

export class DebugUtilsMessageTypeFlagBitsEXT {
    static $gtype: GObject.GType<DebugUtilsMessageTypeFlagBitsEXT>;

    constructor(copy: DebugUtilsMessageTypeFlagBitsEXT);
}

export class DebugUtilsMessageTypeFlagsEXT {
    static $gtype: GObject.GType<DebugUtilsMessageTypeFlagsEXT>;

    constructor(copy: DebugUtilsMessageTypeFlagsEXT);
}

export class DebugUtilsMessageSeverityFlagsEXT {
    static $gtype: GObject.GType<DebugUtilsMessageSeverityFlagsEXT>;

    constructor(copy: DebugUtilsMessageSeverityFlagsEXT);
}

export class DebugUtilsMessengerCreateFlagsEXT {
    static $gtype: GObject.GType<DebugUtilsMessengerCreateFlagsEXT>;

    constructor(copy: DebugUtilsMessengerCreateFlagsEXT);
}

export class DebugUtilsLabelEXT {
    static $gtype: GObject.GType<DebugUtilsLabelEXT>;

    constructor(copy: DebugUtilsLabelEXT);
}

export class DebugUtilsObjectNameInfoEXT {
    static $gtype: GObject.GType<DebugUtilsObjectNameInfoEXT>;

    constructor(copy: DebugUtilsObjectNameInfoEXT);
}

export class DebugUtilsMessengerCallbackDataEXT {
    static $gtype: GObject.GType<DebugUtilsMessengerCallbackDataEXT>;

    constructor(copy: DebugUtilsMessengerCallbackDataEXT);
}

export class DebugUtilsMessengerCreateInfoEXT {
    static $gtype: GObject.GType<DebugUtilsMessengerCreateInfoEXT>;

    constructor(copy: DebugUtilsMessengerCreateInfoEXT);
}

export class DebugUtilsObjectTagInfoEXT {
    static $gtype: GObject.GType<DebugUtilsObjectTagInfoEXT>;

    constructor(copy: DebugUtilsObjectTagInfoEXT);
}

export class SamplerReductionModeEXT {
    static $gtype: GObject.GType<SamplerReductionModeEXT>;

    constructor(copy: SamplerReductionModeEXT);
}

export class SamplerReductionModeCreateInfoEXT {
    static $gtype: GObject.GType<SamplerReductionModeCreateInfoEXT>;

    constructor(copy: SamplerReductionModeCreateInfoEXT);
}

export class PhysicalDeviceSamplerFilterMinmaxPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceSamplerFilterMinmaxPropertiesEXT>;

    constructor(copy: PhysicalDeviceSamplerFilterMinmaxPropertiesEXT);
}

export class PhysicalDeviceInlineUniformBlockFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceInlineUniformBlockFeaturesEXT>;

    constructor(copy: PhysicalDeviceInlineUniformBlockFeaturesEXT);
}

export class PhysicalDeviceInlineUniformBlockPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceInlineUniformBlockPropertiesEXT>;

    constructor(copy: PhysicalDeviceInlineUniformBlockPropertiesEXT);
}

export class WriteDescriptorSetInlineUniformBlockEXT {
    static $gtype: GObject.GType<WriteDescriptorSetInlineUniformBlockEXT>;

    constructor(copy: WriteDescriptorSetInlineUniformBlockEXT);
}

export class DescriptorPoolInlineUniformBlockCreateInfoEXT {
    static $gtype: GObject.GType<DescriptorPoolInlineUniformBlockCreateInfoEXT>;

    constructor(copy: DescriptorPoolInlineUniformBlockCreateInfoEXT);
}

export class SampleLocationEXT {
    static $gtype: GObject.GType<SampleLocationEXT>;

    constructor(copy: SampleLocationEXT);
}

export class SampleLocationsInfoEXT {
    static $gtype: GObject.GType<SampleLocationsInfoEXT>;

    constructor(copy: SampleLocationsInfoEXT);
}

export class AttachmentSampleLocationsEXT {
    static $gtype: GObject.GType<AttachmentSampleLocationsEXT>;

    constructor(copy: AttachmentSampleLocationsEXT);
}

export class SubpassSampleLocationsEXT {
    static $gtype: GObject.GType<SubpassSampleLocationsEXT>;

    constructor(copy: SubpassSampleLocationsEXT);
}

export class RenderPassSampleLocationsBeginInfoEXT {
    static $gtype: GObject.GType<RenderPassSampleLocationsBeginInfoEXT>;

    constructor(copy: RenderPassSampleLocationsBeginInfoEXT);
}

export class PipelineSampleLocationsStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineSampleLocationsStateCreateInfoEXT>;

    constructor(copy: PipelineSampleLocationsStateCreateInfoEXT);
}

export class PhysicalDeviceSampleLocationsPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceSampleLocationsPropertiesEXT>;

    constructor(copy: PhysicalDeviceSampleLocationsPropertiesEXT);
}

export class MultisamplePropertiesEXT {
    static $gtype: GObject.GType<MultisamplePropertiesEXT>;

    constructor(copy: MultisamplePropertiesEXT);
}

export class BlendOverlapEXT {
    static $gtype: GObject.GType<BlendOverlapEXT>;

    constructor(copy: BlendOverlapEXT);
}

export class PhysicalDeviceBlendOperationAdvancedFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceBlendOperationAdvancedFeaturesEXT>;

    constructor(copy: PhysicalDeviceBlendOperationAdvancedFeaturesEXT);
}

export class PhysicalDeviceBlendOperationAdvancedPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceBlendOperationAdvancedPropertiesEXT>;

    constructor(copy: PhysicalDeviceBlendOperationAdvancedPropertiesEXT);
}

export class PipelineColorBlendAdvancedStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineColorBlendAdvancedStateCreateInfoEXT>;

    constructor(copy: PipelineColorBlendAdvancedStateCreateInfoEXT);
}

export class PipelineCoverageToColorStateCreateFlagsNV {
    static $gtype: GObject.GType<PipelineCoverageToColorStateCreateFlagsNV>;

    constructor(copy: PipelineCoverageToColorStateCreateFlagsNV);
}

export class PipelineCoverageToColorStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineCoverageToColorStateCreateInfoNV>;

    constructor(copy: PipelineCoverageToColorStateCreateInfoNV);
}

export class CoverageModulationModeNV {
    static $gtype: GObject.GType<CoverageModulationModeNV>;

    constructor(copy: CoverageModulationModeNV);
}

export class PipelineCoverageModulationStateCreateFlagsNV {
    static $gtype: GObject.GType<PipelineCoverageModulationStateCreateFlagsNV>;

    constructor(copy: PipelineCoverageModulationStateCreateFlagsNV);
}

export class PipelineCoverageModulationStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineCoverageModulationStateCreateInfoNV>;

    constructor(copy: PipelineCoverageModulationStateCreateInfoNV);
}

export class PhysicalDeviceShaderSMBuiltinsPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceShaderSMBuiltinsPropertiesNV>;

    constructor(copy: PhysicalDeviceShaderSMBuiltinsPropertiesNV);
}

export class PhysicalDeviceShaderSMBuiltinsFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceShaderSMBuiltinsFeaturesNV>;

    constructor(copy: PhysicalDeviceShaderSMBuiltinsFeaturesNV);
}

export class DrmFormatModifierPropertiesEXT {
    static $gtype: GObject.GType<DrmFormatModifierPropertiesEXT>;

    constructor(copy: DrmFormatModifierPropertiesEXT);
}

export class DrmFormatModifierPropertiesListEXT {
    static $gtype: GObject.GType<DrmFormatModifierPropertiesListEXT>;

    constructor(copy: DrmFormatModifierPropertiesListEXT);
}

export class PhysicalDeviceImageDrmFormatModifierInfoEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageDrmFormatModifierInfoEXT>;

    constructor(copy: PhysicalDeviceImageDrmFormatModifierInfoEXT);
}

export class ImageDrmFormatModifierListCreateInfoEXT {
    static $gtype: GObject.GType<ImageDrmFormatModifierListCreateInfoEXT>;

    constructor(copy: ImageDrmFormatModifierListCreateInfoEXT);
}

export class ImageDrmFormatModifierExplicitCreateInfoEXT {
    static $gtype: GObject.GType<ImageDrmFormatModifierExplicitCreateInfoEXT>;

    constructor(copy: ImageDrmFormatModifierExplicitCreateInfoEXT);
}

export class ImageDrmFormatModifierPropertiesEXT {
    static $gtype: GObject.GType<ImageDrmFormatModifierPropertiesEXT>;

    constructor(copy: ImageDrmFormatModifierPropertiesEXT);
}

export class DrmFormatModifierProperties2EXT {
    static $gtype: GObject.GType<DrmFormatModifierProperties2EXT>;

    constructor(copy: DrmFormatModifierProperties2EXT);
}

export class DrmFormatModifierPropertiesList2EXT {
    static $gtype: GObject.GType<DrmFormatModifierPropertiesList2EXT>;

    constructor(copy: DrmFormatModifierPropertiesList2EXT);
}

export class ValidationCacheEXT {
    static $gtype: GObject.GType<ValidationCacheEXT>;

    constructor(copy: ValidationCacheEXT);
}

export class ValidationCacheHeaderVersionEXT {
    static $gtype: GObject.GType<ValidationCacheHeaderVersionEXT>;

    constructor(copy: ValidationCacheHeaderVersionEXT);
}

export class ValidationCacheCreateFlagsEXT {
    static $gtype: GObject.GType<ValidationCacheCreateFlagsEXT>;

    constructor(copy: ValidationCacheCreateFlagsEXT);
}

export class ValidationCacheCreateInfoEXT {
    static $gtype: GObject.GType<ValidationCacheCreateInfoEXT>;

    constructor(copy: ValidationCacheCreateInfoEXT);
}

export class ShaderModuleValidationCacheCreateInfoEXT {
    static $gtype: GObject.GType<ShaderModuleValidationCacheCreateInfoEXT>;

    constructor(copy: ShaderModuleValidationCacheCreateInfoEXT);
}

export class DescriptorBindingFlagBitsEXT {
    static $gtype: GObject.GType<DescriptorBindingFlagBitsEXT>;

    constructor(copy: DescriptorBindingFlagBitsEXT);
}

export class DescriptorBindingFlagsEXT {
    static $gtype: GObject.GType<DescriptorBindingFlagsEXT>;

    constructor(copy: DescriptorBindingFlagsEXT);
}

export class DescriptorSetLayoutBindingFlagsCreateInfoEXT {
    static $gtype: GObject.GType<DescriptorSetLayoutBindingFlagsCreateInfoEXT>;

    constructor(copy: DescriptorSetLayoutBindingFlagsCreateInfoEXT);
}

export class PhysicalDeviceDescriptorIndexingFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorIndexingFeaturesEXT>;

    constructor(copy: PhysicalDeviceDescriptorIndexingFeaturesEXT);
}

export class PhysicalDeviceDescriptorIndexingPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorIndexingPropertiesEXT>;

    constructor(copy: PhysicalDeviceDescriptorIndexingPropertiesEXT);
}

export class DescriptorSetVariableDescriptorCountAllocateInfoEXT {
    static $gtype: GObject.GType<DescriptorSetVariableDescriptorCountAllocateInfoEXT>;

    constructor(copy: DescriptorSetVariableDescriptorCountAllocateInfoEXT);
}

export class DescriptorSetVariableDescriptorCountLayoutSupportEXT {
    static $gtype: GObject.GType<DescriptorSetVariableDescriptorCountLayoutSupportEXT>;

    constructor(copy: DescriptorSetVariableDescriptorCountLayoutSupportEXT);
}

export class ShadingRatePaletteEntryNV {
    static $gtype: GObject.GType<ShadingRatePaletteEntryNV>;

    constructor(copy: ShadingRatePaletteEntryNV);
}

export class CoarseSampleOrderTypeNV {
    static $gtype: GObject.GType<CoarseSampleOrderTypeNV>;

    constructor(copy: CoarseSampleOrderTypeNV);
}

export class ShadingRatePaletteNV {
    static $gtype: GObject.GType<ShadingRatePaletteNV>;

    constructor(copy: ShadingRatePaletteNV);
}

export class PipelineViewportShadingRateImageStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineViewportShadingRateImageStateCreateInfoNV>;

    constructor(copy: PipelineViewportShadingRateImageStateCreateInfoNV);
}

export class PhysicalDeviceShadingRateImageFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceShadingRateImageFeaturesNV>;

    constructor(copy: PhysicalDeviceShadingRateImageFeaturesNV);
}

export class PhysicalDeviceShadingRateImagePropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceShadingRateImagePropertiesNV>;

    constructor(copy: PhysicalDeviceShadingRateImagePropertiesNV);
}

export class CoarseSampleLocationNV {
    static $gtype: GObject.GType<CoarseSampleLocationNV>;

    constructor(copy: CoarseSampleLocationNV);
}

export class CoarseSampleOrderCustomNV {
    static $gtype: GObject.GType<CoarseSampleOrderCustomNV>;

    constructor(copy: CoarseSampleOrderCustomNV);
}

export class PipelineViewportCoarseSampleOrderStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineViewportCoarseSampleOrderStateCreateInfoNV>;

    constructor(copy: PipelineViewportCoarseSampleOrderStateCreateInfoNV);
}

export class AccelerationStructureNV {
    static $gtype: GObject.GType<AccelerationStructureNV>;

    constructor(copy: AccelerationStructureNV);
}

export class RayTracingShaderGroupTypeKHR {
    static $gtype: GObject.GType<RayTracingShaderGroupTypeKHR>;

    constructor(copy: RayTracingShaderGroupTypeKHR);
}

export class RayTracingShaderGroupTypeNV {
    static $gtype: GObject.GType<RayTracingShaderGroupTypeNV>;

    constructor(copy: RayTracingShaderGroupTypeNV);
}

export class GeometryTypeKHR {
    static $gtype: GObject.GType<GeometryTypeKHR>;

    constructor(copy: GeometryTypeKHR);
}

export class GeometryTypeNV {
    static $gtype: GObject.GType<GeometryTypeNV>;

    constructor(copy: GeometryTypeNV);
}

export class AccelerationStructureTypeKHR {
    static $gtype: GObject.GType<AccelerationStructureTypeKHR>;

    constructor(copy: AccelerationStructureTypeKHR);
}

export class AccelerationStructureTypeNV {
    static $gtype: GObject.GType<AccelerationStructureTypeNV>;

    constructor(copy: AccelerationStructureTypeNV);
}

export class CopyAccelerationStructureModeKHR {
    static $gtype: GObject.GType<CopyAccelerationStructureModeKHR>;

    constructor(copy: CopyAccelerationStructureModeKHR);
}

export class CopyAccelerationStructureModeNV {
    static $gtype: GObject.GType<CopyAccelerationStructureModeNV>;

    constructor(copy: CopyAccelerationStructureModeNV);
}

export class AccelerationStructureMemoryRequirementsTypeNV {
    static $gtype: GObject.GType<AccelerationStructureMemoryRequirementsTypeNV>;

    constructor(copy: AccelerationStructureMemoryRequirementsTypeNV);
}

export class GeometryFlagBitsKHR {
    static $gtype: GObject.GType<GeometryFlagBitsKHR>;

    constructor(copy: GeometryFlagBitsKHR);
}

export class GeometryFlagsKHR {
    static $gtype: GObject.GType<GeometryFlagsKHR>;

    constructor(copy: GeometryFlagsKHR);
}

export class GeometryFlagsNV {
    static $gtype: GObject.GType<GeometryFlagsNV>;

    constructor(copy: GeometryFlagsNV);
}

export class GeometryFlagBitsNV {
    static $gtype: GObject.GType<GeometryFlagBitsNV>;

    constructor(copy: GeometryFlagBitsNV);
}

export class GeometryInstanceFlagBitsKHR {
    static $gtype: GObject.GType<GeometryInstanceFlagBitsKHR>;

    constructor(copy: GeometryInstanceFlagBitsKHR);
}

export class GeometryInstanceFlagsKHR {
    static $gtype: GObject.GType<GeometryInstanceFlagsKHR>;

    constructor(copy: GeometryInstanceFlagsKHR);
}

export class GeometryInstanceFlagsNV {
    static $gtype: GObject.GType<GeometryInstanceFlagsNV>;

    constructor(copy: GeometryInstanceFlagsNV);
}

export class GeometryInstanceFlagBitsNV {
    static $gtype: GObject.GType<GeometryInstanceFlagBitsNV>;

    constructor(copy: GeometryInstanceFlagBitsNV);
}

export class BuildAccelerationStructureFlagBitsKHR {
    static $gtype: GObject.GType<BuildAccelerationStructureFlagBitsKHR>;

    constructor(copy: BuildAccelerationStructureFlagBitsKHR);
}

export class BuildAccelerationStructureFlagsKHR {
    static $gtype: GObject.GType<BuildAccelerationStructureFlagsKHR>;

    constructor(copy: BuildAccelerationStructureFlagsKHR);
}

export class BuildAccelerationStructureFlagsNV {
    static $gtype: GObject.GType<BuildAccelerationStructureFlagsNV>;

    constructor(copy: BuildAccelerationStructureFlagsNV);
}

export class BuildAccelerationStructureFlagBitsNV {
    static $gtype: GObject.GType<BuildAccelerationStructureFlagBitsNV>;

    constructor(copy: BuildAccelerationStructureFlagBitsNV);
}

export class RayTracingShaderGroupCreateInfoNV {
    static $gtype: GObject.GType<RayTracingShaderGroupCreateInfoNV>;

    constructor(copy: RayTracingShaderGroupCreateInfoNV);
}

export class RayTracingPipelineCreateInfoNV {
    static $gtype: GObject.GType<RayTracingPipelineCreateInfoNV>;

    constructor(copy: RayTracingPipelineCreateInfoNV);
}

export class GeometryTrianglesNV {
    static $gtype: GObject.GType<GeometryTrianglesNV>;

    constructor(copy: GeometryTrianglesNV);
}

export class GeometryAABBNV {
    static $gtype: GObject.GType<GeometryAABBNV>;

    constructor(copy: GeometryAABBNV);
}

export class GeometryDataNV {
    static $gtype: GObject.GType<GeometryDataNV>;

    constructor(copy: GeometryDataNV);
}

export class GeometryNV {
    static $gtype: GObject.GType<GeometryNV>;

    constructor(copy: GeometryNV);
}

export class AccelerationStructureInfoNV {
    static $gtype: GObject.GType<AccelerationStructureInfoNV>;

    constructor(copy: AccelerationStructureInfoNV);
}

export class AccelerationStructureCreateInfoNV {
    static $gtype: GObject.GType<AccelerationStructureCreateInfoNV>;

    constructor(copy: AccelerationStructureCreateInfoNV);
}

export class BindAccelerationStructureMemoryInfoNV {
    static $gtype: GObject.GType<BindAccelerationStructureMemoryInfoNV>;

    constructor(copy: BindAccelerationStructureMemoryInfoNV);
}

export class WriteDescriptorSetAccelerationStructureNV {
    static $gtype: GObject.GType<WriteDescriptorSetAccelerationStructureNV>;

    constructor(copy: WriteDescriptorSetAccelerationStructureNV);
}

export class AccelerationStructureMemoryRequirementsInfoNV {
    static $gtype: GObject.GType<AccelerationStructureMemoryRequirementsInfoNV>;

    constructor(copy: AccelerationStructureMemoryRequirementsInfoNV);
}

export class PhysicalDeviceRayTracingPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingPropertiesNV>;

    constructor(copy: PhysicalDeviceRayTracingPropertiesNV);
}

export class TransformMatrixKHR {
    static $gtype: GObject.GType<TransformMatrixKHR>;

    constructor(copy: TransformMatrixKHR);
}

export class TransformMatrixNV {
    static $gtype: GObject.GType<TransformMatrixNV>;

    constructor(copy: TransformMatrixNV);
}

export class AabbPositionsKHR {
    static $gtype: GObject.GType<AabbPositionsKHR>;

    constructor(copy: AabbPositionsKHR);
}

export class AabbPositionsNV {
    static $gtype: GObject.GType<AabbPositionsNV>;

    constructor(copy: AabbPositionsNV);
}

export class AccelerationStructureInstanceKHR {
    static $gtype: GObject.GType<AccelerationStructureInstanceKHR>;

    constructor(copy: AccelerationStructureInstanceKHR);
}

export class AccelerationStructureInstanceNV {
    static $gtype: GObject.GType<AccelerationStructureInstanceNV>;

    constructor(copy: AccelerationStructureInstanceNV);
}

export class PhysicalDeviceRepresentativeFragmentTestFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceRepresentativeFragmentTestFeaturesNV>;

    constructor(copy: PhysicalDeviceRepresentativeFragmentTestFeaturesNV);
}

export class PipelineRepresentativeFragmentTestStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineRepresentativeFragmentTestStateCreateInfoNV>;

    constructor(copy: PipelineRepresentativeFragmentTestStateCreateInfoNV);
}

export class PhysicalDeviceImageViewImageFormatInfoEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageViewImageFormatInfoEXT>;

    constructor(copy: PhysicalDeviceImageViewImageFormatInfoEXT);
}

export class FilterCubicImageViewImageFormatPropertiesEXT {
    static $gtype: GObject.GType<FilterCubicImageViewImageFormatPropertiesEXT>;

    constructor(copy: FilterCubicImageViewImageFormatPropertiesEXT);
}

export class QueueGlobalPriorityEXT {
    static $gtype: GObject.GType<QueueGlobalPriorityEXT>;

    constructor(copy: QueueGlobalPriorityEXT);
}

export class DeviceQueueGlobalPriorityCreateInfoEXT {
    static $gtype: GObject.GType<DeviceQueueGlobalPriorityCreateInfoEXT>;

    constructor(copy: DeviceQueueGlobalPriorityCreateInfoEXT);
}

export class ImportMemoryHostPointerInfoEXT {
    static $gtype: GObject.GType<ImportMemoryHostPointerInfoEXT>;

    constructor(copy: ImportMemoryHostPointerInfoEXT);
}

export class MemoryHostPointerPropertiesEXT {
    static $gtype: GObject.GType<MemoryHostPointerPropertiesEXT>;

    constructor(copy: MemoryHostPointerPropertiesEXT);
}

export class PhysicalDeviceExternalMemoryHostPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceExternalMemoryHostPropertiesEXT>;

    constructor(copy: PhysicalDeviceExternalMemoryHostPropertiesEXT);
}

export class PipelineCompilerControlFlagBitsAMD {
    static $gtype: GObject.GType<PipelineCompilerControlFlagBitsAMD>;

    constructor(copy: PipelineCompilerControlFlagBitsAMD);
}

export class PipelineCompilerControlFlagsAMD {
    static $gtype: GObject.GType<PipelineCompilerControlFlagsAMD>;

    constructor(copy: PipelineCompilerControlFlagsAMD);
}

export class PipelineCompilerControlCreateInfoAMD {
    static $gtype: GObject.GType<PipelineCompilerControlCreateInfoAMD>;

    constructor(copy: PipelineCompilerControlCreateInfoAMD);
}

export class TimeDomainEXT {
    static $gtype: GObject.GType<TimeDomainEXT>;

    constructor(copy: TimeDomainEXT);
}

export class CalibratedTimestampInfoEXT {
    static $gtype: GObject.GType<CalibratedTimestampInfoEXT>;

    constructor(copy: CalibratedTimestampInfoEXT);
}

export class PhysicalDeviceShaderCorePropertiesAMD {
    static $gtype: GObject.GType<PhysicalDeviceShaderCorePropertiesAMD>;

    constructor(copy: PhysicalDeviceShaderCorePropertiesAMD);
}

export class MemoryOverallocationBehaviorAMD {
    static $gtype: GObject.GType<MemoryOverallocationBehaviorAMD>;

    constructor(copy: MemoryOverallocationBehaviorAMD);
}

export class DeviceMemoryOverallocationCreateInfoAMD {
    static $gtype: GObject.GType<DeviceMemoryOverallocationCreateInfoAMD>;

    constructor(copy: DeviceMemoryOverallocationCreateInfoAMD);
}

export class PhysicalDeviceVertexAttributeDivisorPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceVertexAttributeDivisorPropertiesEXT>;

    constructor(copy: PhysicalDeviceVertexAttributeDivisorPropertiesEXT);
}

export class VertexInputBindingDivisorDescriptionEXT {
    static $gtype: GObject.GType<VertexInputBindingDivisorDescriptionEXT>;

    constructor(copy: VertexInputBindingDivisorDescriptionEXT);
}

export class PipelineVertexInputDivisorStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineVertexInputDivisorStateCreateInfoEXT>;

    constructor(copy: PipelineVertexInputDivisorStateCreateInfoEXT);
}

export class PhysicalDeviceVertexAttributeDivisorFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceVertexAttributeDivisorFeaturesEXT>;

    constructor(copy: PhysicalDeviceVertexAttributeDivisorFeaturesEXT);
}

export class PipelineCreationFeedbackFlagBitsEXT {
    static $gtype: GObject.GType<PipelineCreationFeedbackFlagBitsEXT>;

    constructor(copy: PipelineCreationFeedbackFlagBitsEXT);
}

export class PipelineCreationFeedbackFlagsEXT {
    static $gtype: GObject.GType<PipelineCreationFeedbackFlagsEXT>;

    constructor(copy: PipelineCreationFeedbackFlagsEXT);
}

export class PipelineCreationFeedbackCreateInfoEXT {
    static $gtype: GObject.GType<PipelineCreationFeedbackCreateInfoEXT>;

    constructor(copy: PipelineCreationFeedbackCreateInfoEXT);
}

export class PipelineCreationFeedbackEXT {
    static $gtype: GObject.GType<PipelineCreationFeedbackEXT>;

    constructor(copy: PipelineCreationFeedbackEXT);
}

export class PhysicalDeviceComputeShaderDerivativesFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceComputeShaderDerivativesFeaturesNV>;

    constructor(copy: PhysicalDeviceComputeShaderDerivativesFeaturesNV);
}

export class PhysicalDeviceMeshShaderFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceMeshShaderFeaturesNV>;

    constructor(copy: PhysicalDeviceMeshShaderFeaturesNV);
}

export class PhysicalDeviceMeshShaderPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceMeshShaderPropertiesNV>;

    constructor(copy: PhysicalDeviceMeshShaderPropertiesNV);
}

export class DrawMeshTasksIndirectCommandNV {
    static $gtype: GObject.GType<DrawMeshTasksIndirectCommandNV>;

    constructor(copy: DrawMeshTasksIndirectCommandNV);
}

export class PhysicalDeviceFragmentShaderBarycentricFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShaderBarycentricFeaturesNV>;

    constructor(copy: PhysicalDeviceFragmentShaderBarycentricFeaturesNV);
}

export class PhysicalDeviceShaderImageFootprintFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceShaderImageFootprintFeaturesNV>;

    constructor(copy: PhysicalDeviceShaderImageFootprintFeaturesNV);
}

export class PipelineViewportExclusiveScissorStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineViewportExclusiveScissorStateCreateInfoNV>;

    constructor(copy: PipelineViewportExclusiveScissorStateCreateInfoNV);
}

export class PhysicalDeviceExclusiveScissorFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceExclusiveScissorFeaturesNV>;

    constructor(copy: PhysicalDeviceExclusiveScissorFeaturesNV);
}

export class QueueFamilyCheckpointPropertiesNV {
    static $gtype: GObject.GType<QueueFamilyCheckpointPropertiesNV>;

    constructor(copy: QueueFamilyCheckpointPropertiesNV);
}

export class CheckpointDataNV {
    static $gtype: GObject.GType<CheckpointDataNV>;

    constructor(copy: CheckpointDataNV);
}

export class PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL {
    static $gtype: GObject.GType<PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL>;

    constructor(copy: PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
}

export class PerformanceConfigurationINTEL {
    static $gtype: GObject.GType<PerformanceConfigurationINTEL>;

    constructor(copy: PerformanceConfigurationINTEL);
}

export class PerformanceConfigurationTypeINTEL {
    static $gtype: GObject.GType<PerformanceConfigurationTypeINTEL>;

    constructor(copy: PerformanceConfigurationTypeINTEL);
}

export class QueryPoolSamplingModeINTEL {
    static $gtype: GObject.GType<QueryPoolSamplingModeINTEL>;

    constructor(copy: QueryPoolSamplingModeINTEL);
}

export class PerformanceOverrideTypeINTEL {
    static $gtype: GObject.GType<PerformanceOverrideTypeINTEL>;

    constructor(copy: PerformanceOverrideTypeINTEL);
}

export class PerformanceParameterTypeINTEL {
    static $gtype: GObject.GType<PerformanceParameterTypeINTEL>;

    constructor(copy: PerformanceParameterTypeINTEL);
}

export class PerformanceValueTypeINTEL {
    static $gtype: GObject.GType<PerformanceValueTypeINTEL>;

    constructor(copy: PerformanceValueTypeINTEL);
}

export class PerformanceValueDataINTEL {
    static $gtype: GObject.GType<PerformanceValueDataINTEL>;

    constructor(copy: PerformanceValueDataINTEL);
}

export class PerformanceValueINTEL {
    static $gtype: GObject.GType<PerformanceValueINTEL>;

    constructor(copy: PerformanceValueINTEL);
}

export class InitializePerformanceApiInfoINTEL {
    static $gtype: GObject.GType<InitializePerformanceApiInfoINTEL>;

    constructor(copy: InitializePerformanceApiInfoINTEL);
}

export class QueryPoolPerformanceQueryCreateInfoINTEL {
    static $gtype: GObject.GType<QueryPoolPerformanceQueryCreateInfoINTEL>;

    constructor(copy: QueryPoolPerformanceQueryCreateInfoINTEL);
}

export class QueryPoolCreateInfoINTEL {
    static $gtype: GObject.GType<QueryPoolCreateInfoINTEL>;

    constructor(copy: QueryPoolCreateInfoINTEL);
}

export class PerformanceMarkerInfoINTEL {
    static $gtype: GObject.GType<PerformanceMarkerInfoINTEL>;

    constructor(copy: PerformanceMarkerInfoINTEL);
}

export class PerformanceStreamMarkerInfoINTEL {
    static $gtype: GObject.GType<PerformanceStreamMarkerInfoINTEL>;

    constructor(copy: PerformanceStreamMarkerInfoINTEL);
}

export class PerformanceOverrideInfoINTEL {
    static $gtype: GObject.GType<PerformanceOverrideInfoINTEL>;

    constructor(copy: PerformanceOverrideInfoINTEL);
}

export class PerformanceConfigurationAcquireInfoINTEL {
    static $gtype: GObject.GType<PerformanceConfigurationAcquireInfoINTEL>;

    constructor(copy: PerformanceConfigurationAcquireInfoINTEL);
}

export class PhysicalDevicePCIBusInfoPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDevicePCIBusInfoPropertiesEXT>;

    constructor(copy: PhysicalDevicePCIBusInfoPropertiesEXT);
}

export class DisplayNativeHdrSurfaceCapabilitiesAMD {
    static $gtype: GObject.GType<DisplayNativeHdrSurfaceCapabilitiesAMD>;

    constructor(copy: DisplayNativeHdrSurfaceCapabilitiesAMD);
}

export class SwapchainDisplayNativeHdrCreateInfoAMD {
    static $gtype: GObject.GType<SwapchainDisplayNativeHdrCreateInfoAMD>;

    constructor(copy: SwapchainDisplayNativeHdrCreateInfoAMD);
}

export class PhysicalDeviceFragmentDensityMapFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFragmentDensityMapFeaturesEXT>;

    constructor(copy: PhysicalDeviceFragmentDensityMapFeaturesEXT);
}

export class PhysicalDeviceFragmentDensityMapPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFragmentDensityMapPropertiesEXT>;

    constructor(copy: PhysicalDeviceFragmentDensityMapPropertiesEXT);
}

export class RenderPassFragmentDensityMapCreateInfoEXT {
    static $gtype: GObject.GType<RenderPassFragmentDensityMapCreateInfoEXT>;

    constructor(copy: RenderPassFragmentDensityMapCreateInfoEXT);
}

export class PhysicalDeviceScalarBlockLayoutFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceScalarBlockLayoutFeaturesEXT>;

    constructor(copy: PhysicalDeviceScalarBlockLayoutFeaturesEXT);
}

export class PhysicalDeviceSubgroupSizeControlFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceSubgroupSizeControlFeaturesEXT>;

    constructor(copy: PhysicalDeviceSubgroupSizeControlFeaturesEXT);
}

export class PhysicalDeviceSubgroupSizeControlPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceSubgroupSizeControlPropertiesEXT>;

    constructor(copy: PhysicalDeviceSubgroupSizeControlPropertiesEXT);
}

export class PipelineShaderStageRequiredSubgroupSizeCreateInfoEXT {
    static $gtype: GObject.GType<PipelineShaderStageRequiredSubgroupSizeCreateInfoEXT>;

    constructor(copy: PipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
}

export class ShaderCorePropertiesFlagBitsAMD {
    static $gtype: GObject.GType<ShaderCorePropertiesFlagBitsAMD>;

    constructor(copy: ShaderCorePropertiesFlagBitsAMD);
}

export class ShaderCorePropertiesFlagsAMD {
    static $gtype: GObject.GType<ShaderCorePropertiesFlagsAMD>;

    constructor(copy: ShaderCorePropertiesFlagsAMD);
}

export class PhysicalDeviceShaderCoreProperties2AMD {
    static $gtype: GObject.GType<PhysicalDeviceShaderCoreProperties2AMD>;

    constructor(copy: PhysicalDeviceShaderCoreProperties2AMD);
}

export class PhysicalDeviceCoherentMemoryFeaturesAMD {
    static $gtype: GObject.GType<PhysicalDeviceCoherentMemoryFeaturesAMD>;

    constructor(copy: PhysicalDeviceCoherentMemoryFeaturesAMD);
}

export class PhysicalDeviceShaderImageAtomicInt64FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderImageAtomicInt64FeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderImageAtomicInt64FeaturesEXT);
}

export class PhysicalDeviceMemoryBudgetPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMemoryBudgetPropertiesEXT>;

    constructor(copy: PhysicalDeviceMemoryBudgetPropertiesEXT);
}

export class PhysicalDeviceMemoryPriorityFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMemoryPriorityFeaturesEXT>;

    constructor(copy: PhysicalDeviceMemoryPriorityFeaturesEXT);
}

export class MemoryPriorityAllocateInfoEXT {
    static $gtype: GObject.GType<MemoryPriorityAllocateInfoEXT>;

    constructor(copy: MemoryPriorityAllocateInfoEXT);
}

export class PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV>;

    constructor(copy: PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
}

export class PhysicalDeviceBufferDeviceAddressFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceBufferDeviceAddressFeaturesEXT>;

    constructor(copy: PhysicalDeviceBufferDeviceAddressFeaturesEXT);
}

export class PhysicalDeviceBufferAddressFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceBufferAddressFeaturesEXT>;

    constructor(copy: PhysicalDeviceBufferAddressFeaturesEXT);
}

export class BufferDeviceAddressInfoEXT {
    static $gtype: GObject.GType<BufferDeviceAddressInfoEXT>;

    constructor(copy: BufferDeviceAddressInfoEXT);
}

export class BufferDeviceAddressCreateInfoEXT {
    static $gtype: GObject.GType<BufferDeviceAddressCreateInfoEXT>;

    constructor(copy: BufferDeviceAddressCreateInfoEXT);
}

export class ToolPurposeFlagBitsEXT {
    static $gtype: GObject.GType<ToolPurposeFlagBitsEXT>;

    constructor(copy: ToolPurposeFlagBitsEXT);
}

export class ToolPurposeFlagsEXT {
    static $gtype: GObject.GType<ToolPurposeFlagsEXT>;

    constructor(copy: ToolPurposeFlagsEXT);
}

export class PhysicalDeviceToolPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceToolPropertiesEXT>;

    constructor(copy: PhysicalDeviceToolPropertiesEXT);
}

export class ImageStencilUsageCreateInfoEXT {
    static $gtype: GObject.GType<ImageStencilUsageCreateInfoEXT>;

    constructor(copy: ImageStencilUsageCreateInfoEXT);
}

export class ValidationFeatureEnableEXT {
    static $gtype: GObject.GType<ValidationFeatureEnableEXT>;

    constructor(copy: ValidationFeatureEnableEXT);
}

export class ValidationFeatureDisableEXT {
    static $gtype: GObject.GType<ValidationFeatureDisableEXT>;

    constructor(copy: ValidationFeatureDisableEXT);
}

export class ValidationFeaturesEXT {
    static $gtype: GObject.GType<ValidationFeaturesEXT>;

    constructor(copy: ValidationFeaturesEXT);
}

export class ComponentTypeNV {
    static $gtype: GObject.GType<ComponentTypeNV>;

    constructor(copy: ComponentTypeNV);
}

export class ScopeNV {
    static $gtype: GObject.GType<ScopeNV>;

    constructor(copy: ScopeNV);
}

export class CooperativeMatrixPropertiesNV {
    static $gtype: GObject.GType<CooperativeMatrixPropertiesNV>;

    constructor(copy: CooperativeMatrixPropertiesNV);
}

export class PhysicalDeviceCooperativeMatrixFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceCooperativeMatrixFeaturesNV>;

    constructor(copy: PhysicalDeviceCooperativeMatrixFeaturesNV);
}

export class PhysicalDeviceCooperativeMatrixPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceCooperativeMatrixPropertiesNV>;

    constructor(copy: PhysicalDeviceCooperativeMatrixPropertiesNV);
}

export class CoverageReductionModeNV {
    static $gtype: GObject.GType<CoverageReductionModeNV>;

    constructor(copy: CoverageReductionModeNV);
}

export class PipelineCoverageReductionStateCreateFlagsNV {
    static $gtype: GObject.GType<PipelineCoverageReductionStateCreateFlagsNV>;

    constructor(copy: PipelineCoverageReductionStateCreateFlagsNV);
}

export class PhysicalDeviceCoverageReductionModeFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceCoverageReductionModeFeaturesNV>;

    constructor(copy: PhysicalDeviceCoverageReductionModeFeaturesNV);
}

export class PipelineCoverageReductionStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineCoverageReductionStateCreateInfoNV>;

    constructor(copy: PipelineCoverageReductionStateCreateInfoNV);
}

export class FramebufferMixedSamplesCombinationNV {
    static $gtype: GObject.GType<FramebufferMixedSamplesCombinationNV>;

    constructor(copy: FramebufferMixedSamplesCombinationNV);
}

export class PhysicalDeviceFragmentShaderInterlockFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShaderInterlockFeaturesEXT>;

    constructor(copy: PhysicalDeviceFragmentShaderInterlockFeaturesEXT);
}

export class PhysicalDeviceYcbcrImageArraysFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceYcbcrImageArraysFeaturesEXT>;

    constructor(copy: PhysicalDeviceYcbcrImageArraysFeaturesEXT);
}

export class ProvokingVertexModeEXT {
    static $gtype: GObject.GType<ProvokingVertexModeEXT>;

    constructor(copy: ProvokingVertexModeEXT);
}

export class PhysicalDeviceProvokingVertexFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceProvokingVertexFeaturesEXT>;

    constructor(copy: PhysicalDeviceProvokingVertexFeaturesEXT);
}

export class PhysicalDeviceProvokingVertexPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceProvokingVertexPropertiesEXT>;

    constructor(copy: PhysicalDeviceProvokingVertexPropertiesEXT);
}

export class PipelineRasterizationProvokingVertexStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineRasterizationProvokingVertexStateCreateInfoEXT>;

    constructor(copy: PipelineRasterizationProvokingVertexStateCreateInfoEXT);
}

export class HeadlessSurfaceCreateFlagsEXT {
    static $gtype: GObject.GType<HeadlessSurfaceCreateFlagsEXT>;

    constructor(copy: HeadlessSurfaceCreateFlagsEXT);
}

export class HeadlessSurfaceCreateInfoEXT {
    static $gtype: GObject.GType<HeadlessSurfaceCreateInfoEXT>;

    constructor(copy: HeadlessSurfaceCreateInfoEXT);
}

export class LineRasterizationModeEXT {
    static $gtype: GObject.GType<LineRasterizationModeEXT>;

    constructor(copy: LineRasterizationModeEXT);
}

export class PhysicalDeviceLineRasterizationFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceLineRasterizationFeaturesEXT>;

    constructor(copy: PhysicalDeviceLineRasterizationFeaturesEXT);
}

export class PhysicalDeviceLineRasterizationPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceLineRasterizationPropertiesEXT>;

    constructor(copy: PhysicalDeviceLineRasterizationPropertiesEXT);
}

export class PipelineRasterizationLineStateCreateInfoEXT {
    static $gtype: GObject.GType<PipelineRasterizationLineStateCreateInfoEXT>;

    constructor(copy: PipelineRasterizationLineStateCreateInfoEXT);
}

export class PhysicalDeviceShaderAtomicFloatFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderAtomicFloatFeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderAtomicFloatFeaturesEXT);
}

export class PhysicalDeviceHostQueryResetFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceHostQueryResetFeaturesEXT>;

    constructor(copy: PhysicalDeviceHostQueryResetFeaturesEXT);
}

export class PhysicalDeviceIndexTypeUint8FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceIndexTypeUint8FeaturesEXT>;

    constructor(copy: PhysicalDeviceIndexTypeUint8FeaturesEXT);
}

export class PhysicalDeviceExtendedDynamicStateFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceExtendedDynamicStateFeaturesEXT>;

    constructor(copy: PhysicalDeviceExtendedDynamicStateFeaturesEXT);
}

export class HostImageCopyFlagBitsEXT {
    static $gtype: GObject.GType<HostImageCopyFlagBitsEXT>;

    constructor(copy: HostImageCopyFlagBitsEXT);
}

export class HostImageCopyFlagsEXT {
    static $gtype: GObject.GType<HostImageCopyFlagsEXT>;

    constructor(copy: HostImageCopyFlagsEXT);
}

export class PhysicalDeviceHostImageCopyFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceHostImageCopyFeaturesEXT>;

    constructor(copy: PhysicalDeviceHostImageCopyFeaturesEXT);
}

export class PhysicalDeviceHostImageCopyPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceHostImageCopyPropertiesEXT>;

    constructor(copy: PhysicalDeviceHostImageCopyPropertiesEXT);
}

export class MemoryToImageCopyEXT {
    static $gtype: GObject.GType<MemoryToImageCopyEXT>;

    constructor(copy: MemoryToImageCopyEXT);
}

export class ImageToMemoryCopyEXT {
    static $gtype: GObject.GType<ImageToMemoryCopyEXT>;

    constructor(copy: ImageToMemoryCopyEXT);
}

export class CopyMemoryToImageInfoEXT {
    static $gtype: GObject.GType<CopyMemoryToImageInfoEXT>;

    constructor(copy: CopyMemoryToImageInfoEXT);
}

export class CopyImageToMemoryInfoEXT {
    static $gtype: GObject.GType<CopyImageToMemoryInfoEXT>;

    constructor(copy: CopyImageToMemoryInfoEXT);
}

export class CopyImageToImageInfoEXT {
    static $gtype: GObject.GType<CopyImageToImageInfoEXT>;

    constructor(copy: CopyImageToImageInfoEXT);
}

export class HostImageLayoutTransitionInfoEXT {
    static $gtype: GObject.GType<HostImageLayoutTransitionInfoEXT>;

    constructor(copy: HostImageLayoutTransitionInfoEXT);
}

export class SubresourceHostMemcpySizeEXT {
    static $gtype: GObject.GType<SubresourceHostMemcpySizeEXT>;

    constructor(copy: SubresourceHostMemcpySizeEXT);
}

export class HostImageCopyDevicePerformanceQueryEXT {
    static $gtype: GObject.GType<HostImageCopyDevicePerformanceQueryEXT>;

    constructor(copy: HostImageCopyDevicePerformanceQueryEXT);
}

export class SubresourceLayout2EXT {
    static $gtype: GObject.GType<SubresourceLayout2EXT>;

    constructor(copy: SubresourceLayout2EXT);
}

export class ImageSubresource2EXT {
    static $gtype: GObject.GType<ImageSubresource2EXT>;

    constructor(copy: ImageSubresource2EXT);
}

export class PhysicalDeviceMapMemoryPlacedFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMapMemoryPlacedFeaturesEXT>;

    constructor(copy: PhysicalDeviceMapMemoryPlacedFeaturesEXT);
}

export class PhysicalDeviceMapMemoryPlacedPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMapMemoryPlacedPropertiesEXT>;

    constructor(copy: PhysicalDeviceMapMemoryPlacedPropertiesEXT);
}

export class MemoryMapPlacedInfoEXT {
    static $gtype: GObject.GType<MemoryMapPlacedInfoEXT>;

    constructor(copy: MemoryMapPlacedInfoEXT);
}

export class PhysicalDeviceShaderAtomicFloat2FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderAtomicFloat2FeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderAtomicFloat2FeaturesEXT);
}

export class PresentScalingFlagBitsEXT {
    static $gtype: GObject.GType<PresentScalingFlagBitsEXT>;

    constructor(copy: PresentScalingFlagBitsEXT);
}

export class PresentScalingFlagsEXT {
    static $gtype: GObject.GType<PresentScalingFlagsEXT>;

    constructor(copy: PresentScalingFlagsEXT);
}

export class PresentGravityFlagBitsEXT {
    static $gtype: GObject.GType<PresentGravityFlagBitsEXT>;

    constructor(copy: PresentGravityFlagBitsEXT);
}

export class PresentGravityFlagsEXT {
    static $gtype: GObject.GType<PresentGravityFlagsEXT>;

    constructor(copy: PresentGravityFlagsEXT);
}

export class SurfacePresentModeEXT {
    static $gtype: GObject.GType<SurfacePresentModeEXT>;

    constructor(copy: SurfacePresentModeEXT);
}

export class SurfacePresentScalingCapabilitiesEXT {
    static $gtype: GObject.GType<SurfacePresentScalingCapabilitiesEXT>;

    constructor(copy: SurfacePresentScalingCapabilitiesEXT);
}

export class SurfacePresentModeCompatibilityEXT {
    static $gtype: GObject.GType<SurfacePresentModeCompatibilityEXT>;

    constructor(copy: SurfacePresentModeCompatibilityEXT);
}

export class PhysicalDeviceSwapchainMaintenance1FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceSwapchainMaintenance1FeaturesEXT>;

    constructor(copy: PhysicalDeviceSwapchainMaintenance1FeaturesEXT);
}

export class SwapchainPresentFenceInfoEXT {
    static $gtype: GObject.GType<SwapchainPresentFenceInfoEXT>;

    constructor(copy: SwapchainPresentFenceInfoEXT);
}

export class SwapchainPresentModesCreateInfoEXT {
    static $gtype: GObject.GType<SwapchainPresentModesCreateInfoEXT>;

    constructor(copy: SwapchainPresentModesCreateInfoEXT);
}

export class SwapchainPresentModeInfoEXT {
    static $gtype: GObject.GType<SwapchainPresentModeInfoEXT>;

    constructor(copy: SwapchainPresentModeInfoEXT);
}

export class SwapchainPresentScalingCreateInfoEXT {
    static $gtype: GObject.GType<SwapchainPresentScalingCreateInfoEXT>;

    constructor(copy: SwapchainPresentScalingCreateInfoEXT);
}

export class ReleaseSwapchainImagesInfoEXT {
    static $gtype: GObject.GType<ReleaseSwapchainImagesInfoEXT>;

    constructor(copy: ReleaseSwapchainImagesInfoEXT);
}

export class PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
}

export class IndirectCommandsLayoutNV {
    static $gtype: GObject.GType<IndirectCommandsLayoutNV>;

    constructor(copy: IndirectCommandsLayoutNV);
}

export class IndirectCommandsTokenTypeNV {
    static $gtype: GObject.GType<IndirectCommandsTokenTypeNV>;

    constructor(copy: IndirectCommandsTokenTypeNV);
}

export class IndirectStateFlagBitsNV {
    static $gtype: GObject.GType<IndirectStateFlagBitsNV>;

    constructor(copy: IndirectStateFlagBitsNV);
}

export class IndirectStateFlagsNV {
    static $gtype: GObject.GType<IndirectStateFlagsNV>;

    constructor(copy: IndirectStateFlagsNV);
}

export class IndirectCommandsLayoutUsageFlagBitsNV {
    static $gtype: GObject.GType<IndirectCommandsLayoutUsageFlagBitsNV>;

    constructor(copy: IndirectCommandsLayoutUsageFlagBitsNV);
}

export class IndirectCommandsLayoutUsageFlagsNV {
    static $gtype: GObject.GType<IndirectCommandsLayoutUsageFlagsNV>;

    constructor(copy: IndirectCommandsLayoutUsageFlagsNV);
}

export class PhysicalDeviceDeviceGeneratedCommandsPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceDeviceGeneratedCommandsPropertiesNV>;

    constructor(copy: PhysicalDeviceDeviceGeneratedCommandsPropertiesNV);
}

export class PhysicalDeviceDeviceGeneratedCommandsFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceDeviceGeneratedCommandsFeaturesNV>;

    constructor(copy: PhysicalDeviceDeviceGeneratedCommandsFeaturesNV);
}

export class GraphicsShaderGroupCreateInfoNV {
    static $gtype: GObject.GType<GraphicsShaderGroupCreateInfoNV>;

    constructor(copy: GraphicsShaderGroupCreateInfoNV);
}

export class GraphicsPipelineShaderGroupsCreateInfoNV {
    static $gtype: GObject.GType<GraphicsPipelineShaderGroupsCreateInfoNV>;

    constructor(copy: GraphicsPipelineShaderGroupsCreateInfoNV);
}

export class BindShaderGroupIndirectCommandNV {
    static $gtype: GObject.GType<BindShaderGroupIndirectCommandNV>;

    constructor(copy: BindShaderGroupIndirectCommandNV);
}

export class BindIndexBufferIndirectCommandNV {
    static $gtype: GObject.GType<BindIndexBufferIndirectCommandNV>;

    constructor(copy: BindIndexBufferIndirectCommandNV);
}

export class BindVertexBufferIndirectCommandNV {
    static $gtype: GObject.GType<BindVertexBufferIndirectCommandNV>;

    constructor(copy: BindVertexBufferIndirectCommandNV);
}

export class SetStateFlagsIndirectCommandNV {
    static $gtype: GObject.GType<SetStateFlagsIndirectCommandNV>;

    constructor(copy: SetStateFlagsIndirectCommandNV);
}

export class IndirectCommandsStreamNV {
    static $gtype: GObject.GType<IndirectCommandsStreamNV>;

    constructor(copy: IndirectCommandsStreamNV);
}

export class IndirectCommandsLayoutTokenNV {
    static $gtype: GObject.GType<IndirectCommandsLayoutTokenNV>;

    constructor(copy: IndirectCommandsLayoutTokenNV);
}

export class IndirectCommandsLayoutCreateInfoNV {
    static $gtype: GObject.GType<IndirectCommandsLayoutCreateInfoNV>;

    constructor(copy: IndirectCommandsLayoutCreateInfoNV);
}

export class GeneratedCommandsInfoNV {
    static $gtype: GObject.GType<GeneratedCommandsInfoNV>;

    constructor(copy: GeneratedCommandsInfoNV);
}

export class GeneratedCommandsMemoryRequirementsInfoNV {
    static $gtype: GObject.GType<GeneratedCommandsMemoryRequirementsInfoNV>;

    constructor(copy: GeneratedCommandsMemoryRequirementsInfoNV);
}

export class PhysicalDeviceInheritedViewportScissorFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceInheritedViewportScissorFeaturesNV>;

    constructor(copy: PhysicalDeviceInheritedViewportScissorFeaturesNV);
}

export class CommandBufferInheritanceViewportScissorInfoNV {
    static $gtype: GObject.GType<CommandBufferInheritanceViewportScissorInfoNV>;

    constructor(copy: CommandBufferInheritanceViewportScissorInfoNV);
}

export class PhysicalDeviceTexelBufferAlignmentFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceTexelBufferAlignmentFeaturesEXT>;

    constructor(copy: PhysicalDeviceTexelBufferAlignmentFeaturesEXT);
}

export class PhysicalDeviceTexelBufferAlignmentPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceTexelBufferAlignmentPropertiesEXT>;

    constructor(copy: PhysicalDeviceTexelBufferAlignmentPropertiesEXT);
}

export class RenderPassTransformBeginInfoQCOM {
    static $gtype: GObject.GType<RenderPassTransformBeginInfoQCOM>;

    constructor(copy: RenderPassTransformBeginInfoQCOM);
}

export class CommandBufferInheritanceRenderPassTransformInfoQCOM {
    static $gtype: GObject.GType<CommandBufferInheritanceRenderPassTransformInfoQCOM>;

    constructor(copy: CommandBufferInheritanceRenderPassTransformInfoQCOM);
}

export class DepthBiasRepresentationEXT {
    static $gtype: GObject.GType<DepthBiasRepresentationEXT>;

    constructor(copy: DepthBiasRepresentationEXT);
}

export class PhysicalDeviceDepthBiasControlFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDepthBiasControlFeaturesEXT>;

    constructor(copy: PhysicalDeviceDepthBiasControlFeaturesEXT);
}

export class DepthBiasInfoEXT {
    static $gtype: GObject.GType<DepthBiasInfoEXT>;

    constructor(copy: DepthBiasInfoEXT);
}

export class DepthBiasRepresentationInfoEXT {
    static $gtype: GObject.GType<DepthBiasRepresentationInfoEXT>;

    constructor(copy: DepthBiasRepresentationInfoEXT);
}

export class DeviceMemoryReportEventTypeEXT {
    static $gtype: GObject.GType<DeviceMemoryReportEventTypeEXT>;

    constructor(copy: DeviceMemoryReportEventTypeEXT);
}

export class DeviceMemoryReportFlagsEXT {
    static $gtype: GObject.GType<DeviceMemoryReportFlagsEXT>;

    constructor(copy: DeviceMemoryReportFlagsEXT);
}

export class PhysicalDeviceDeviceMemoryReportFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDeviceMemoryReportFeaturesEXT>;

    constructor(copy: PhysicalDeviceDeviceMemoryReportFeaturesEXT);
}

export class DeviceMemoryReportCallbackDataEXT {
    static $gtype: GObject.GType<DeviceMemoryReportCallbackDataEXT>;

    constructor(copy: DeviceMemoryReportCallbackDataEXT);
}

export class DeviceDeviceMemoryReportCreateInfoEXT {
    static $gtype: GObject.GType<DeviceDeviceMemoryReportCreateInfoEXT>;

    constructor(copy: DeviceDeviceMemoryReportCreateInfoEXT);
}

export class PhysicalDeviceRobustness2FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceRobustness2FeaturesEXT>;

    constructor(copy: PhysicalDeviceRobustness2FeaturesEXT);
}

export class PhysicalDeviceRobustness2PropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceRobustness2PropertiesEXT>;

    constructor(copy: PhysicalDeviceRobustness2PropertiesEXT);
}

export class SamplerCustomBorderColorCreateInfoEXT {
    static $gtype: GObject.GType<SamplerCustomBorderColorCreateInfoEXT>;

    constructor(copy: SamplerCustomBorderColorCreateInfoEXT);
}

export class PhysicalDeviceCustomBorderColorPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceCustomBorderColorPropertiesEXT>;

    constructor(copy: PhysicalDeviceCustomBorderColorPropertiesEXT);
}

export class PhysicalDeviceCustomBorderColorFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceCustomBorderColorFeaturesEXT>;

    constructor(copy: PhysicalDeviceCustomBorderColorFeaturesEXT);
}

export class PhysicalDevicePresentBarrierFeaturesNV {
    static $gtype: GObject.GType<PhysicalDevicePresentBarrierFeaturesNV>;

    constructor(copy: PhysicalDevicePresentBarrierFeaturesNV);
}

export class SurfaceCapabilitiesPresentBarrierNV {
    static $gtype: GObject.GType<SurfaceCapabilitiesPresentBarrierNV>;

    constructor(copy: SurfaceCapabilitiesPresentBarrierNV);
}

export class SwapchainPresentBarrierCreateInfoNV {
    static $gtype: GObject.GType<SwapchainPresentBarrierCreateInfoNV>;

    constructor(copy: SwapchainPresentBarrierCreateInfoNV);
}

export class PrivateDataSlotEXT {
    static $gtype: GObject.GType<PrivateDataSlotEXT>;

    constructor(copy: PrivateDataSlotEXT);
}

export class PrivateDataSlotCreateFlagsEXT {
    static $gtype: GObject.GType<PrivateDataSlotCreateFlagsEXT>;

    constructor(copy: PrivateDataSlotCreateFlagsEXT);
}

export class PhysicalDevicePrivateDataFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePrivateDataFeaturesEXT>;

    constructor(copy: PhysicalDevicePrivateDataFeaturesEXT);
}

export class DevicePrivateDataCreateInfoEXT {
    static $gtype: GObject.GType<DevicePrivateDataCreateInfoEXT>;

    constructor(copy: DevicePrivateDataCreateInfoEXT);
}

export class PrivateDataSlotCreateInfoEXT {
    static $gtype: GObject.GType<PrivateDataSlotCreateInfoEXT>;

    constructor(copy: PrivateDataSlotCreateInfoEXT);
}

export class PhysicalDevicePipelineCreationCacheControlFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePipelineCreationCacheControlFeaturesEXT>;

    constructor(copy: PhysicalDevicePipelineCreationCacheControlFeaturesEXT);
}

export class DeviceDiagnosticsConfigFlagBitsNV {
    static $gtype: GObject.GType<DeviceDiagnosticsConfigFlagBitsNV>;

    constructor(copy: DeviceDiagnosticsConfigFlagBitsNV);
}

export class DeviceDiagnosticsConfigFlagsNV {
    static $gtype: GObject.GType<DeviceDiagnosticsConfigFlagsNV>;

    constructor(copy: DeviceDiagnosticsConfigFlagsNV);
}

export class PhysicalDeviceDiagnosticsConfigFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceDiagnosticsConfigFeaturesNV>;

    constructor(copy: PhysicalDeviceDiagnosticsConfigFeaturesNV);
}

export class DeviceDiagnosticsConfigCreateInfoNV {
    static $gtype: GObject.GType<DeviceDiagnosticsConfigCreateInfoNV>;

    constructor(copy: DeviceDiagnosticsConfigCreateInfoNV);
}

export class CudaModuleNV {
    static $gtype: GObject.GType<CudaModuleNV>;

    constructor(copy: CudaModuleNV);
}

export class CudaFunctionNV {
    static $gtype: GObject.GType<CudaFunctionNV>;

    constructor(copy: CudaFunctionNV);
}

export class CudaModuleCreateInfoNV {
    static $gtype: GObject.GType<CudaModuleCreateInfoNV>;

    constructor(copy: CudaModuleCreateInfoNV);
}

export class CudaFunctionCreateInfoNV {
    static $gtype: GObject.GType<CudaFunctionCreateInfoNV>;

    constructor(copy: CudaFunctionCreateInfoNV);
}

export class CudaLaunchInfoNV {
    static $gtype: GObject.GType<CudaLaunchInfoNV>;

    constructor(copy: CudaLaunchInfoNV);
}

export class PhysicalDeviceCudaKernelLaunchFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceCudaKernelLaunchFeaturesNV>;

    constructor(copy: PhysicalDeviceCudaKernelLaunchFeaturesNV);
}

export class PhysicalDeviceCudaKernelLaunchPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceCudaKernelLaunchPropertiesNV>;

    constructor(copy: PhysicalDeviceCudaKernelLaunchPropertiesNV);
}

export class QueryLowLatencySupportNV {
    static $gtype: GObject.GType<QueryLowLatencySupportNV>;

    constructor(copy: QueryLowLatencySupportNV);
}

export class AccelerationStructureKHR {
    static $gtype: GObject.GType<AccelerationStructureKHR>;

    constructor(copy: AccelerationStructureKHR);
}

export class PhysicalDeviceDescriptorBufferPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorBufferPropertiesEXT>;

    constructor(copy: PhysicalDeviceDescriptorBufferPropertiesEXT);
}

export class PhysicalDeviceDescriptorBufferDensityMapPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorBufferDensityMapPropertiesEXT>;

    constructor(copy: PhysicalDeviceDescriptorBufferDensityMapPropertiesEXT);
}

export class PhysicalDeviceDescriptorBufferFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorBufferFeaturesEXT>;

    constructor(copy: PhysicalDeviceDescriptorBufferFeaturesEXT);
}

export class DescriptorAddressInfoEXT {
    static $gtype: GObject.GType<DescriptorAddressInfoEXT>;

    constructor(copy: DescriptorAddressInfoEXT);
}

export class DescriptorBufferBindingInfoEXT {
    static $gtype: GObject.GType<DescriptorBufferBindingInfoEXT>;

    constructor(copy: DescriptorBufferBindingInfoEXT);
}

export class DescriptorBufferBindingPushDescriptorBufferHandleEXT {
    static $gtype: GObject.GType<DescriptorBufferBindingPushDescriptorBufferHandleEXT>;

    constructor(copy: DescriptorBufferBindingPushDescriptorBufferHandleEXT);
}

export class DescriptorDataEXT {
    static $gtype: GObject.GType<DescriptorDataEXT>;

    constructor(copy: DescriptorDataEXT);
}

export class DescriptorGetInfoEXT {
    static $gtype: GObject.GType<DescriptorGetInfoEXT>;

    constructor(copy: DescriptorGetInfoEXT);
}

export class BufferCaptureDescriptorDataInfoEXT {
    static $gtype: GObject.GType<BufferCaptureDescriptorDataInfoEXT>;

    constructor(copy: BufferCaptureDescriptorDataInfoEXT);
}

export class ImageCaptureDescriptorDataInfoEXT {
    static $gtype: GObject.GType<ImageCaptureDescriptorDataInfoEXT>;

    constructor(copy: ImageCaptureDescriptorDataInfoEXT);
}

export class ImageViewCaptureDescriptorDataInfoEXT {
    static $gtype: GObject.GType<ImageViewCaptureDescriptorDataInfoEXT>;

    constructor(copy: ImageViewCaptureDescriptorDataInfoEXT);
}

export class SamplerCaptureDescriptorDataInfoEXT {
    static $gtype: GObject.GType<SamplerCaptureDescriptorDataInfoEXT>;

    constructor(copy: SamplerCaptureDescriptorDataInfoEXT);
}

export class OpaqueCaptureDescriptorDataCreateInfoEXT {
    static $gtype: GObject.GType<OpaqueCaptureDescriptorDataCreateInfoEXT>;

    constructor(copy: OpaqueCaptureDescriptorDataCreateInfoEXT);
}

export class AccelerationStructureCaptureDescriptorDataInfoEXT {
    static $gtype: GObject.GType<AccelerationStructureCaptureDescriptorDataInfoEXT>;

    constructor(copy: AccelerationStructureCaptureDescriptorDataInfoEXT);
}

export class GraphicsPipelineLibraryFlagBitsEXT {
    static $gtype: GObject.GType<GraphicsPipelineLibraryFlagBitsEXT>;

    constructor(copy: GraphicsPipelineLibraryFlagBitsEXT);
}

export class GraphicsPipelineLibraryFlagsEXT {
    static $gtype: GObject.GType<GraphicsPipelineLibraryFlagsEXT>;

    constructor(copy: GraphicsPipelineLibraryFlagsEXT);
}

export class PhysicalDeviceGraphicsPipelineLibraryFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceGraphicsPipelineLibraryFeaturesEXT>;

    constructor(copy: PhysicalDeviceGraphicsPipelineLibraryFeaturesEXT);
}

export class PhysicalDeviceGraphicsPipelineLibraryPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceGraphicsPipelineLibraryPropertiesEXT>;

    constructor(copy: PhysicalDeviceGraphicsPipelineLibraryPropertiesEXT);
}

export class GraphicsPipelineLibraryCreateInfoEXT {
    static $gtype: GObject.GType<GraphicsPipelineLibraryCreateInfoEXT>;

    constructor(copy: GraphicsPipelineLibraryCreateInfoEXT);
}

export class PhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAMD {
    static $gtype: GObject.GType<PhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAMD>;

    constructor(copy: PhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAMD);
}

export class FragmentShadingRateTypeNV {
    static $gtype: GObject.GType<FragmentShadingRateTypeNV>;

    constructor(copy: FragmentShadingRateTypeNV);
}

export class FragmentShadingRateNV {
    static $gtype: GObject.GType<FragmentShadingRateNV>;

    constructor(copy: FragmentShadingRateNV);
}

export class PhysicalDeviceFragmentShadingRateEnumsFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShadingRateEnumsFeaturesNV>;

    constructor(copy: PhysicalDeviceFragmentShadingRateEnumsFeaturesNV);
}

export class PhysicalDeviceFragmentShadingRateEnumsPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceFragmentShadingRateEnumsPropertiesNV>;

    constructor(copy: PhysicalDeviceFragmentShadingRateEnumsPropertiesNV);
}

export class PipelineFragmentShadingRateEnumStateCreateInfoNV {
    static $gtype: GObject.GType<PipelineFragmentShadingRateEnumStateCreateInfoNV>;

    constructor(copy: PipelineFragmentShadingRateEnumStateCreateInfoNV);
}

export class AccelerationStructureMotionInstanceTypeNV {
    static $gtype: GObject.GType<AccelerationStructureMotionInstanceTypeNV>;

    constructor(copy: AccelerationStructureMotionInstanceTypeNV);
}

export class AccelerationStructureMotionInfoFlagsNV {
    static $gtype: GObject.GType<AccelerationStructureMotionInfoFlagsNV>;

    constructor(copy: AccelerationStructureMotionInfoFlagsNV);
}

export class AccelerationStructureMotionInstanceFlagsNV {
    static $gtype: GObject.GType<AccelerationStructureMotionInstanceFlagsNV>;

    constructor(copy: AccelerationStructureMotionInstanceFlagsNV);
}

export class DeviceOrHostAddressConstKHR {
    static $gtype: GObject.GType<DeviceOrHostAddressConstKHR>;

    constructor(copy: DeviceOrHostAddressConstKHR);
}

export class AccelerationStructureGeometryMotionTrianglesDataNV {
    static $gtype: GObject.GType<AccelerationStructureGeometryMotionTrianglesDataNV>;

    constructor(copy: AccelerationStructureGeometryMotionTrianglesDataNV);
}

export class AccelerationStructureMotionInfoNV {
    static $gtype: GObject.GType<AccelerationStructureMotionInfoNV>;

    constructor(copy: AccelerationStructureMotionInfoNV);
}

export class AccelerationStructureMatrixMotionInstanceNV {
    static $gtype: GObject.GType<AccelerationStructureMatrixMotionInstanceNV>;

    constructor(copy: AccelerationStructureMatrixMotionInstanceNV);
}

export class SRTDataNV {
    static $gtype: GObject.GType<SRTDataNV>;

    constructor(copy: SRTDataNV);
}

export class AccelerationStructureSRTMotionInstanceNV {
    static $gtype: GObject.GType<AccelerationStructureSRTMotionInstanceNV>;

    constructor(copy: AccelerationStructureSRTMotionInstanceNV);
}

export class AccelerationStructureMotionInstanceDataNV {
    static $gtype: GObject.GType<AccelerationStructureMotionInstanceDataNV>;

    constructor(copy: AccelerationStructureMotionInstanceDataNV);
}

export class AccelerationStructureMotionInstanceNV {
    static $gtype: GObject.GType<AccelerationStructureMotionInstanceNV>;

    constructor(copy: AccelerationStructureMotionInstanceNV);
}

export class PhysicalDeviceRayTracingMotionBlurFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingMotionBlurFeaturesNV>;

    constructor(copy: PhysicalDeviceRayTracingMotionBlurFeaturesNV);
}

export class PhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT>;

    constructor(copy: PhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT);
}

export class PhysicalDeviceFragmentDensityMap2FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFragmentDensityMap2FeaturesEXT>;

    constructor(copy: PhysicalDeviceFragmentDensityMap2FeaturesEXT);
}

export class PhysicalDeviceFragmentDensityMap2PropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFragmentDensityMap2PropertiesEXT>;

    constructor(copy: PhysicalDeviceFragmentDensityMap2PropertiesEXT);
}

export class CopyCommandTransformInfoQCOM {
    static $gtype: GObject.GType<CopyCommandTransformInfoQCOM>;

    constructor(copy: CopyCommandTransformInfoQCOM);
}

export class PhysicalDeviceImageRobustnessFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageRobustnessFeaturesEXT>;

    constructor(copy: PhysicalDeviceImageRobustnessFeaturesEXT);
}

export class ImageCompressionFlagBitsEXT {
    static $gtype: GObject.GType<ImageCompressionFlagBitsEXT>;

    constructor(copy: ImageCompressionFlagBitsEXT);
}

export class ImageCompressionFlagsEXT {
    static $gtype: GObject.GType<ImageCompressionFlagsEXT>;

    constructor(copy: ImageCompressionFlagsEXT);
}

export class ImageCompressionFixedRateFlagBitsEXT {
    static $gtype: GObject.GType<ImageCompressionFixedRateFlagBitsEXT>;

    constructor(copy: ImageCompressionFixedRateFlagBitsEXT);
}

export class ImageCompressionFixedRateFlagsEXT {
    static $gtype: GObject.GType<ImageCompressionFixedRateFlagsEXT>;

    constructor(copy: ImageCompressionFixedRateFlagsEXT);
}

export class PhysicalDeviceImageCompressionControlFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageCompressionControlFeaturesEXT>;

    constructor(copy: PhysicalDeviceImageCompressionControlFeaturesEXT);
}

export class ImageCompressionControlEXT {
    static $gtype: GObject.GType<ImageCompressionControlEXT>;

    constructor(copy: ImageCompressionControlEXT);
}

export class ImageCompressionPropertiesEXT {
    static $gtype: GObject.GType<ImageCompressionPropertiesEXT>;

    constructor(copy: ImageCompressionPropertiesEXT);
}

export class PhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesEXT>;

    constructor(copy: PhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesEXT);
}

export class PhysicalDevice4444FormatsFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevice4444FormatsFeaturesEXT>;

    constructor(copy: PhysicalDevice4444FormatsFeaturesEXT);
}

export class DeviceFaultAddressTypeEXT {
    static $gtype: GObject.GType<DeviceFaultAddressTypeEXT>;

    constructor(copy: DeviceFaultAddressTypeEXT);
}

export class DeviceFaultVendorBinaryHeaderVersionEXT {
    static $gtype: GObject.GType<DeviceFaultVendorBinaryHeaderVersionEXT>;

    constructor(copy: DeviceFaultVendorBinaryHeaderVersionEXT);
}

export class PhysicalDeviceFaultFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFaultFeaturesEXT>;

    constructor(copy: PhysicalDeviceFaultFeaturesEXT);
}

export class DeviceFaultCountsEXT {
    static $gtype: GObject.GType<DeviceFaultCountsEXT>;

    constructor(copy: DeviceFaultCountsEXT);
}

export class DeviceFaultAddressInfoEXT {
    static $gtype: GObject.GType<DeviceFaultAddressInfoEXT>;

    constructor(copy: DeviceFaultAddressInfoEXT);
}

export class DeviceFaultVendorInfoEXT {
    static $gtype: GObject.GType<DeviceFaultVendorInfoEXT>;

    constructor(copy: DeviceFaultVendorInfoEXT);
}

export class DeviceFaultInfoEXT {
    static $gtype: GObject.GType<DeviceFaultInfoEXT>;

    constructor(copy: DeviceFaultInfoEXT);
}

export class DeviceFaultVendorBinaryHeaderVersionOneEXT {
    static $gtype: GObject.GType<DeviceFaultVendorBinaryHeaderVersionOneEXT>;

    constructor(copy: DeviceFaultVendorBinaryHeaderVersionOneEXT);
}

export class PhysicalDeviceRasterizationOrderAttachmentAccessFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceRasterizationOrderAttachmentAccessFeaturesEXT>;

    constructor(copy: PhysicalDeviceRasterizationOrderAttachmentAccessFeaturesEXT);
}

export class PhysicalDeviceRasterizationOrderAttachmentAccessFeaturesARM {
    static $gtype: GObject.GType<PhysicalDeviceRasterizationOrderAttachmentAccessFeaturesARM>;

    constructor(copy: PhysicalDeviceRasterizationOrderAttachmentAccessFeaturesARM);
}

export class PhysicalDeviceRGBA10X6FormatsFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceRGBA10X6FormatsFeaturesEXT>;

    constructor(copy: PhysicalDeviceRGBA10X6FormatsFeaturesEXT);
}

export class PhysicalDeviceMutableDescriptorTypeFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMutableDescriptorTypeFeaturesEXT>;

    constructor(copy: PhysicalDeviceMutableDescriptorTypeFeaturesEXT);
}

export class PhysicalDeviceMutableDescriptorTypeFeaturesVALVE {
    static $gtype: GObject.GType<PhysicalDeviceMutableDescriptorTypeFeaturesVALVE>;

    constructor(copy: PhysicalDeviceMutableDescriptorTypeFeaturesVALVE);
}

export class MutableDescriptorTypeListEXT {
    static $gtype: GObject.GType<MutableDescriptorTypeListEXT>;

    constructor(copy: MutableDescriptorTypeListEXT);
}

export class MutableDescriptorTypeListVALVE {
    static $gtype: GObject.GType<MutableDescriptorTypeListVALVE>;

    constructor(copy: MutableDescriptorTypeListVALVE);
}

export class MutableDescriptorTypeCreateInfoEXT {
    static $gtype: GObject.GType<MutableDescriptorTypeCreateInfoEXT>;

    constructor(copy: MutableDescriptorTypeCreateInfoEXT);
}

export class MutableDescriptorTypeCreateInfoVALVE {
    static $gtype: GObject.GType<MutableDescriptorTypeCreateInfoVALVE>;

    constructor(copy: MutableDescriptorTypeCreateInfoVALVE);
}

export class PhysicalDeviceVertexInputDynamicStateFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceVertexInputDynamicStateFeaturesEXT>;

    constructor(copy: PhysicalDeviceVertexInputDynamicStateFeaturesEXT);
}

export class VertexInputBindingDescription2EXT {
    static $gtype: GObject.GType<VertexInputBindingDescription2EXT>;

    constructor(copy: VertexInputBindingDescription2EXT);
}

export class VertexInputAttributeDescription2EXT {
    static $gtype: GObject.GType<VertexInputAttributeDescription2EXT>;

    constructor(copy: VertexInputAttributeDescription2EXT);
}

export class PhysicalDeviceDrmPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDrmPropertiesEXT>;

    constructor(copy: PhysicalDeviceDrmPropertiesEXT);
}

export class DeviceAddressBindingTypeEXT {
    static $gtype: GObject.GType<DeviceAddressBindingTypeEXT>;

    constructor(copy: DeviceAddressBindingTypeEXT);
}

export class DeviceAddressBindingFlagBitsEXT {
    static $gtype: GObject.GType<DeviceAddressBindingFlagBitsEXT>;

    constructor(copy: DeviceAddressBindingFlagBitsEXT);
}

export class DeviceAddressBindingFlagsEXT {
    static $gtype: GObject.GType<DeviceAddressBindingFlagsEXT>;

    constructor(copy: DeviceAddressBindingFlagsEXT);
}

export class PhysicalDeviceAddressBindingReportFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceAddressBindingReportFeaturesEXT>;

    constructor(copy: PhysicalDeviceAddressBindingReportFeaturesEXT);
}

export class DeviceAddressBindingCallbackDataEXT {
    static $gtype: GObject.GType<DeviceAddressBindingCallbackDataEXT>;

    constructor(copy: DeviceAddressBindingCallbackDataEXT);
}

export class PhysicalDeviceDepthClipControlFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDepthClipControlFeaturesEXT>;

    constructor(copy: PhysicalDeviceDepthClipControlFeaturesEXT);
}

export class PipelineViewportDepthClipControlCreateInfoEXT {
    static $gtype: GObject.GType<PipelineViewportDepthClipControlCreateInfoEXT>;

    constructor(copy: PipelineViewportDepthClipControlCreateInfoEXT);
}

export class PhysicalDevicePrimitiveTopologyListRestartFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePrimitiveTopologyListRestartFeaturesEXT>;

    constructor(copy: PhysicalDevicePrimitiveTopologyListRestartFeaturesEXT);
}

export class SubpassShadingPipelineCreateInfoHUAWEI {
    static $gtype: GObject.GType<SubpassShadingPipelineCreateInfoHUAWEI>;

    constructor(copy: SubpassShadingPipelineCreateInfoHUAWEI);
}

export class PhysicalDeviceSubpassShadingFeaturesHUAWEI {
    static $gtype: GObject.GType<PhysicalDeviceSubpassShadingFeaturesHUAWEI>;

    constructor(copy: PhysicalDeviceSubpassShadingFeaturesHUAWEI);
}

export class PhysicalDeviceSubpassShadingPropertiesHUAWEI {
    static $gtype: GObject.GType<PhysicalDeviceSubpassShadingPropertiesHUAWEI>;

    constructor(copy: PhysicalDeviceSubpassShadingPropertiesHUAWEI);
}

export class PhysicalDeviceInvocationMaskFeaturesHUAWEI {
    static $gtype: GObject.GType<PhysicalDeviceInvocationMaskFeaturesHUAWEI>;

    constructor(copy: PhysicalDeviceInvocationMaskFeaturesHUAWEI);
}

export class RemoteAddressNV {
    static $gtype: GObject.GType<RemoteAddressNV>;

    constructor(copy: RemoteAddressNV);
}

export class MemoryGetRemoteAddressInfoNV {
    static $gtype: GObject.GType<MemoryGetRemoteAddressInfoNV>;

    constructor(copy: MemoryGetRemoteAddressInfoNV);
}

export class PhysicalDeviceExternalMemoryRDMAFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceExternalMemoryRDMAFeaturesNV>;

    constructor(copy: PhysicalDeviceExternalMemoryRDMAFeaturesNV);
}

export class PipelineInfoEXT {
    static $gtype: GObject.GType<PipelineInfoEXT>;

    constructor(copy: PipelineInfoEXT);
}

export class PipelinePropertiesIdentifierEXT {
    static $gtype: GObject.GType<PipelinePropertiesIdentifierEXT>;

    constructor(copy: PipelinePropertiesIdentifierEXT);
}

export class PhysicalDevicePipelinePropertiesFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePipelinePropertiesFeaturesEXT>;

    constructor(copy: PhysicalDevicePipelinePropertiesFeaturesEXT);
}

export class FrameBoundaryFlagBitsEXT {
    static $gtype: GObject.GType<FrameBoundaryFlagBitsEXT>;

    constructor(copy: FrameBoundaryFlagBitsEXT);
}

export class FrameBoundaryFlagsEXT {
    static $gtype: GObject.GType<FrameBoundaryFlagsEXT>;

    constructor(copy: FrameBoundaryFlagsEXT);
}

export class PhysicalDeviceFrameBoundaryFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceFrameBoundaryFeaturesEXT>;

    constructor(copy: PhysicalDeviceFrameBoundaryFeaturesEXT);
}

export class FrameBoundaryEXT {
    static $gtype: GObject.GType<FrameBoundaryEXT>;

    constructor(copy: FrameBoundaryEXT);
}

export class PhysicalDeviceMultisampledRenderToSingleSampledFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMultisampledRenderToSingleSampledFeaturesEXT>;

    constructor(copy: PhysicalDeviceMultisampledRenderToSingleSampledFeaturesEXT);
}

export class SubpassResolvePerformanceQueryEXT {
    static $gtype: GObject.GType<SubpassResolvePerformanceQueryEXT>;

    constructor(copy: SubpassResolvePerformanceQueryEXT);
}

export class MultisampledRenderToSingleSampledInfoEXT {
    static $gtype: GObject.GType<MultisampledRenderToSingleSampledInfoEXT>;

    constructor(copy: MultisampledRenderToSingleSampledInfoEXT);
}

export class PhysicalDeviceExtendedDynamicState2FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceExtendedDynamicState2FeaturesEXT>;

    constructor(copy: PhysicalDeviceExtendedDynamicState2FeaturesEXT);
}

export class PhysicalDeviceColorWriteEnableFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceColorWriteEnableFeaturesEXT>;

    constructor(copy: PhysicalDeviceColorWriteEnableFeaturesEXT);
}

export class PipelineColorWriteCreateInfoEXT {
    static $gtype: GObject.GType<PipelineColorWriteCreateInfoEXT>;

    constructor(copy: PipelineColorWriteCreateInfoEXT);
}

export class PhysicalDevicePrimitivesGeneratedQueryFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePrimitivesGeneratedQueryFeaturesEXT>;

    constructor(copy: PhysicalDevicePrimitivesGeneratedQueryFeaturesEXT);
}

export class PhysicalDeviceGlobalPriorityQueryFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceGlobalPriorityQueryFeaturesEXT>;

    constructor(copy: PhysicalDeviceGlobalPriorityQueryFeaturesEXT);
}

export class QueueFamilyGlobalPriorityPropertiesEXT {
    static $gtype: GObject.GType<QueueFamilyGlobalPriorityPropertiesEXT>;

    constructor(copy: QueueFamilyGlobalPriorityPropertiesEXT);
}

export class PhysicalDeviceImageViewMinLodFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageViewMinLodFeaturesEXT>;

    constructor(copy: PhysicalDeviceImageViewMinLodFeaturesEXT);
}

export class ImageViewMinLodCreateInfoEXT {
    static $gtype: GObject.GType<ImageViewMinLodCreateInfoEXT>;

    constructor(copy: ImageViewMinLodCreateInfoEXT);
}

export class PhysicalDeviceMultiDrawFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMultiDrawFeaturesEXT>;

    constructor(copy: PhysicalDeviceMultiDrawFeaturesEXT);
}

export class PhysicalDeviceMultiDrawPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMultiDrawPropertiesEXT>;

    constructor(copy: PhysicalDeviceMultiDrawPropertiesEXT);
}

export class MultiDrawInfoEXT {
    static $gtype: GObject.GType<MultiDrawInfoEXT>;

    constructor(copy: MultiDrawInfoEXT);
}

export class MultiDrawIndexedInfoEXT {
    static $gtype: GObject.GType<MultiDrawIndexedInfoEXT>;

    constructor(copy: MultiDrawIndexedInfoEXT);
}

export class PhysicalDeviceImage2DViewOf3DFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceImage2DViewOf3DFeaturesEXT>;

    constructor(copy: PhysicalDeviceImage2DViewOf3DFeaturesEXT);
}

export class PhysicalDeviceShaderTileImageFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderTileImageFeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderTileImageFeaturesEXT);
}

export class PhysicalDeviceShaderTileImagePropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderTileImagePropertiesEXT>;

    constructor(copy: PhysicalDeviceShaderTileImagePropertiesEXT);
}

export class MicromapEXT {
    static $gtype: GObject.GType<MicromapEXT>;

    constructor(copy: MicromapEXT);
}

export class MicromapTypeEXT {
    static $gtype: GObject.GType<MicromapTypeEXT>;

    constructor(copy: MicromapTypeEXT);
}

export class BuildMicromapModeEXT {
    static $gtype: GObject.GType<BuildMicromapModeEXT>;

    constructor(copy: BuildMicromapModeEXT);
}

export class CopyMicromapModeEXT {
    static $gtype: GObject.GType<CopyMicromapModeEXT>;

    constructor(copy: CopyMicromapModeEXT);
}

export class OpacityMicromapFormatEXT {
    static $gtype: GObject.GType<OpacityMicromapFormatEXT>;

    constructor(copy: OpacityMicromapFormatEXT);
}

export class OpacityMicromapSpecialIndexEXT {
    static $gtype: GObject.GType<OpacityMicromapSpecialIndexEXT>;

    constructor(copy: OpacityMicromapSpecialIndexEXT);
}

export class AccelerationStructureCompatibilityKHR {
    static $gtype: GObject.GType<AccelerationStructureCompatibilityKHR>;

    constructor(copy: AccelerationStructureCompatibilityKHR);
}

export class AccelerationStructureBuildTypeKHR {
    static $gtype: GObject.GType<AccelerationStructureBuildTypeKHR>;

    constructor(copy: AccelerationStructureBuildTypeKHR);
}

export class BuildMicromapFlagBitsEXT {
    static $gtype: GObject.GType<BuildMicromapFlagBitsEXT>;

    constructor(copy: BuildMicromapFlagBitsEXT);
}

export class BuildMicromapFlagsEXT {
    static $gtype: GObject.GType<BuildMicromapFlagsEXT>;

    constructor(copy: BuildMicromapFlagsEXT);
}

export class MicromapCreateFlagBitsEXT {
    static $gtype: GObject.GType<MicromapCreateFlagBitsEXT>;

    constructor(copy: MicromapCreateFlagBitsEXT);
}

export class MicromapCreateFlagsEXT {
    static $gtype: GObject.GType<MicromapCreateFlagsEXT>;

    constructor(copy: MicromapCreateFlagsEXT);
}

export class MicromapUsageEXT {
    static $gtype: GObject.GType<MicromapUsageEXT>;

    constructor(copy: MicromapUsageEXT);
}

export class DeviceOrHostAddressKHR {
    static $gtype: GObject.GType<DeviceOrHostAddressKHR>;

    constructor(copy: DeviceOrHostAddressKHR);
}

export class MicromapBuildInfoEXT {
    static $gtype: GObject.GType<MicromapBuildInfoEXT>;

    constructor(copy: MicromapBuildInfoEXT);
}

export class MicromapCreateInfoEXT {
    static $gtype: GObject.GType<MicromapCreateInfoEXT>;

    constructor(copy: MicromapCreateInfoEXT);
}

export class PhysicalDeviceOpacityMicromapFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceOpacityMicromapFeaturesEXT>;

    constructor(copy: PhysicalDeviceOpacityMicromapFeaturesEXT);
}

export class PhysicalDeviceOpacityMicromapPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceOpacityMicromapPropertiesEXT>;

    constructor(copy: PhysicalDeviceOpacityMicromapPropertiesEXT);
}

export class MicromapVersionInfoEXT {
    static $gtype: GObject.GType<MicromapVersionInfoEXT>;

    constructor(copy: MicromapVersionInfoEXT);
}

export class CopyMicromapToMemoryInfoEXT {
    static $gtype: GObject.GType<CopyMicromapToMemoryInfoEXT>;

    constructor(copy: CopyMicromapToMemoryInfoEXT);
}

export class CopyMemoryToMicromapInfoEXT {
    static $gtype: GObject.GType<CopyMemoryToMicromapInfoEXT>;

    constructor(copy: CopyMemoryToMicromapInfoEXT);
}

export class CopyMicromapInfoEXT {
    static $gtype: GObject.GType<CopyMicromapInfoEXT>;

    constructor(copy: CopyMicromapInfoEXT);
}

export class MicromapBuildSizesInfoEXT {
    static $gtype: GObject.GType<MicromapBuildSizesInfoEXT>;

    constructor(copy: MicromapBuildSizesInfoEXT);
}

export class AccelerationStructureTrianglesOpacityMicromapEXT {
    static $gtype: GObject.GType<AccelerationStructureTrianglesOpacityMicromapEXT>;

    constructor(copy: AccelerationStructureTrianglesOpacityMicromapEXT);
}

export class MicromapTriangleEXT {
    static $gtype: GObject.GType<MicromapTriangleEXT>;

    constructor(copy: MicromapTriangleEXT);
}

export class PhysicalDeviceClusterCullingShaderFeaturesHUAWEI {
    static $gtype: GObject.GType<PhysicalDeviceClusterCullingShaderFeaturesHUAWEI>;

    constructor(copy: PhysicalDeviceClusterCullingShaderFeaturesHUAWEI);
}

export class PhysicalDeviceClusterCullingShaderPropertiesHUAWEI {
    static $gtype: GObject.GType<PhysicalDeviceClusterCullingShaderPropertiesHUAWEI>;

    constructor(copy: PhysicalDeviceClusterCullingShaderPropertiesHUAWEI);
}

export class PhysicalDeviceClusterCullingShaderVrsFeaturesHUAWEI {
    static $gtype: GObject.GType<PhysicalDeviceClusterCullingShaderVrsFeaturesHUAWEI>;

    constructor(copy: PhysicalDeviceClusterCullingShaderVrsFeaturesHUAWEI);
}

export class PhysicalDeviceBorderColorSwizzleFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceBorderColorSwizzleFeaturesEXT>;

    constructor(copy: PhysicalDeviceBorderColorSwizzleFeaturesEXT);
}

export class SamplerBorderColorComponentMappingCreateInfoEXT {
    static $gtype: GObject.GType<SamplerBorderColorComponentMappingCreateInfoEXT>;

    constructor(copy: SamplerBorderColorComponentMappingCreateInfoEXT);
}

export class PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT>;

    constructor(copy: PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT);
}

export class PhysicalDeviceShaderCorePropertiesARM {
    static $gtype: GObject.GType<PhysicalDeviceShaderCorePropertiesARM>;

    constructor(copy: PhysicalDeviceShaderCorePropertiesARM);
}

export class PhysicalDeviceSchedulingControlsFlagsARM {
    static $gtype: GObject.GType<PhysicalDeviceSchedulingControlsFlagsARM>;

    constructor(copy: PhysicalDeviceSchedulingControlsFlagsARM);
}

export class PhysicalDeviceSchedulingControlsFlagBitsARM {
    static $gtype: GObject.GType<PhysicalDeviceSchedulingControlsFlagBitsARM>;

    constructor(copy: PhysicalDeviceSchedulingControlsFlagBitsARM);
}

export class DeviceQueueShaderCoreControlCreateInfoARM {
    static $gtype: GObject.GType<DeviceQueueShaderCoreControlCreateInfoARM>;

    constructor(copy: DeviceQueueShaderCoreControlCreateInfoARM);
}

export class PhysicalDeviceSchedulingControlsFeaturesARM {
    static $gtype: GObject.GType<PhysicalDeviceSchedulingControlsFeaturesARM>;

    constructor(copy: PhysicalDeviceSchedulingControlsFeaturesARM);
}

export class PhysicalDeviceSchedulingControlsPropertiesARM {
    static $gtype: GObject.GType<PhysicalDeviceSchedulingControlsPropertiesARM>;

    constructor(copy: PhysicalDeviceSchedulingControlsPropertiesARM);
}

export class PhysicalDeviceImageSlicedViewOf3DFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageSlicedViewOf3DFeaturesEXT>;

    constructor(copy: PhysicalDeviceImageSlicedViewOf3DFeaturesEXT);
}

export class ImageViewSlicedCreateInfoEXT {
    static $gtype: GObject.GType<ImageViewSlicedCreateInfoEXT>;

    constructor(copy: ImageViewSlicedCreateInfoEXT);
}

export class PhysicalDeviceDescriptorSetHostMappingFeaturesVALVE {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorSetHostMappingFeaturesVALVE>;

    constructor(copy: PhysicalDeviceDescriptorSetHostMappingFeaturesVALVE);
}

export class DescriptorSetBindingReferenceVALVE {
    static $gtype: GObject.GType<DescriptorSetBindingReferenceVALVE>;

    constructor(copy: DescriptorSetBindingReferenceVALVE);
}

export class DescriptorSetLayoutHostMappingInfoVALVE {
    static $gtype: GObject.GType<DescriptorSetLayoutHostMappingInfoVALVE>;

    constructor(copy: DescriptorSetLayoutHostMappingInfoVALVE);
}

export class PhysicalDeviceDepthClampZeroOneFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDepthClampZeroOneFeaturesEXT>;

    constructor(copy: PhysicalDeviceDepthClampZeroOneFeaturesEXT);
}

export class PhysicalDeviceNonSeamlessCubeMapFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceNonSeamlessCubeMapFeaturesEXT>;

    constructor(copy: PhysicalDeviceNonSeamlessCubeMapFeaturesEXT);
}

export class PhysicalDeviceRenderPassStripedFeaturesARM {
    static $gtype: GObject.GType<PhysicalDeviceRenderPassStripedFeaturesARM>;

    constructor(copy: PhysicalDeviceRenderPassStripedFeaturesARM);
}

export class PhysicalDeviceRenderPassStripedPropertiesARM {
    static $gtype: GObject.GType<PhysicalDeviceRenderPassStripedPropertiesARM>;

    constructor(copy: PhysicalDeviceRenderPassStripedPropertiesARM);
}

export class RenderPassStripeInfoARM {
    static $gtype: GObject.GType<RenderPassStripeInfoARM>;

    constructor(copy: RenderPassStripeInfoARM);
}

export class RenderPassStripeBeginInfoARM {
    static $gtype: GObject.GType<RenderPassStripeBeginInfoARM>;

    constructor(copy: RenderPassStripeBeginInfoARM);
}

export class RenderPassStripeSubmitInfoARM {
    static $gtype: GObject.GType<RenderPassStripeSubmitInfoARM>;

    constructor(copy: RenderPassStripeSubmitInfoARM);
}

export class PhysicalDeviceFragmentDensityMapOffsetFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceFragmentDensityMapOffsetFeaturesQCOM>;

    constructor(copy: PhysicalDeviceFragmentDensityMapOffsetFeaturesQCOM);
}

export class PhysicalDeviceFragmentDensityMapOffsetPropertiesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceFragmentDensityMapOffsetPropertiesQCOM>;

    constructor(copy: PhysicalDeviceFragmentDensityMapOffsetPropertiesQCOM);
}

export class SubpassFragmentDensityMapOffsetEndInfoQCOM {
    static $gtype: GObject.GType<SubpassFragmentDensityMapOffsetEndInfoQCOM>;

    constructor(copy: SubpassFragmentDensityMapOffsetEndInfoQCOM);
}

export class CopyMemoryIndirectCommandNV {
    static $gtype: GObject.GType<CopyMemoryIndirectCommandNV>;

    constructor(copy: CopyMemoryIndirectCommandNV);
}

export class CopyMemoryToImageIndirectCommandNV {
    static $gtype: GObject.GType<CopyMemoryToImageIndirectCommandNV>;

    constructor(copy: CopyMemoryToImageIndirectCommandNV);
}

export class PhysicalDeviceCopyMemoryIndirectFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceCopyMemoryIndirectFeaturesNV>;

    constructor(copy: PhysicalDeviceCopyMemoryIndirectFeaturesNV);
}

export class PhysicalDeviceCopyMemoryIndirectPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceCopyMemoryIndirectPropertiesNV>;

    constructor(copy: PhysicalDeviceCopyMemoryIndirectPropertiesNV);
}

export class MemoryDecompressionMethodFlagBitsNV {
    static $gtype: GObject.GType<MemoryDecompressionMethodFlagBitsNV>;

    constructor(copy: MemoryDecompressionMethodFlagBitsNV);
}

export class MemoryDecompressionMethodFlagsNV {
    static $gtype: GObject.GType<MemoryDecompressionMethodFlagsNV>;

    constructor(copy: MemoryDecompressionMethodFlagsNV);
}

export class DecompressMemoryRegionNV {
    static $gtype: GObject.GType<DecompressMemoryRegionNV>;

    constructor(copy: DecompressMemoryRegionNV);
}

export class PhysicalDeviceMemoryDecompressionFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceMemoryDecompressionFeaturesNV>;

    constructor(copy: PhysicalDeviceMemoryDecompressionFeaturesNV);
}

export class PhysicalDeviceMemoryDecompressionPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceMemoryDecompressionPropertiesNV>;

    constructor(copy: PhysicalDeviceMemoryDecompressionPropertiesNV);
}

export class PhysicalDeviceDeviceGeneratedCommandsComputeFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceDeviceGeneratedCommandsComputeFeaturesNV>;

    constructor(copy: PhysicalDeviceDeviceGeneratedCommandsComputeFeaturesNV);
}

export class ComputePipelineIndirectBufferInfoNV {
    static $gtype: GObject.GType<ComputePipelineIndirectBufferInfoNV>;

    constructor(copy: ComputePipelineIndirectBufferInfoNV);
}

export class PipelineIndirectDeviceAddressInfoNV {
    static $gtype: GObject.GType<PipelineIndirectDeviceAddressInfoNV>;

    constructor(copy: PipelineIndirectDeviceAddressInfoNV);
}

export class BindPipelineIndirectCommandNV {
    static $gtype: GObject.GType<BindPipelineIndirectCommandNV>;

    constructor(copy: BindPipelineIndirectCommandNV);
}

export class PhysicalDeviceLinearColorAttachmentFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceLinearColorAttachmentFeaturesNV>;

    constructor(copy: PhysicalDeviceLinearColorAttachmentFeaturesNV);
}

export class PhysicalDeviceImageCompressionControlSwapchainFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceImageCompressionControlSwapchainFeaturesEXT>;

    constructor(copy: PhysicalDeviceImageCompressionControlSwapchainFeaturesEXT);
}

export class ImageViewSampleWeightCreateInfoQCOM {
    static $gtype: GObject.GType<ImageViewSampleWeightCreateInfoQCOM>;

    constructor(copy: ImageViewSampleWeightCreateInfoQCOM);
}

export class PhysicalDeviceImageProcessingFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceImageProcessingFeaturesQCOM>;

    constructor(copy: PhysicalDeviceImageProcessingFeaturesQCOM);
}

export class PhysicalDeviceImageProcessingPropertiesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceImageProcessingPropertiesQCOM>;

    constructor(copy: PhysicalDeviceImageProcessingPropertiesQCOM);
}

export class PhysicalDeviceNestedCommandBufferFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceNestedCommandBufferFeaturesEXT>;

    constructor(copy: PhysicalDeviceNestedCommandBufferFeaturesEXT);
}

export class PhysicalDeviceNestedCommandBufferPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceNestedCommandBufferPropertiesEXT>;

    constructor(copy: PhysicalDeviceNestedCommandBufferPropertiesEXT);
}

export class ExternalMemoryAcquireUnmodifiedEXT {
    static $gtype: GObject.GType<ExternalMemoryAcquireUnmodifiedEXT>;

    constructor(copy: ExternalMemoryAcquireUnmodifiedEXT);
}

export class PhysicalDeviceExtendedDynamicState3FeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceExtendedDynamicState3FeaturesEXT>;

    constructor(copy: PhysicalDeviceExtendedDynamicState3FeaturesEXT);
}

export class PhysicalDeviceExtendedDynamicState3PropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceExtendedDynamicState3PropertiesEXT>;

    constructor(copy: PhysicalDeviceExtendedDynamicState3PropertiesEXT);
}

export class ColorBlendEquationEXT {
    static $gtype: GObject.GType<ColorBlendEquationEXT>;

    constructor(copy: ColorBlendEquationEXT);
}

export class ColorBlendAdvancedEXT {
    static $gtype: GObject.GType<ColorBlendAdvancedEXT>;

    constructor(copy: ColorBlendAdvancedEXT);
}

export class SubpassMergeStatusEXT {
    static $gtype: GObject.GType<SubpassMergeStatusEXT>;

    constructor(copy: SubpassMergeStatusEXT);
}

export class PhysicalDeviceSubpassMergeFeedbackFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceSubpassMergeFeedbackFeaturesEXT>;

    constructor(copy: PhysicalDeviceSubpassMergeFeedbackFeaturesEXT);
}

export class RenderPassCreationControlEXT {
    static $gtype: GObject.GType<RenderPassCreationControlEXT>;

    constructor(copy: RenderPassCreationControlEXT);
}

export class RenderPassCreationFeedbackInfoEXT {
    static $gtype: GObject.GType<RenderPassCreationFeedbackInfoEXT>;

    constructor(copy: RenderPassCreationFeedbackInfoEXT);
}

export class RenderPassCreationFeedbackCreateInfoEXT {
    static $gtype: GObject.GType<RenderPassCreationFeedbackCreateInfoEXT>;

    constructor(copy: RenderPassCreationFeedbackCreateInfoEXT);
}

export class RenderPassSubpassFeedbackInfoEXT {
    static $gtype: GObject.GType<RenderPassSubpassFeedbackInfoEXT>;

    constructor(copy: RenderPassSubpassFeedbackInfoEXT);
}

export class RenderPassSubpassFeedbackCreateInfoEXT {
    static $gtype: GObject.GType<RenderPassSubpassFeedbackCreateInfoEXT>;

    constructor(copy: RenderPassSubpassFeedbackCreateInfoEXT);
}

export class DirectDriverLoadingModeLUNARG {
    static $gtype: GObject.GType<DirectDriverLoadingModeLUNARG>;

    constructor(copy: DirectDriverLoadingModeLUNARG);
}

export class DirectDriverLoadingFlagsLUNARG {
    static $gtype: GObject.GType<DirectDriverLoadingFlagsLUNARG>;

    constructor(copy: DirectDriverLoadingFlagsLUNARG);
}

export class DirectDriverLoadingInfoLUNARG {
    static $gtype: GObject.GType<DirectDriverLoadingInfoLUNARG>;

    constructor(copy: DirectDriverLoadingInfoLUNARG);
}

export class DirectDriverLoadingListLUNARG {
    static $gtype: GObject.GType<DirectDriverLoadingListLUNARG>;

    constructor(copy: DirectDriverLoadingListLUNARG);
}

export class PhysicalDeviceShaderModuleIdentifierFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderModuleIdentifierFeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderModuleIdentifierFeaturesEXT);
}

export class PhysicalDeviceShaderModuleIdentifierPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderModuleIdentifierPropertiesEXT>;

    constructor(copy: PhysicalDeviceShaderModuleIdentifierPropertiesEXT);
}

export class PipelineShaderStageModuleIdentifierCreateInfoEXT {
    static $gtype: GObject.GType<PipelineShaderStageModuleIdentifierCreateInfoEXT>;

    constructor(copy: PipelineShaderStageModuleIdentifierCreateInfoEXT);
}

export class ShaderModuleIdentifierEXT {
    static $gtype: GObject.GType<ShaderModuleIdentifierEXT>;

    constructor(copy: ShaderModuleIdentifierEXT);
}

export class OpticalFlowSessionNV {
    static $gtype: GObject.GType<OpticalFlowSessionNV>;

    constructor(copy: OpticalFlowSessionNV);
}

export class OpticalFlowPerformanceLevelNV {
    static $gtype: GObject.GType<OpticalFlowPerformanceLevelNV>;

    constructor(copy: OpticalFlowPerformanceLevelNV);
}

export class OpticalFlowSessionBindingPointNV {
    static $gtype: GObject.GType<OpticalFlowSessionBindingPointNV>;

    constructor(copy: OpticalFlowSessionBindingPointNV);
}

export class OpticalFlowGridSizeFlagBitsNV {
    static $gtype: GObject.GType<OpticalFlowGridSizeFlagBitsNV>;

    constructor(copy: OpticalFlowGridSizeFlagBitsNV);
}

export class OpticalFlowGridSizeFlagsNV {
    static $gtype: GObject.GType<OpticalFlowGridSizeFlagsNV>;

    constructor(copy: OpticalFlowGridSizeFlagsNV);
}

export class OpticalFlowUsageFlagBitsNV {
    static $gtype: GObject.GType<OpticalFlowUsageFlagBitsNV>;

    constructor(copy: OpticalFlowUsageFlagBitsNV);
}

export class OpticalFlowUsageFlagsNV {
    static $gtype: GObject.GType<OpticalFlowUsageFlagsNV>;

    constructor(copy: OpticalFlowUsageFlagsNV);
}

export class OpticalFlowSessionCreateFlagBitsNV {
    static $gtype: GObject.GType<OpticalFlowSessionCreateFlagBitsNV>;

    constructor(copy: OpticalFlowSessionCreateFlagBitsNV);
}

export class OpticalFlowSessionCreateFlagsNV {
    static $gtype: GObject.GType<OpticalFlowSessionCreateFlagsNV>;

    constructor(copy: OpticalFlowSessionCreateFlagsNV);
}

export class OpticalFlowExecuteFlagBitsNV {
    static $gtype: GObject.GType<OpticalFlowExecuteFlagBitsNV>;

    constructor(copy: OpticalFlowExecuteFlagBitsNV);
}

export class OpticalFlowExecuteFlagsNV {
    static $gtype: GObject.GType<OpticalFlowExecuteFlagsNV>;

    constructor(copy: OpticalFlowExecuteFlagsNV);
}

export class PhysicalDeviceOpticalFlowFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceOpticalFlowFeaturesNV>;

    constructor(copy: PhysicalDeviceOpticalFlowFeaturesNV);
}

export class PhysicalDeviceOpticalFlowPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceOpticalFlowPropertiesNV>;

    constructor(copy: PhysicalDeviceOpticalFlowPropertiesNV);
}

export class OpticalFlowImageFormatInfoNV {
    static $gtype: GObject.GType<OpticalFlowImageFormatInfoNV>;

    constructor(copy: OpticalFlowImageFormatInfoNV);
}

export class OpticalFlowImageFormatPropertiesNV {
    static $gtype: GObject.GType<OpticalFlowImageFormatPropertiesNV>;

    constructor(copy: OpticalFlowImageFormatPropertiesNV);
}

export class OpticalFlowSessionCreateInfoNV {
    static $gtype: GObject.GType<OpticalFlowSessionCreateInfoNV>;

    constructor(copy: OpticalFlowSessionCreateInfoNV);
}

export class OpticalFlowSessionCreatePrivateDataInfoNV {
    static $gtype: GObject.GType<OpticalFlowSessionCreatePrivateDataInfoNV>;

    constructor(copy: OpticalFlowSessionCreatePrivateDataInfoNV);
}

export class OpticalFlowExecuteInfoNV {
    static $gtype: GObject.GType<OpticalFlowExecuteInfoNV>;

    constructor(copy: OpticalFlowExecuteInfoNV);
}

export class PhysicalDeviceLegacyDitheringFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceLegacyDitheringFeaturesEXT>;

    constructor(copy: PhysicalDeviceLegacyDitheringFeaturesEXT);
}

export class PhysicalDevicePipelineProtectedAccessFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePipelineProtectedAccessFeaturesEXT>;

    constructor(copy: PhysicalDevicePipelineProtectedAccessFeaturesEXT);
}

export class ShaderEXT {
    static $gtype: GObject.GType<ShaderEXT>;

    constructor(copy: ShaderEXT);
}

export class ShaderCodeTypeEXT {
    static $gtype: GObject.GType<ShaderCodeTypeEXT>;

    constructor(copy: ShaderCodeTypeEXT);
}

export class ShaderCreateFlagBitsEXT {
    static $gtype: GObject.GType<ShaderCreateFlagBitsEXT>;

    constructor(copy: ShaderCreateFlagBitsEXT);
}

export class ShaderCreateFlagsEXT {
    static $gtype: GObject.GType<ShaderCreateFlagsEXT>;

    constructor(copy: ShaderCreateFlagsEXT);
}

export class PhysicalDeviceShaderObjectFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderObjectFeaturesEXT>;

    constructor(copy: PhysicalDeviceShaderObjectFeaturesEXT);
}

export class PhysicalDeviceShaderObjectPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceShaderObjectPropertiesEXT>;

    constructor(copy: PhysicalDeviceShaderObjectPropertiesEXT);
}

export class ShaderCreateInfoEXT {
    static $gtype: GObject.GType<ShaderCreateInfoEXT>;

    constructor(copy: ShaderCreateInfoEXT);
}

export class ShaderRequiredSubgroupSizeCreateInfoEXT {
    static $gtype: GObject.GType<ShaderRequiredSubgroupSizeCreateInfoEXT>;

    constructor(copy: ShaderRequiredSubgroupSizeCreateInfoEXT);
}

export class PhysicalDeviceTilePropertiesFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceTilePropertiesFeaturesQCOM>;

    constructor(copy: PhysicalDeviceTilePropertiesFeaturesQCOM);
}

export class TilePropertiesQCOM {
    static $gtype: GObject.GType<TilePropertiesQCOM>;

    constructor(copy: TilePropertiesQCOM);
}

export class PhysicalDeviceAmigoProfilingFeaturesSEC {
    static $gtype: GObject.GType<PhysicalDeviceAmigoProfilingFeaturesSEC>;

    constructor(copy: PhysicalDeviceAmigoProfilingFeaturesSEC);
}

export class AmigoProfilingSubmitInfoSEC {
    static $gtype: GObject.GType<AmigoProfilingSubmitInfoSEC>;

    constructor(copy: AmigoProfilingSubmitInfoSEC);
}

export class PhysicalDeviceMultiviewPerViewViewportsFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewPerViewViewportsFeaturesQCOM>;

    constructor(copy: PhysicalDeviceMultiviewPerViewViewportsFeaturesQCOM);
}

export class RayTracingInvocationReorderModeNV {
    static $gtype: GObject.GType<RayTracingInvocationReorderModeNV>;

    constructor(copy: RayTracingInvocationReorderModeNV);
}

export class PhysicalDeviceRayTracingInvocationReorderPropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingInvocationReorderPropertiesNV>;

    constructor(copy: PhysicalDeviceRayTracingInvocationReorderPropertiesNV);
}

export class PhysicalDeviceRayTracingInvocationReorderFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingInvocationReorderFeaturesNV>;

    constructor(copy: PhysicalDeviceRayTracingInvocationReorderFeaturesNV);
}

export class PhysicalDeviceExtendedSparseAddressSpaceFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceExtendedSparseAddressSpaceFeaturesNV>;

    constructor(copy: PhysicalDeviceExtendedSparseAddressSpaceFeaturesNV);
}

export class PhysicalDeviceExtendedSparseAddressSpacePropertiesNV {
    static $gtype: GObject.GType<PhysicalDeviceExtendedSparseAddressSpacePropertiesNV>;

    constructor(copy: PhysicalDeviceExtendedSparseAddressSpacePropertiesNV);
}

export class PhysicalDeviceLegacyVertexAttributesFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceLegacyVertexAttributesFeaturesEXT>;

    constructor(copy: PhysicalDeviceLegacyVertexAttributesFeaturesEXT);
}

export class PhysicalDeviceLegacyVertexAttributesPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceLegacyVertexAttributesPropertiesEXT>;

    constructor(copy: PhysicalDeviceLegacyVertexAttributesPropertiesEXT);
}

export class LayerSettingTypeEXT {
    static $gtype: GObject.GType<LayerSettingTypeEXT>;

    constructor(copy: LayerSettingTypeEXT);
}

export class LayerSettingEXT {
    static $gtype: GObject.GType<LayerSettingEXT>;

    constructor(copy: LayerSettingEXT);
}

export class LayerSettingsCreateInfoEXT {
    static $gtype: GObject.GType<LayerSettingsCreateInfoEXT>;

    constructor(copy: LayerSettingsCreateInfoEXT);
}

export class PhysicalDeviceShaderCoreBuiltinsFeaturesARM {
    static $gtype: GObject.GType<PhysicalDeviceShaderCoreBuiltinsFeaturesARM>;

    constructor(copy: PhysicalDeviceShaderCoreBuiltinsFeaturesARM);
}

export class PhysicalDeviceShaderCoreBuiltinsPropertiesARM {
    static $gtype: GObject.GType<PhysicalDeviceShaderCoreBuiltinsPropertiesARM>;

    constructor(copy: PhysicalDeviceShaderCoreBuiltinsPropertiesARM);
}

export class PhysicalDevicePipelineLibraryGroupHandlesFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDevicePipelineLibraryGroupHandlesFeaturesEXT>;

    constructor(copy: PhysicalDevicePipelineLibraryGroupHandlesFeaturesEXT);
}

export class PhysicalDeviceDynamicRenderingUnusedAttachmentsFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceDynamicRenderingUnusedAttachmentsFeaturesEXT>;

    constructor(copy: PhysicalDeviceDynamicRenderingUnusedAttachmentsFeaturesEXT);
}

export class LatencyMarkerNV {
    static $gtype: GObject.GType<LatencyMarkerNV>;

    constructor(copy: LatencyMarkerNV);
}

export class OutOfBandQueueTypeNV {
    static $gtype: GObject.GType<OutOfBandQueueTypeNV>;

    constructor(copy: OutOfBandQueueTypeNV);
}

export class LatencySleepModeInfoNV {
    static $gtype: GObject.GType<LatencySleepModeInfoNV>;

    constructor(copy: LatencySleepModeInfoNV);
}

export class LatencySleepInfoNV {
    static $gtype: GObject.GType<LatencySleepInfoNV>;

    constructor(copy: LatencySleepInfoNV);
}

export class SetLatencyMarkerInfoNV {
    static $gtype: GObject.GType<SetLatencyMarkerInfoNV>;

    constructor(copy: SetLatencyMarkerInfoNV);
}

export class LatencyTimingsFrameReportNV {
    static $gtype: GObject.GType<LatencyTimingsFrameReportNV>;

    constructor(copy: LatencyTimingsFrameReportNV);
}

export class GetLatencyMarkerInfoNV {
    static $gtype: GObject.GType<GetLatencyMarkerInfoNV>;

    constructor(copy: GetLatencyMarkerInfoNV);
}

export class LatencySubmissionPresentIdNV {
    static $gtype: GObject.GType<LatencySubmissionPresentIdNV>;

    constructor(copy: LatencySubmissionPresentIdNV);
}

export class SwapchainLatencyCreateInfoNV {
    static $gtype: GObject.GType<SwapchainLatencyCreateInfoNV>;

    constructor(copy: SwapchainLatencyCreateInfoNV);
}

export class OutOfBandQueueTypeInfoNV {
    static $gtype: GObject.GType<OutOfBandQueueTypeInfoNV>;

    constructor(copy: OutOfBandQueueTypeInfoNV);
}

export class LatencySurfaceCapabilitiesNV {
    static $gtype: GObject.GType<LatencySurfaceCapabilitiesNV>;

    constructor(copy: LatencySurfaceCapabilitiesNV);
}

export class PhysicalDeviceMultiviewPerViewRenderAreasFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceMultiviewPerViewRenderAreasFeaturesQCOM>;

    constructor(copy: PhysicalDeviceMultiviewPerViewRenderAreasFeaturesQCOM);
}

export class MultiviewPerViewRenderAreasRenderPassBeginInfoQCOM {
    static $gtype: GObject.GType<MultiviewPerViewRenderAreasRenderPassBeginInfoQCOM>;

    constructor(copy: MultiviewPerViewRenderAreasRenderPassBeginInfoQCOM);
}

export class PhysicalDevicePerStageDescriptorSetFeaturesNV {
    static $gtype: GObject.GType<PhysicalDevicePerStageDescriptorSetFeaturesNV>;

    constructor(copy: PhysicalDevicePerStageDescriptorSetFeaturesNV);
}

export class BlockMatchWindowCompareModeQCOM {
    static $gtype: GObject.GType<BlockMatchWindowCompareModeQCOM>;

    constructor(copy: BlockMatchWindowCompareModeQCOM);
}

export class PhysicalDeviceImageProcessing2FeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceImageProcessing2FeaturesQCOM>;

    constructor(copy: PhysicalDeviceImageProcessing2FeaturesQCOM);
}

export class PhysicalDeviceImageProcessing2PropertiesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceImageProcessing2PropertiesQCOM>;

    constructor(copy: PhysicalDeviceImageProcessing2PropertiesQCOM);
}

export class SamplerBlockMatchWindowCreateInfoQCOM {
    static $gtype: GObject.GType<SamplerBlockMatchWindowCreateInfoQCOM>;

    constructor(copy: SamplerBlockMatchWindowCreateInfoQCOM);
}

export class CubicFilterWeightsQCOM {
    static $gtype: GObject.GType<CubicFilterWeightsQCOM>;

    constructor(copy: CubicFilterWeightsQCOM);
}

export class PhysicalDeviceCubicWeightsFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceCubicWeightsFeaturesQCOM>;

    constructor(copy: PhysicalDeviceCubicWeightsFeaturesQCOM);
}

export class SamplerCubicWeightsCreateInfoQCOM {
    static $gtype: GObject.GType<SamplerCubicWeightsCreateInfoQCOM>;

    constructor(copy: SamplerCubicWeightsCreateInfoQCOM);
}

export class BlitImageCubicWeightsInfoQCOM {
    static $gtype: GObject.GType<BlitImageCubicWeightsInfoQCOM>;

    constructor(copy: BlitImageCubicWeightsInfoQCOM);
}

export class PhysicalDeviceYcbcrDegammaFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceYcbcrDegammaFeaturesQCOM>;

    constructor(copy: PhysicalDeviceYcbcrDegammaFeaturesQCOM);
}

export class SamplerYcbcrConversionYcbcrDegammaCreateInfoQCOM {
    static $gtype: GObject.GType<SamplerYcbcrConversionYcbcrDegammaCreateInfoQCOM>;

    constructor(copy: SamplerYcbcrConversionYcbcrDegammaCreateInfoQCOM);
}

export class PhysicalDeviceCubicClampFeaturesQCOM {
    static $gtype: GObject.GType<PhysicalDeviceCubicClampFeaturesQCOM>;

    constructor(copy: PhysicalDeviceCubicClampFeaturesQCOM);
}

export class PhysicalDeviceAttachmentFeedbackLoopDynamicStateFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceAttachmentFeedbackLoopDynamicStateFeaturesEXT>;

    constructor(copy: PhysicalDeviceAttachmentFeedbackLoopDynamicStateFeaturesEXT);
}

export class LayeredDriverUnderlyingApiMSFT {
    static $gtype: GObject.GType<LayeredDriverUnderlyingApiMSFT>;

    constructor(copy: LayeredDriverUnderlyingApiMSFT);
}

export class PhysicalDeviceLayeredDriverPropertiesMSFT {
    static $gtype: GObject.GType<PhysicalDeviceLayeredDriverPropertiesMSFT>;

    constructor(copy: PhysicalDeviceLayeredDriverPropertiesMSFT);
}

export class PhysicalDeviceDescriptorPoolOverallocationFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceDescriptorPoolOverallocationFeaturesNV>;

    constructor(copy: PhysicalDeviceDescriptorPoolOverallocationFeaturesNV);
}

export class PhysicalDeviceRawAccessChainsFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceRawAccessChainsFeaturesNV>;

    constructor(copy: PhysicalDeviceRawAccessChainsFeaturesNV);
}

export class PhysicalDeviceShaderAtomicFloat16VectorFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceShaderAtomicFloat16VectorFeaturesNV>;

    constructor(copy: PhysicalDeviceShaderAtomicFloat16VectorFeaturesNV);
}

export class PhysicalDeviceRayTracingValidationFeaturesNV {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingValidationFeaturesNV>;

    constructor(copy: PhysicalDeviceRayTracingValidationFeaturesNV);
}

export class PhysicalDeviceImageAlignmentControlFeaturesMESA {
    static $gtype: GObject.GType<PhysicalDeviceImageAlignmentControlFeaturesMESA>;

    constructor(copy: PhysicalDeviceImageAlignmentControlFeaturesMESA);
}

export class PhysicalDeviceImageAlignmentControlPropertiesMESA {
    static $gtype: GObject.GType<PhysicalDeviceImageAlignmentControlPropertiesMESA>;

    constructor(copy: PhysicalDeviceImageAlignmentControlPropertiesMESA);
}

export class ImageAlignmentControlCreateInfoMESA {
    static $gtype: GObject.GType<ImageAlignmentControlCreateInfoMESA>;

    constructor(copy: ImageAlignmentControlCreateInfoMESA);
}

export class BuildAccelerationStructureModeKHR {
    static $gtype: GObject.GType<BuildAccelerationStructureModeKHR>;

    constructor(copy: BuildAccelerationStructureModeKHR);
}

export class AccelerationStructureCreateFlagBitsKHR {
    static $gtype: GObject.GType<AccelerationStructureCreateFlagBitsKHR>;

    constructor(copy: AccelerationStructureCreateFlagBitsKHR);
}

export class AccelerationStructureCreateFlagsKHR {
    static $gtype: GObject.GType<AccelerationStructureCreateFlagsKHR>;

    constructor(copy: AccelerationStructureCreateFlagsKHR);
}

export class AccelerationStructureBuildRangeInfoKHR {
    static $gtype: GObject.GType<AccelerationStructureBuildRangeInfoKHR>;

    constructor(copy: AccelerationStructureBuildRangeInfoKHR);
}

export class AccelerationStructureGeometryTrianglesDataKHR {
    static $gtype: GObject.GType<AccelerationStructureGeometryTrianglesDataKHR>;

    constructor(copy: AccelerationStructureGeometryTrianglesDataKHR);
}

export class AccelerationStructureGeometryAabbsDataKHR {
    static $gtype: GObject.GType<AccelerationStructureGeometryAabbsDataKHR>;

    constructor(copy: AccelerationStructureGeometryAabbsDataKHR);
}

export class AccelerationStructureGeometryInstancesDataKHR {
    static $gtype: GObject.GType<AccelerationStructureGeometryInstancesDataKHR>;

    constructor(copy: AccelerationStructureGeometryInstancesDataKHR);
}

export class AccelerationStructureGeometryDataKHR {
    static $gtype: GObject.GType<AccelerationStructureGeometryDataKHR>;

    constructor(copy: AccelerationStructureGeometryDataKHR);
}

export class AccelerationStructureGeometryKHR {
    static $gtype: GObject.GType<AccelerationStructureGeometryKHR>;

    constructor(copy: AccelerationStructureGeometryKHR);
}

export class AccelerationStructureBuildGeometryInfoKHR {
    static $gtype: GObject.GType<AccelerationStructureBuildGeometryInfoKHR>;

    constructor(copy: AccelerationStructureBuildGeometryInfoKHR);
}

export class AccelerationStructureCreateInfoKHR {
    static $gtype: GObject.GType<AccelerationStructureCreateInfoKHR>;

    constructor(copy: AccelerationStructureCreateInfoKHR);
}

export class WriteDescriptorSetAccelerationStructureKHR {
    static $gtype: GObject.GType<WriteDescriptorSetAccelerationStructureKHR>;

    constructor(copy: WriteDescriptorSetAccelerationStructureKHR);
}

export class PhysicalDeviceAccelerationStructureFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceAccelerationStructureFeaturesKHR>;

    constructor(copy: PhysicalDeviceAccelerationStructureFeaturesKHR);
}

export class PhysicalDeviceAccelerationStructurePropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceAccelerationStructurePropertiesKHR>;

    constructor(copy: PhysicalDeviceAccelerationStructurePropertiesKHR);
}

export class AccelerationStructureDeviceAddressInfoKHR {
    static $gtype: GObject.GType<AccelerationStructureDeviceAddressInfoKHR>;

    constructor(copy: AccelerationStructureDeviceAddressInfoKHR);
}

export class AccelerationStructureVersionInfoKHR {
    static $gtype: GObject.GType<AccelerationStructureVersionInfoKHR>;

    constructor(copy: AccelerationStructureVersionInfoKHR);
}

export class CopyAccelerationStructureToMemoryInfoKHR {
    static $gtype: GObject.GType<CopyAccelerationStructureToMemoryInfoKHR>;

    constructor(copy: CopyAccelerationStructureToMemoryInfoKHR);
}

export class CopyMemoryToAccelerationStructureInfoKHR {
    static $gtype: GObject.GType<CopyMemoryToAccelerationStructureInfoKHR>;

    constructor(copy: CopyMemoryToAccelerationStructureInfoKHR);
}

export class CopyAccelerationStructureInfoKHR {
    static $gtype: GObject.GType<CopyAccelerationStructureInfoKHR>;

    constructor(copy: CopyAccelerationStructureInfoKHR);
}

export class AccelerationStructureBuildSizesInfoKHR {
    static $gtype: GObject.GType<AccelerationStructureBuildSizesInfoKHR>;

    constructor(copy: AccelerationStructureBuildSizesInfoKHR);
}

export class ShaderGroupShaderKHR {
    static $gtype: GObject.GType<ShaderGroupShaderKHR>;

    constructor(copy: ShaderGroupShaderKHR);
}

export class RayTracingShaderGroupCreateInfoKHR {
    static $gtype: GObject.GType<RayTracingShaderGroupCreateInfoKHR>;

    constructor(copy: RayTracingShaderGroupCreateInfoKHR);
}

export class RayTracingPipelineInterfaceCreateInfoKHR {
    static $gtype: GObject.GType<RayTracingPipelineInterfaceCreateInfoKHR>;

    constructor(copy: RayTracingPipelineInterfaceCreateInfoKHR);
}

export class RayTracingPipelineCreateInfoKHR {
    static $gtype: GObject.GType<RayTracingPipelineCreateInfoKHR>;

    constructor(copy: RayTracingPipelineCreateInfoKHR);
}

export class PhysicalDeviceRayTracingPipelineFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingPipelineFeaturesKHR>;

    constructor(copy: PhysicalDeviceRayTracingPipelineFeaturesKHR);
}

export class PhysicalDeviceRayTracingPipelinePropertiesKHR {
    static $gtype: GObject.GType<PhysicalDeviceRayTracingPipelinePropertiesKHR>;

    constructor(copy: PhysicalDeviceRayTracingPipelinePropertiesKHR);
}

export class StridedDeviceAddressRegionKHR {
    static $gtype: GObject.GType<StridedDeviceAddressRegionKHR>;

    constructor(copy: StridedDeviceAddressRegionKHR);
}

export class TraceRaysIndirectCommandKHR {
    static $gtype: GObject.GType<TraceRaysIndirectCommandKHR>;

    constructor(copy: TraceRaysIndirectCommandKHR);
}

export class PhysicalDeviceRayQueryFeaturesKHR {
    static $gtype: GObject.GType<PhysicalDeviceRayQueryFeaturesKHR>;

    constructor(copy: PhysicalDeviceRayQueryFeaturesKHR);
}

export class PhysicalDeviceMeshShaderFeaturesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMeshShaderFeaturesEXT>;

    constructor(copy: PhysicalDeviceMeshShaderFeaturesEXT);
}

export class PhysicalDeviceMeshShaderPropertiesEXT {
    static $gtype: GObject.GType<PhysicalDeviceMeshShaderPropertiesEXT>;

    constructor(copy: PhysicalDeviceMeshShaderPropertiesEXT);
}

export class DrawMeshTasksIndirectCommandEXT {
    static $gtype: GObject.GType<DrawMeshTasksIndirectCommandEXT>;

    constructor(copy: DrawMeshTasksIndirectCommandEXT);
}
